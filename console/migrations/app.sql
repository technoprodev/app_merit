-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

UPDATE `configuration` SET `id` = '1', `name` = 'app.name', `type` = 'var', `value` = 'Merit Self Assessment' WHERE `id` = '1';
UPDATE `configuration` SET `id` = '2', `name` = 'app.description', `type` = 'var', `value` = 'Penilaian Mandiri Penerapan Sistem Merit' WHERE `id` = '2';
UPDATE `configuration` SET `id` = '3', `name` = 'app.keywords', `type` = 'var', `value` = 'technopro, KASN, merit' WHERE `id` = '3';
UPDATE `configuration` SET `id` = '4', `name` = 'app.owner', `type` = 'var', `value` = 'KASN' WHERE `id` = '4';
UPDATE `configuration` SET `id` = '5', `name` = 'app.author', `type` = 'var', `value` = 'KASN' WHERE `id` = '5';
UPDATE `configuration` SET `id` = '6', `name` = 'enable.permission-checking', `type` = 'bool', `value` = 'false' WHERE `id` = '6';
UPDATE `configuration` SET `id` = '7', `name` = 'enable.permission-init', `type` = 'bool', `value` = 'false' WHERE `id` = '7';