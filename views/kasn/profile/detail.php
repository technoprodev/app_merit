<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['name'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['email'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['username'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user']->attributeLabels()['phone'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user']->phone ? $model['user']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user_kasn']->attributeLabels()['jabatan_di_kasn'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user_kasn']->jabatan_di_kasn ? $model['user_kasn']->jabatan_di_kasn : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['user_kasn']->attributeLabels()['jabatan_di_sk'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['user_kasn']->jabatan_di_sk ? $model['user_kasn']->jabatan_di_sk : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    <hr class="margin-y-15">
    <div class="form-group clearfix">
        <?= Html::a('Update', ['update', 'id' => $model['user']->id], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>&nbsp;
        <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure text-azure hover-bg-azure pull-right']) ?>
    </div>
</div>
<?php endif; ?>