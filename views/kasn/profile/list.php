<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kasn/profile/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <div class="clearfix">
        <h1 class="text-cyan margin-bottom-30 pull-left m-pull-none">
            <?= $title ?>
        </h1>
        <?php if ($this->context->can('create')): ?>
            <?= Html::a('Pendaftaran User Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none margin-y-20 m-button-block']) ?>
        <?php endif; ?>
    </div>

    <table class="datatables table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr class="bg-azure border-azure fs-14">
                <th style="text-align: center;">Action</th>
                <th style="text-align: center;">Nama</th>
                <th style="text-align: center;">Email</th>
                <th style="text-align: center;">Username</th>
                <th style="text-align: center;">HP</th>
                <th style="text-align: center;">Jabatan di kasn</th>
                <th style="text-align: center;">Jabatan di SK</th>
            </tr>
            <tr class="dt-search">
                <th></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search name..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search email..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search username..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search hp..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search jabatan di kasn..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search jabatan di sk..."/></th>
            </tr>
        </thead>
    </table>
</div>