<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/instansi_pemerintah/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$instansi_pemerintahChildren = [];
if (isset($model['instansi_pemerintah_child']))
    foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild)
        $instansi_pemerintahChildren[] = $instansi_pemerintahChild->attributes;

$this->registerJs(
    'vm.$data.instansi_pemerintah.virtual_category = ' . json_encode($model['user']->virtual_category) . ';' .
    'vm.$data.instansi_pemerintah.instansi_pemerintahChildren = vm.$data.instansi_pemerintah.instansi_pemerintahChildren.concat(' . json_encode($instansi_pemerintahChildren) . ');',
    // 'vm.$data.instansi_pemerintah.instansi_pemerintahChildren = Object.assign({}, vm.$data.instansi_pemerintah.instansi_pemerintahChildren, ' . json_encode($instansi_pemerintahChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['user']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}

/*if ($model['instansi_pemerintah_extend']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['instansi_pemerintah_extend'], ['class' => '']);
}

if (isset($model['instansi_pemerintah_child'])) foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild) {
    if ($instansi_pemerintahChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($instansi_pemerintahChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0"><?= $model['user']->attributeLabels()['name'] ?> : </div>
        <div class="box-11 m-padding-x-0 text-dark">
            <?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0"><?= $model['user']->attributeLabels()['email'] ?> : </div>
        <div class="box-11 m-padding-x-0 text-dark">
            <?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0"><?= $model['user']->attributeLabels()['username'] ?> : </div>
        <div class="box-11 m-padding-x-0 text-dark">
            <?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <div class="margin-top-15"></div>

    <?= $form->field($model['user'], 'password', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password', ['class' => 'form-label', 'label' => 'Password Baru']); ?>
        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password')->end(); ?>
  
    <?= $form->field($model['user'], 'password_repeat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'form-label']); ?>
        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password_repeat', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'password_repeat')->end(); ?>

    <hr class="margin-y-15">

    <?php if ($errorMessage) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-wrapper clearfix">
        <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button border-azure bg-lightest text-azure']); ?>
        <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure bg-lightest text-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

</div>