<tbody>
                <?php foreach ($model['instansi_pemerintah_pusat'] as $key => $instansiPemerintah) : ?>
                    <?php 
                        if ($instansiPemerintah['nilai_akhir'] <= 174)  {
                            $color = 'red';
                            $kategori = 1;
                        }
                        elseif ($instansiPemerintah['nilai_akhir'] < 249)  {
                            $color = 'yellow';
                            $kategori = 2;
                        }
                        elseif ($instansiPemerintah['nilai_akhir'] < 324)  {
                            $color = 'green';
                            $kategori = 3;
                        }
                        else {
                            $color = 'azure';
                            $kategori = 4;
                        }
                    ?>
                    <tr class="text-<?= $color ?>">
                        <td><?= $instansiPemerintah['nama'] ?></td>
                        <td><?= $instansiPemerintah['nilai_akhir'] ?></td>
                        <td>
                            <div class="circle-icon bg-<?= $color ?> border-<?= $color ?>" style="width: 18px; height: 18px;"></div>
                            <span>Kategori <?= $kategori ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>