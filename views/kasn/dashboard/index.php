<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

// $this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBhx0Dgxc0tv-B2Dn0oCAc4bfKV32oGDyM&libraries=places', ['position' => 1]);
// $this->registerJsFile('@web/app/kasn/dashboard/index.js');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js', ['position' => 1]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js', ['position' => 1]);
$this->registerJsFile('@web/app/kasn/dashboard/datamaps.idn.min.js', ['position' => 1]);

$this->registerJsFile('@web/app/kasn/dashboard/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Instansi Pemerintah Daerah</h1>
    <div class="">
        <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load('current', {
            'packages':['geochart'],
            'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
          });
          google.charts.setOnLoadCallback(drawRegionsMap);

          function drawRegionsMap() {
            var data = google.visualization.arrayToDataTable([
              ['provinces', 'Popularity'],
              ['Jawa Barat', 200],
            ]);

            var options = {};

            var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

            chart.draw(data, options);
          }
        </script>
        <div id="regions_div" style="width: 900px; height: 500px;"></div> -->

        <!-- <div id="map1" style="height: 400px;" class="margin-y-5"></div> -->

        <div id="container" style="position: relative; width: 100%; height: 400px;"></div>
        <script>
            // console.log(<?= json_encode($model['label']) ?>);
            var map = new Datamap({
                scope: 'idn',
                element: document.getElementById('container'),
                setProjection: function (element) {
                    var projection = d3.geo.mercator()
                        .center([120,-7])
                        .rotate([0, 0])
                        .scale(1000)
                        // .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
                    var path = d3.geo.path()
                        .projection(projection);
      
                    return {path: path, projection: projection};
                },
                fills: {
                    kategori4: 'blue',
                    kategori3: 'green',
                    kategori2: 'yellow',
                    kategori1: 'red',
                    unknown: 'rgb(0,0,0)',
                    defaultFill: '#7d7d7d',
                },
                data: <?= json_encode($model['code']) ?>,
                // data: {
                    //'ID.AC': {fillKey: 'kategori4'}, //Aceh
                    //'ID.KI': {fillKey: 'kategori1'}, //Kalimantan Timur
                    //'ID.JR': {fillKey: 'kategori3'}, //Jawa Barat
                    //'ID.JT': {fillKey: 'kategori4'}, //Jawa Tengah
                    //'ID.BE': {fillKey: 'kategori4'}, //Bengkulu
                    //'ID.BT': {fillKey: 'kategori4'}, //Banten
                    //'ID.JK': {fillKey: 'kategori4'}, //Jakarta Raya
                    //'ID.KB': {fillKey: 'kategori4'}, //Kalimantan Barat
                    //'ID.LA': {fillKey: 'kategori2'}, //Lampung
                    //'ID.SL': {fillKey: 'kategori4'}, //Sumatera Selatan
                    //'ID.BB': {fillKey: 'kategori4'}, //Bangka-Belitung
                    //'ID.BA': {fillKey: 'kategori2'}, //Bali
                    //'ID.JI': {fillKey: 'kategori1'}, //Jawa Timur
                    //'ID.KS': {fillKey: 'kategori4'}, //Kalimantan Selatan
                    //'ID.NT': {fillKey: 'kategori4'}, //Nusa Tenggara Timur
                    //'ID.SE': {fillKey: 'kategori4'}, //Sulawesi Selatan
                    //'ID.SR': {fillKey: 'kategori3'}, //Sulawesi Barat
                    //'ID.KR': {fillKey: 'kategori3'}, //Kepulauan Riau
                    //'ID.GO': {fillKey: 'kategori4'}, //Gorontalo
                    //'ID.JA': {fillKey: 'kategori2'}, //Jambi
                    //'ID.KT': {fillKey: 'kategori4'}, //Kalimantan Tengah
                    //'ID.IB': {fillKey: 'kategori4'}, //Irian Jaya Barat
                    //'ID.SU': {fillKey: 'kategori4'}, //Sumatera Utara
                    //'ID.RI': {fillKey: 'kategori1'}, //Riau
                    //'ID.SW': {fillKey: 'kategori4'}, //Sulawesi Utara
                    //'ID.LA': {fillKey: 'kategori2'}, //Maluku Utara
                    //'ID.SB': {fillKey: 'kategori4'}, //Sumatera Barat
                    //'ID.YO': {fillKey: 'kategori4'}, //Yogyakarta
                    //'ID.MA': {fillKey: 'kategori2'}, //Maluku
                    //'ID.NB': {fillKey: 'kategori4'}, //Nusa Tenggara Barat
                    //'ID.SG': {fillKey: 'kategori4'}, //Sulawesi Tenggara
                    //'ID.ST': {fillKey: 'kategori4'}, //Sulawesi Tengah
                    //'ID.PA': {fillKey: 'kategori2'}, //Papua
                // }
            });
            // map.labels({'customLabelText': <?= json_encode($model['label']) ?>});
        </script>
    </div>
</div>

<div class="margin-top-30"></div>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Instansi Pemerintah Pusat</h1>
    <div class="">
        <table class="datatables table table-striped table-hover table-condensed table-bordered">
            <thead>
                <tr class="bg-azure border-azure fs-14">
                    <th style="text-align: center;">Nama Instansi</th>
                    <th style="text-align: center;">Total Skor</th>
                    <th style="text-align: center;">Kategori</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
