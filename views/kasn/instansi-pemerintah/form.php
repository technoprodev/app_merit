<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChoicesAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);

$this->registerJsFile('@web/app/kasn/instansi-pemerintah/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

/*technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$instansi_pemerintahChildren = [];
if (isset($model['instansi_pemerintah_child']))
    foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild)
        $instansi_pemerintahChildren[] = $instansi_pemerintahChild->attributes;*/

$this->registerJs(
    'vm.$data.instansi_pemerintah.jenis = "' . $model['instansi_pemerintah']->jenis . '";',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['instansi_pemerintah']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['instansi_pemerintah'], ['class' => '']);
}

/*if ($model['instansi_pemerintah_extend']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['instansi_pemerintah_extend'], ['class' => '']);
}

if (isset($model['instansi_pemerintah_child'])) foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild) {
    if ($instansi_pemerintahChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($instansi_pemerintahChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?= $form->field($model['instansi_pemerintah'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['instansi_pemerintah'], 'nama', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['instansi_pemerintah'], 'nama', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['instansi_pemerintah'], 'nama', ['class' => 'form-info']); ?>
    <?= $form->field($model['instansi_pemerintah'], 'nama')->end(); ?>

    <?= $form->field($model['instansi_pemerintah'], 'jenis', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['instansi_pemerintah'], 'jenis', ['class' => 'form-label']); ?>
        <?= Html::activeRadioList($model['instansi_pemerintah'], 'jenis', $model['instansi_pemerintah']->getEnum('jenis'), ['class' => 'form-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, []) ? 'disabled' : '';
                return "<label><input type='radio' name='$name' value='$value' $checked $disabled v-model='instansi_pemerintah.jenis'>$label</label>";
            }]); ?>
        <?= Html::error($model['instansi_pemerintah'], 'jenis', ['class' => 'form-info']); ?>
    <?= $form->field($model['instansi_pemerintah'], 'jenis')->end(); ?>

    <?= $form->field($model['instansi_pemerintah'], 'virtual_logo_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['instansi_pemerintah'], 'virtual_logo_upload', ['class' => 'form-label f-bold margin-top-5']); ?>
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            <div class="form-text rounded-xs">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"><a href="<?= $model['instansi_pemerintah']->virtual_logo_download ?>" target="_blank"><?= $model['instansi_pemerintah']->logo ?></a></span>
            </div>
            <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <?= Html::activeFileInput($model['instansi_pemerintah'], 'virtual_logo_upload'); ?>
            </span>
        </div>
        <?= Html::error($model['instansi_pemerintah'], 'virtual_logo_upload', ['class' => 'form-info']); ?>
    <?= $form->field($model['instansi_pemerintah'], 'virtual_logo_upload')->end(); ?>

    <div v-show="instansi_pemerintah.jenis == 'Provinsi' || instansi_pemerintah.jenis == 'Kabupaten' || instansi_pemerintah.jenis == 'Kota'">
    <?= $form->field($model['instansi_pemerintah'], 'id_provinces', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['instansi_pemerintah'], 'id_provinces', ['class' => 'form-label']); ?>
        <?= Html::activeDropDownList($model['instansi_pemerintah'], 'id_provinces', ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown input-choices']); ?>
        <?= Html::error($model['instansi_pemerintah'], 'id_provinces', ['class' => 'form-info']); ?>
    <?= $form->field($model['instansi_pemerintah'], 'id_provinces')->end(); ?>
    </div>

    <div v-show="instansi_pemerintah.jenis == 'Kabupaten' || instansi_pemerintah.jenis == 'Kota'">
    <?= $form->field($model['instansi_pemerintah'], 'id_regencies', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['instansi_pemerintah'], 'id_regencies', ['class' => 'form-label']); ?>
        <?= Html::activeDropDownList($model['instansi_pemerintah'], 'id_regencies', ArrayHelper::map(\technosmart\modules\location\models\Regencies::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown input-choices']); ?>
        <?= Html::error($model['instansi_pemerintah'], 'id_regencies', ['class' => 'form-info']); ?>
    <?= $form->field($model['instansi_pemerintah'], 'id_regencies')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <?php if ($errorMessage) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-wrapper clearfix">
        <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button border-azure bg-lightest text-azure']); ?>
        <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure bg-lightest text-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

</div>