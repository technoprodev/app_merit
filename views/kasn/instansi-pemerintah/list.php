<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kasn/instansi-pemerintah/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <div class="clearfix">
        <h1 class="text-cyan margin-bottom-30 pull-left m-pull-none">
            <?= $title ?>
        </h1>
        <?php if ($this->context->can('create')): ?>
            <?= Html::a('Pendaftaran Instansi Pemerintah Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none margin-y-20 m-button-block']) ?>
        <?php endif; ?>
    </div>

    <table class="datatables table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr class="bg-azure border-azure fs-14">
                <th style="text-align: center;">Action</th>
                <th style="text-align: center;">Nama</th>
                <th style="text-align: center;">Jenis</th>
                <th style="text-align: center;">Provinsi</th>
                <th style="text-align: center;">Kota/Kabupaten</th>
                <th style="text-align: center;">Waktu Terakhir Update</th>
            </tr>
            <tr class="dt-search">
                <th></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search instansi..."/></th>
                <th>
                    <select class="form-dropdown border-none padding-0">
                        <option value="">all</option>
                        <?php foreach ((new \app_merit\models\InstansiPemerintah)->getEnum('jenis') as $key => $value) : ?>
                            <option value="<?= $value ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </th>
                <th>
                    <select class="form-dropdown border-none padding-0">
                        <option value="">all</option>
                        <?php foreach (ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->indexBy('id')->asArray()->all(), 'id', 'name') as $id => $name) : ?>
                            <option value="<?= $name ?>"><?= $name ?></option>
                        <?php endforeach; ?>
                    </select>
                </th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search kota/kabupaten..."/></th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>