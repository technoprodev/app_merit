<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['nama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['jenis'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->jenis ? $model['instansi_pemerintah']->jenis : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['id_provinces'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->id_provinces ? $model['instansi_pemerintah']->provinces->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['id_regencies'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->id_regencies ? $model['instansi_pemerintah']->regencies->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    <hr class="margin-y-15">
    <div class="form-group clearfix">
        <?= Html::a('Update', ['update', 'id' => $model['instansi_pemerintah']->id], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>&nbsp;
        <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure text-azure hover-bg-azure pull-right']) ?>
    </div>
</div>
<?php endif; ?>