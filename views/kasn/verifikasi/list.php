<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kasn/verifikasi/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
// ddx((new \app_merit\models\Penilaian)->getEnum('status'));
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Verifikasi</h1>
    <div class="">
        <table class="datatables table table-striped table-hover table-condensed table-bordered">
            <thead>
                <tr class="bg-azure border-azure fs-14">
                    <th style="text-align: center;">Nama</th>
                    <th style="text-align: center;">Jenis</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">BA Penilaian</th>
                    <th style="text-align: center;">Verifikasi</th>
                    <th style="text-align: center;">BA Verifikasi</th>
                    <th style="text-align: center;">Rekomendasi</th>
                </tr>
                <tr class="dt-search">
                    <th><input type="text" class="form-text border-none padding-0" placeholder="search instansi..."/></th>
                    <th>
                        <select class="form-dropdown border-none padding-0">
                            <option value="">all</option>
                            <?php foreach ((new \app_merit\models\InstansiPemerintah)->getEnum('jenis') as $key => $value) : ?>
                                <option value="<?= $value ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </th>
                    <th>
                        <select class="form-dropdown border-none padding-0">
                            <option value="">all</option>
                            <?php foreach (['' => 'Belum Input', 'Sedang Input' => 'Sedang Input', 'Selesai Input' => 'Selesai Input'] as $key => $value) : ?>
                                <option value="<?= $value ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>
