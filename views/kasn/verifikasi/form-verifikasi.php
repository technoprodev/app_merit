<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$errorMessage = '';
$errorVue = false;
if ($model['penilaian_detail']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['penilaian_detail'], ['class' => '']);
}
?>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Aspek</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']->aspek->urutan ?>. <?= $model['indikator']->aspek->nama ?>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Indikator</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']->aspek->urutan ?>.<?= $model['indikator']->urutan ?>. <?= $model['indikator']->nama ?>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Kondisi Saat Ini</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['penilaian_detail']->kondisi_saat_ini ?>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Bobot</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']->bobot ?>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Nilai Tim Instansi</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?php if ($model['penilaian_detail']->nilai_tim_instansi) : ?>
            <?php
                if ($model['penilaian_detail']->nilai_tim_instansi == '1') {
                    $color = 'red';
                }
                else if ($model['penilaian_detail']->nilai_tim_instansi == '2') {
                    $color = 'yellow';
                }
                else if ($model['penilaian_detail']->nilai_tim_instansi == '3') {
                    $color = 'spring';
                }
                else if ($model['penilaian_detail']->nilai_tim_instansi == '4') {
                    $color = 'azure';
                }
            ?>
            <span class="padding-y-2 padding-x-5 margin-right-5 bg-<?= $color?>"><?= $model['penilaian_detail']->nilai_tim_instansi ?></span>
            <?php $indikator = $model['penilaian_detail']->nilai_tim_instansi ?>
            <?= $model['indikator']->$indikator ?>
        <?php endif; ?>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Bukti</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?php if ($model['penilaian_detail']->bukti_wajib) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_wajib_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-azure">wajib</span><?= $model['penilaian_detail']->bukti_wajib ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_1) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_1_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+1</span><?= $model['penilaian_detail']->bukti_tambahan_1 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_2) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_2_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+2</span><?= $model['penilaian_detail']->bukti_tambahan_2 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_3) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_3_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+3</span><?= $model['penilaian_detail']->bukti_tambahan_3 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_4) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_4_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+4</span><?= $model['penilaian_detail']->bukti_tambahan_4 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_5) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_5_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+5</span><?= $model['penilaian_detail']->bukti_tambahan_5 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_6) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_6_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+6</span><?= $model['penilaian_detail']->bukti_tambahan_6 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_7) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_7_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+7</span><?= $model['penilaian_detail']->bukti_tambahan_7 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_8) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_8_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+8</span><?= $model['penilaian_detail']->bukti_tambahan_8 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_9) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_9_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+9</span><?= $model['penilaian_detail']->bukti_tambahan_9 ?></a>
            </div>
        <?php endif; ?>
        <?php if ($model['penilaian_detail']->bukti_tambahan_10) : ?>
            <div class="margin-bottom-2">
                <a class="margin-right-5" href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_10_download ?>" target="_blank"><span class="padding-y-2 padding-x-5 margin-right-5 bg-cyan">+10</span><?= $model['penilaian_detail']->bukti_tambahan_10 ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?= $form->field($model['penilaian_detail'], 'nilai_tim_verifikasi', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left">
            <?= Html::activeLabel($model['penilaian_detail'], 'nilai_tim_verifikasi', ['class' => 'form-label fw-bold']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeRadioList($model['penilaian_detail'], 'nilai_tim_verifikasi',
                \Yii::$app->db->createCommand(
                    "SELECT s.1, s.2, s.3, s.4 FROM indikator s
                    WHERE
                        s.id = :id"
                )->bindValue(':id', $model['indikator']['id'])->queryOne(),
                [
                    'class' => 'form-radio', 
                    'unselect' => null,
                    'item' => function($index, $label, $name, $checked, $value) {
                        $checked = $checked ? 'checked' : '';
                        $disabled = in_array($value, []) ? 'disabled' : '';
                        return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                    },
                ]
            ); ?>
            <?= Html::error($model['penilaian_detail'], 'nilai_tim_verifikasi', ['class' => 'form-info']); ?>
        </div>
    <?= $form->field($model['penilaian_detail'], 'nilai_tim_verifikasi')->end(); ?>
  
    <?= $form->field($model['penilaian_detail'], 'catatan', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-top-5">
            <?= Html::activeLabel($model['penilaian_detail'], 'catatan', ['class' => 'form-label fw-bold']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextArea($model['penilaian_detail'], 'catatan', ['class' => 'form-text', 'maxlength' => true]); ?>
            <?= Html::error($model['penilaian_detail'], 'catatan', ['class' => 'form-info']); ?>
        </div>
    <?= $form->field($model['penilaian_detail'], 'catatan')->end(); ?>


    <?php if ($errorMessage) : ?>
    <div class="form-group form-group-sm box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left">
        </div>
        <div class="box-10 m-padding-x-0">
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="form-group form-group-sm box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left">
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>