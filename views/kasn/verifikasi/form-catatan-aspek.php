<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$errorMessage = '';
$errorVue = false;
if ($model['penilaian_aspek']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['penilaian_aspek'], ['class' => '']);
}
?>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Aspek</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['aspek']->urutan ?>. <?= $model['aspek']->nama ?>
    </div>
</div>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?= $form->field($model['penilaian_aspek'], 'catatan', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-top-5">
            <?= Html::activeLabel($model['penilaian_aspek'], 'catatan', ['class' => 'form-label fw-bold']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextArea($model['penilaian_aspek'], 'catatan', ['class' => 'form-text', 'maxlength' => true]); ?>
            <?= Html::error($model['penilaian_aspek'], 'catatan', ['class' => 'form-info']); ?>
        </div>
    <?= $form->field($model['penilaian_aspek'], 'catatan')->end(); ?>


    <?php if ($errorMessage) : ?>
    <div class="form-group form-group-sm box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left">
        </div>
        <div class="box-10 m-padding-x-0">
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="form-group form-group-sm box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left">
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>