<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Verifikasi</h1>
    
    <div class="">
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-default" style="border-bottom: 1px; width: 1px;">No</th>
                    <th class="text-dark f-default" style="border-bottom: 1px;">Nama Instansi</th>
                    <th class="text-dark f-default" style="border-bottom: 1px">Status</th>
                    <th class="text-dark f-default" style="border-bottom: 1px">Action</th>
                    </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Kementrian Tenaga Kerja</td>
                    <td>Belum Input</td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Kementrian PU</td>
                    <td>Sedang Input</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Kementrian Komunikasi & Informasi</td>
                    <td>Selesai Input</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Kementrian Olahraga & Pemuda</td>
                    <td>Selesai Upload BA Penilaian</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Download BA Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Verifikasi</a>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Kementrian Perikanan</td>
                    <td>Sudah Diverifikasi</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Download BA Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Print BA Verifikasi</a>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Kementrian Perindustrian</td>
                    <td>Sudah Print BA Verifikasi</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Download BA Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Print BA Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Upload BA Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Upload Rekomendasi</a>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Kementrian Sosial</td>
                    <td>Sudah upload BA Verifikasi</td>
                    <td>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Lihat Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Download BA Penilaian</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Print BA Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Upload BA Verifikasi</a>
                        <a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Upload Rekomendasi</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
