<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <div class="clearfix">
        <h1 class="text-cyan margin-bottom-30 pull-left m-pull-none">
            Formulir Penilaian oleh Tim Verifikasi
        </h1>
        <?php if ($this->context->can('create')): ?>
            <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure bg-lightest hover-bg-azure text-azure pull-right m-pull-none margin-y-20 m-button-block']) ?>
        <?php endif; ?>
    </div>

    <div class="box box-break-sm margin-bottom-10 fs-16">
        <div class="box-1 padding-x-0">Instansi : </div>
        <div class="box-11 m-padding-x-0 text-dark fw-bold">
            <?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '<span class="text-gray f-italic">(kosong)</span>' ?>
        </div>
    </div>

    <div class="margin-top-30"></div>

    <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr class="bg-azure border-azure fs-14">
                <th>No</th>
                <th>Aspek</th>
                <th>Indikator</th>
                <th>Kondisi Saat Ini</th>
                <th>Bobot</th>
                <th>Nilai Tim Instansi</th>
                <th>Nilai Tim Verifikasi</th>
                <th>Nilai Akhir</th>
                <th>Catatan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $bobotTotal = 0;
                $nilaiTimInstansiTotal = 0;
                $nilaiTimVerifikasiTotal = 0;
                $nilaiAkhirTotal = 0;
            ?>
            <?php foreach($model['aspek'] as $keyAspek => $aspek) : ?>
                <?php
                    $bobot = 0;
                    $nilaiTimInstansi = 0;
                    $nilaiTimVerifikasi = 0;
                    $nilaiAkhir = 0;
                ?>
                <?php foreach($aspek['indikators'] as $keyIndikator => $indikator) : ?>
                    <?php
                        $bobot += (float)$indikator['bobot'];
                        $nilaiTimInstansi += (int)$indikator['nilai_tim_instansi'];
                        $nilaiTimVerifikasi += (int)$indikator['nilai_tim_verifikasi'];
                        $nilaiAkhir += ((float)$indikator['bobot'] * (int)$indikator['nilai_tim_verifikasi']);
                    ?>
                    <tr>
                        <?php if ($indikator['urutan'] == 1) : ?>
                            <td><?= $aspek['urutan'] ?>.</td>
                            <td class="f-bold"><?= $aspek['nama'] ?></td>
                        <?php else : ?>
                            <td></td>
                            <td></td>
                        <?php endif; ?>
                        <td><?= $indikator['urutan'] ?>. <?= $indikator['nama'] ?></td>
                        <td class="text-center">
                            <?= $indikator['kondisi_saat_ini'] ? Html::a('detail', ['kondisi-saat-ini', 'id' => $indikator['idPenilaianDetail']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Kondisi Saat Ini']) : '' ?></td>
                        <td class="text-center text-grayest fs-15"><?= $indikator['bobot'] ?></td>
                        <td class="text-center text-orange fs-15">
                            <?= $indikator['nilai_tim_instansi'] ?>
                            <br>
                            <?= $indikator['nilai_tim_instansi'] ? Html::a('detail', ['nilai-tim-instansi', 'id' => $indikator['id'], 'field' => $indikator['nilai_tim_instansi']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Skor']) : '' ?></td>
                        <td class="text-center text-azure fs-15 fw-bold">
                            <?= $indikator['nilai_tim_verifikasi'] ?>
                            <br>
                            <?= $indikator['nilai_tim_verifikasi'] ? Html::a('detail', ['nilai-tim-verifikasi', 'id' => $indikator['id'], 'field' => $indikator['nilai_tim_verifikasi']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Nilai Tim Verifikasi']) : '' ?></td>
                        <td class="text-center text-azure fs-18 fw-bold"><?= ((float)$indikator['bobot'] * (int)$indikator['nilai_tim_verifikasi']) ?></td>
                        <td class="text-center">
                            <?= $indikator['catatan'] ? Html::a('detail', ['catatan', 'id' => $indikator['idPenilaianDetail']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Catatan']) : '' ?></td>
                        <td class="text-center">
                            <?php if ($indikator['nilai_tim_instansi']) : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/form-verifikasi', 'idIndikator' => $indikator['id'], 'idInstansiPemerintah' => $idInstansiPemerintah]) ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi"><?= $indikator['nilai_tim_verifikasi'] ? 'Input' : 'Ubah' ?> Verifikasi</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php
                    $bobotTotal += $bobot;
                    $nilaiTimInstansiTotal += $nilaiTimInstansi;
                    $nilaiTimVerifikasiTotal += $nilaiTimVerifikasi;
                    $nilaiAkhirTotal += $nilaiAkhir;
                ?>
                <tr>
                    <td class="bg-light-orange"></td>
                    <td class="bg-light-orange" colspan="2">
                        <div>
                            Catatan KASN untuk Aspek <?= $aspek['urutan'] ?> : 
                        </div>
                        <div class="fw-bold fs-14">
                            <?= $aspek['catatanAspek'] ? $aspek['catatanAspek'] : '(kosong)' ?>
                        </div>
                    </td>
                    <!-- <td class="bg-light-orange"></td> -->
                    <td class="bg-light-orange"></td>
                    <td class="bg-light-orange"></td>
                    <td class="bg-light-orange"></td>
                    <!-- <td class="bg-light-orange text-center text-grayest fs-15"><?= $bobot ?></td> -->
                    <!-- <td class="bg-light-orange text-center text-orange fs-15"><?= $nilaiTimInstansi ?></td> -->
                    <!-- <td class="bg-light-orange text-center text-azure fs-15 fw-bold"><?= $nilaiTimVerifikasi ?></td> -->
                    <td class="bg-light-orange text-center">Total</td>
                    <td class="bg-light-orange text-center text-azure fs-18 fw-bold"><?= $nilaiAkhir ?></td>
                    <td class="bg-light-orange"></td>
                    <!-- <td class="bg-light-orange"></td>
                        <?= $aspek['catatanAspek'] ? Html::a('detail', ['catatan-aspek', 'id' => $aspek['idPenilaianAspek']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Catatan Aspek']) : '' ?></td>
                    </td> -->
                    <td class="bg-light-orange text-center">
                        <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/form-catatan-aspek', 'idAspek' => $aspek['id'], 'idInstansiPemerintah' => $idInstansiPemerintah]) ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-md="" modal-title="Form Catatan Aspek">Input Catatan</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
                <!-- <td class="bg-orange text-center fs-15"><?= $bobotTotal ?></td> -->
                <!-- <td class="bg-orange text-center fs-15"><?= $nilaiTimInstansiTotal ?></td> -->
                <!-- <td class="bg-orange text-center fs-15 fw-bold"><?= $nilaiTimVerifikasiTotal ?></td> -->
                <td class="bg-orange text-center">Total Akhir</td>
                <td class="bg-orange text-center fs-18 fw-bold"><?= $nilaiAkhirTotal ?></td>
                <td class="bg-orange"></td>
                <td class="bg-orange"></td>
            </tr>
        </tbody>
    </table>
</div>