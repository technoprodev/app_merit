<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);

$errorMessage = '';
$errorVue = false;
if ($model['penilaian']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['penilaian'], ['class' => '']);
}
?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['nama'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['jenis'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->jenis ? $model['instansi_pemerintah']->jenis : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['id_provinces'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->id_provinces ? $model['instansi_pemerintah']->provinces->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['instansi_pemerintah']->attributeLabels()['id_regencies'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark">
        <?= $model['instansi_pemerintah']->id_regencies ? $model['instansi_pemerintah']->regencies->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

<?= $form->field($model['penilaian'], 'virtual_rekomendasi_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        <div class="form-text rounded-xs">
            <i class="glyphicon glyphicon-file fileinput-exists"></i>
            <span class="fileinput-filename"><a href="<?= $model['penilaian']->virtual_rekomendasi_download ?>" target="_blank"><?= $model['penilaian']->rekomendasi ?></a></span>
        </div>
        <span class="input-group-addon btn btn-default btn-file">
            <span class="fileinput-new">Select file</span>
            <span class="fileinput-exists">Change</span>
            <?= Html::activeFileInput($model['penilaian'], 'virtual_rekomendasi_upload'); ?>
        </span>
    </div>
    <?= Html::error($model['penilaian'], 'virtual_rekomendasi_upload', ['class' => 'form-info']); ?>
<?= $form->field($model['penilaian'], 'virtual_rekomendasi_upload')->end(); ?>

<?php if ($errorMessage) : ?>
<div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
    <?= $errorMessage ?>
</div>
<?php endif; ?>

<div class="clearfix">
    <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure button-block button-lg']) ?>
</div>

<?php ActiveForm::end(); ?>