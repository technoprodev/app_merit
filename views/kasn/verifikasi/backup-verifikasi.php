<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Formulir Penilaian oleh Tim Verifikasi</h1>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left padding-y-5">
            <label class="control-label">Nama Instansi</label>
        </div>
        <div class="box-10 m-padding-x-0 f-bold">
            <div class="padding-y-5">Kementrian Ketenagakerjaan</div>
        </div>
    </div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left padding-y-5">
            <label class="control-label">Tim Verifikasi</label>
        </div>
        <div class="box-10 m-padding-x-0">
            <div class="padding-y-5"><b>Pak Hayun</b> (081253234213) <br>Ketua</div>
            <div class="padding-y-5"><b>Pak Lian</b> (081253234213) <br>Wakil Ketua</div>
            <div class="padding-y-5"><b>Pak Asep</b> (081253234213) <br>Wakil Ketua</div>
        </div>
    </div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left padding-y-5">
            <label class="control-label">Tanggal</label>
        </div>
        <div class="box-10 m-padding-x-0 f-bold">
            <div class="padding-y-5">23 Mei 2018</div>
        </div>
    </div>

    <table class="table table-striped table-hover table-condensed">
        <thead>
            <tr role="row">
                <th class="text-dark f-default" style="border-bottom: 1px; width: 1px;">No</th>
                <th class="text-dark f-default" style="border-bottom: 1px;">Aspek</th>
                <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                <th class="text-dark f-default" style="border-bottom: 1px">Nilai Tim Instansi</th>
                <th class="text-dark f-default" style="border-bottom: 1px">Nilai Tim Verifikasi</th>
                <th class="text-dark f-default" style="border-bottom: 1px">Catatan</th>
                <th class="text-dark f-default" style="border-bottom: 1px"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1.</td>
                <td class="f-bold">Perencanaan Kebutuhan Pegawai</td>
                <td>1. Ketersediaan peta jabatan dan  rencana kebutuhan pegawai untuk jangka menengah (5 tahun) yang sudah ditetapkan PPK</td>
                <td class="text-yellow">5</td>
                <td class="text-azure">4</td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2.  Ketersediaan data kepegawaiansecara lengkap dan  real time yang disusun menurut jabatan, pangkat, unit kerja, kualifikasi dan kompetensi;</td>
                <td class="text-yellow">5</td>
                <td class="text-azure">3</td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3.  Ketersediaan data pegawai yang akan memasuki masa pensiun dalam 5 tahun yang disusun menurut jabatan, pangkat, unit kerja;</td>
                <td class="text-yellow">5</td>
                <td class="text-azure">4</td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4.  Ketersediaan rencana pengadaan untuk memenuhi kebutuhan ASN untuk jangka menengah (5 tahun) yang berasal dari CALON PNS, PPPK ataupun PNS dari instansi lain;</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td class="f-bold">Pengadaan</td>
                <td>1. Ketersediaan rencana pengadaan ASN untuk tahun berjalan yang dirinci menurut jumlah, jenis jabatan, pangkat, kualifikasi, kompetensi, dan unit kerja, baik dari CALON PNS, PPPK, ataupun PNS dari instansi lainserta TNI/Polri (untuk instansi tertentu)</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Ketersediaan kebijakan internal (Permen/Pergub / Perbup/Perwal) terkait pengadaan ASN  dan TNI/Polri (untuk instansi tertentu) secara terbuka, kompetitif, transparan dan tidak diskriminatif</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Pelaksanaan penerimaan CALON PNS/PPPK/ PNS dari instansi lain dilakukan  secara terbuka</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4. Pelaksanaan LPJ (Latihan Pra Jabatan ) bagi CALON PNS</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>5. Persentase  pegawai yang baru diangkat menjadi PNS yang  penempatan pertamanya sesuai  jabatan yang dilamar</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>3.</td>
                <td class="f-bold">Pembinaan Karir dan Peningkatan Kompetensi</td>
                <td>1. Ketersediaan standar kompetensi manajerial, bidang, dan sosio kultural untuk setiap jabatan</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Ketersediaan profil pegawai yang disusun berdasarkan pemetaan talenta/kompetensi</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Ketersediaan talent pool dan rencana suksesi yang disusun berdasarkan profil ASN (kualifikasi, kompetensi, dan kinerja) dengan mempertimbangkan pola karir instansi</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4. Ketersediaan informasi tentang kesenjangan kualifikasi dan kompetensi pegawai</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>5. Ketersediaan informasi tentang kesenjangan kinerja</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>6. Ketersediaan strategi dan program untuk mengatasi kesenjangan kompetensi dan kinerja dalam rangka peningkatan kapasitas ASN</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>7. Penyelenggaraan Diklat yang dalam rangka mengatasi kesenjangan</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>8. Pelaksanaan  peningkatan kompetensi melalui praktik kerja dan pertukaran pegawai</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>9. Pelaksanaan  peningkatan kompetensi melalui coaching, counseling dan mentoring</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>4.</td>
                <td class="f-bold">Mutasi, Rotasi dan Promosi</td>
                <td>1. Penyusunan dan penetapan kebijakan internal (Permen/Pergub/Perbup/Perwal) tentang pola karier</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Ketersediaan kebijakan internal (Permen/Pergub/Perbup/Perwali)tentang mutasi, rotasi  dan promosi secara obyektif dan transparan dengan mengacu kepada rencana suksesi</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Pelaksanaan kebijakan pengisian JPT, Administrator dan Pengawas secara terbuka dan kompetitif </td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>5.</td>
                <td class="f-bold">Manajemen Kinerja</td>
                <td>1. Penyusunan kontrak kinerja yang terukur</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Penerapan metode penilaian kinerja yang obyektif dan terukur</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Pelaksanaan penilaian kinerja secara berkala untuk memastikan tercapainya kontrak kinerja</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4. Ketersediaan informasi dan strategi untuk mengatasi  permasalahan kinerja</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>5. Penggunaan hasil penilaian kinerja sebagai dasar bagi penentuan keputusan manajemen terkait pembinaan dan pengembangan karier (promosi, mutasi, demosi, rotasi, diklat)</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>6.</td>
                <td class="f-bold">Penggajian, Penghargaan dan Disiplin</td>
                <td>1. Kebijakan pembayaran tunjangan kinerja dikaitkan dengan hasil penilaian kinerja</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Ketersediaan  kebijakan internal (Permen/Pergub/Perbup/Perwali) untuk memberi  penghargaan yang bersifat finansial dan non-finansial terhadap pegawai berprestasi luar biasa</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Penegakan  kode etik dan kode perilaku ASN di lingkungan instansinya</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4. Pengelolaan data terkait pelanggaran disiplin, pelanggaran kode etik dan kode perilaku yang dilakukan pegawai</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>7.</td>
                <td class="f-bold">Perlindungan dan Pelayanan ASN</td>
                <td>1. Kebijakan perlindungan pegawai diluar dari jaminan kesehatan, jaminan kecelakaan kerja, program pensiun, yang diselenggarakan  secara nasional</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Penyediaan kemudahan bagi pegawai yang membutuhkan pelayanan administrasi</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td>8.</td>
                <td class="f-bold">Sistem Pendukung</td>
                <td>1. Pembangunan Sistem Informasi Kepegawaian yang berbasis online  yang terintegrasi dengan sistem penilaian kinerja, penegakan disiplin dan pembinaan pegawai</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>2. Penerapan e-performance yang terintegrasi dengan  Sistem Informasi Kepegawaian yang berbasis online</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>3. Penggunaan e-office yang memudahkan pelayanan administrasi kepegawaian</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>4. Pembangunan dan penggunaan asessment center dalam pemetaan kompetensi dan pengisian jabatan</td>
                <td class="text-yellow">5</td>
                <td class="text-azure"></td>
                <td class="text-grayest"></td>
                <td>
                    <a href="<?= Yii::$app->urlManager->createUrl("kasn/verifikasi/form-verifikasi") ?>" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure" modal-lg="" modal-title="Form Verifikasi">Input Verifikasi</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>