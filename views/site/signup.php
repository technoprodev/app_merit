<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['user']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="50px;" class="bg-lightest shadow circle padding-5">
</div>
<div class="margin-top-20 border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-azure text-center fs-18">
        <span class="f-italic"><?= $this->title ?></span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'app']); ?>

        <?= $form->field($model['user'], 'username')->begin(); ?>
            <?= Html::activeLabel($model['user'], 'username', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['user'], 'username', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['user'], 'username')->end(); ?>

        <?= $form->field($model['user'], 'email')->begin(); ?>
            <?= Html::activeLabel($model['user'], 'email', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['user'], 'email', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['user'], 'email')->end(); ?>

        <?= $form->field($model['user'], 'password')->begin(); ?>
            <?= Html::activeLabel($model['user'], 'password', ['class' => 'control-label']); ?>
            <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['user'], 'password', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['user'], 'password')->end(); ?>

        <?= $form->field($model['user'], 'password_repeat')->begin(); ?>
            <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'control-label']); ?>
            <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['user'], 'password_repeat', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['user'], 'password_repeat')->end(); ?>

        <?php if ($error) : ?>
            <div class="alert alert-danger">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Signup', ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="bg-lighter padding-y-10">
        <div class="text-center text-azure fs-18"><?= Yii::$app->params['app.name'] ?></div>
        <div class="text-center fs-12 margin-top-2"><?= Yii::$app->params['app.author'] ?> © <?= date('Y') ?></div>
    </div>
</div>