<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartPieLabelAsset::register($this);

$error = false;
$errorMessage = '';
if ($model['login']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}
?>

<div class="clearfix">
    <div class="border-azure border-thin rounded-md padding-30 has-bg-img pull-left m-pull-none m-margin-x-auto" style="width:100%;max-width:300px;">
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>

        <?php $form = ActiveForm::begin(['id' => 'app']); ?>
            <?= $form->field($model['login'], 'login', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model['login'], 'login', ['class' => 'form-label']); ?>
                <?= Html::activeTextInput($model['login'], 'login', ['class' => 'form-text border-cyan', 'maxlength' => true]); ?>
                <?= Html::error($model['login'], 'login', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'login')->end(); ?>

            <?= $form->field($model['login'], 'password', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model['login'], 'password', ['class' => 'form-label']); ?>
                <?= Html::activePasswordInput($model['login'], 'password', ['class' => 'form-text border-cyan', 'maxlength' => true]); ?>
                <?= Html::error($model['login'], 'password', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'password')->end(); ?>

            <?= $form->field($model['login'], 'captcha', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model['login'], 'captcha', ['class' => 'form-label']); ?>
                <?= Captcha::widget(['model' => $model['login'], 'attribute' => 'captcha', 'captchaAction' => 'captcha/captcha', 'options' => ['class' => 'form-text border-cyan', 'maxlength' => true]]); ?>
                <?= Html::error($model['login'], 'captcha', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'captcha')->end(); ?>

            <?= $form->field($model['login'], 'rememberMe', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <div class="checkbox">
                    <?= Html::activeCheckbox($model['login'], 'rememberMe', ['uncheck' => 0]); ?>
                </div>
                <?= Html::error($model['login'], 'rememberMe', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'rememberMe')->end(); ?>

            <?php if ($error) : ?>
                <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div class="margin-top-30"></div>

            <div class="">
                <?= Html::submitButton('Login', ['class' => 'button button-block button-lg padding-x-30 border-azure bg-azure hover-bg-lightest hover-text-azure rounded-md']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    
    <div class="m-margin-top-30"></div>
    
    <?php if ($model['pie']) : ?>

    <div class="border-azure border-thin rounded-md padding-30 has-bg-img pull-right m-pull-none m-margin-x-auto" style="width:100%;max-width:300px;">
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>

        <h5 class="text-center">Hasil Penilaian Mandiri Seluruh Instansi</h5>

        <div>
            <canvas id="statistik" height="300"></canvas>
            <script>
                window.addEventListener('load', function() {
                    var ctx = document.getElementById('statistik').getContext('2d');
                    var config = {
                        type: 'horizontalBar',
                        data: {
                          labels: <?= json_encode($model['label']) ?>,
                          datasets: [{
                            data: <?= json_encode($model['data']) ?>,
                            backgroundColor: <?= json_encode($model['background']) ?>,
                            borderColor: <?= json_encode($model['border']) ?>,
                            borderWidth: 1,
                            // label: 'asdf',
                          }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        // display: false
                                    }
                                }]
                            },
                            maintainAspectRatio: false,
                            elements: {
                                line: {
                                    tension: 0, // disables bezier curves
                                }
                            },
                            animation: {
                                duration: 0, // general animation time
                            },
                            hover: {
                                animationDuration: 0, // duration of animations when hovering an item
                            },
                            responsiveAnimationDuration: 0, // animation duration after a resize
                            responsive: true,
                            legend: {
                                display: false
                            },
                            plugins: {
                                labels: {
                                    // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                                    render: function (args) {
                                        return args.label + ' ' + args.percentage + '%';
                                    },
                                    fontSize: 12,
                                    fontColor: <?= json_encode($model['border']) ?>,
                                },
                            },
                        }
                    };
                    window.statistik = new Chart(ctx, config);
                });
            </script>
        </div>
    </div>

    <?php endif; ?>
</div>

<div class="margin-top-30"></div>

<h3 class="bg-lightest text-azure padding-5 text-center rounded-md shadow m-margin-x-auto" style="width: 300px;">
    <span class="">Tim Pengembang</span>
</h3>
<div class="m-margin-x-auto" style="width: 300px;">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/1.png" width="90px;" class="circle padding-5 bg-light-orange margin-right-5">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/2.png" width="90px;" class="circle padding-5 bg-light-orange margin-right-5">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/31.png" width="90px;" class="circle padding-5 bg-light-orange margin-right-5">
</div>