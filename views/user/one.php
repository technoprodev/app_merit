<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['name'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->name) ? $model['user']->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['username'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->username) ? $model['user']->username : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['email'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->email) ? $model['user']->email : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['phone'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->phone) ? $model['user']->phone : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->created_at) ? $model['user']->created_at : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['user']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['user']->updated_at) ? $model['user']->updated_at : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div>
            <?= Html::a('Update', ['update', 'id' => $model['user']->id], ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= Html::a('Reset Password', ['reset-password', 'id' => $model['user']->id], ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>
            <?= Html::a('Kembali', ['index'], ['class' => 'btn text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>
