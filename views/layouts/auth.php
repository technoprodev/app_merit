<?php

use app_merit\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
        <style type="text/css">
            .bg-orange {
                background-color: #EF9C00;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper has-bg-img">
            <div class="bg-img bg-light"></div>
            <?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'login') : ?>
                <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/login.png');"></div>
            <?php else : ?>
                <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/login.png');"></div>
                <!-- <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/pastel-orange.png');"></div> -->
            <?php endif; ?>

            <div class="padding-y-5 clearfix text-center container" style="">
                <div class="pull-right padding-top-15">
                    <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-kasn.png" width="90px;" class="circle padding-5 bg-lightest text-middle shadow">
                    </a>
                </div>
                <div class="pull-left padding-top-15">
                    <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="90px;" class=""><!-- <div class="margin-top-5"></div> -->
                        <!-- <span class="fw-bold fs-18 text-middle bg-lightest padding-5 padding-x-15 rounded-md shadow"><?= Yii::$app->params['app.name'] ?></span> -->
                    </a>
                </div>
            </div>

            <div class="header bg-orange hidden-sm-less">
                <div class="container" style="">
                    <div class="pull-left padding-top-15">
                        <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                            <!-- <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle"> -->
                            <span class="fw-bold fs-20 text-middle text-uppercase"><?= Yii::$app->params['app.name'] ?></span>
                        </a>
                    </div>
                    <?php $menu['header'] = [
                        [
                            'code' => 'header',
                            'id' => '1',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-rocket margin-right-5"></i> Tentang Sipinter',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'home',
                            'url_action' => 'tentang',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],
                        [
                            'code' => 'header',
                            'id' => '2',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-tasks margin-right-5"></i> Panduan Penggunaan',
                            'icon' => null,
                            'url' => Yii::$app->getRequest()->getBaseUrl() . '/download/panduan1.pdf',
                            'url_controller' => null,
                            'url_action' => null,
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],
                        [
                            'code' => 'header',
                            'id' => '3',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<i class="fa fa-qrcode margin-right-5"></i> Kontak Kami',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'home',
                            'url_action' => 'kontak',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ],
                    ]; ?>
                    <?= MenuWidget::widget([
                        'items' => Menu::menuTree($menu['header'], null, true),
                        'options' => [
                            'class' => 'pull-right menu-x menu-space-y-20 fs-14
                                lighter-10 menu-hover-bg-lighter menu-hover-text-orange menu-active-bg-lightest menu-active-text-orange
                                submenu-text-orange submenu-hover-bg-lighter submenu-hover-text-orange submenu-active-bg-lightest submenu-active-text-orange',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                    <!-- <ul class="pull-right menu-x menu-space-y-20 fs-14
                        lighter-10 menu-hover-bg-lighter menu-hover-text-orange menu-active-bg-lightest menu-active-text-orange
                        submenu-text-orange submenu-hover-bg-lighter submenu-hover-text-orange submenu-active-bg-lightest submenu-active-text-orange">
                        <li class=""><a href="#faq">Tentang Sipinter</a></li>
                        <li class=""><a href="#faq">Panduan Penggunaan</a></li>
                        <li class=""><a href="#faq">Kontak Kami</a></li>
                    </ul> -->
                </div>
            </div>

            <div class="header visible-sm-less">
                <div class="text-center padding-y-10 bg-orange darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                        <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div>
                <div class="bg-orange text-center">
                    <?= MenuWidget::widget([
                        'items' => Menu::menuTree($menu['header'], null, true),
                        'options' => [
                            'class' => 'menu-x menu-space-y-10 fs-13 inline-block margin-bottom-min-5
                                lighter-10 menu-active-border-lightest
                                submenu-text-orange submenu-hover-bg-lighter submenu-hover-text-orange submenu-active-bg-lightest submenu-active-text-orange',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                    <!-- <ul class="menu-x menu-space-y-10 fs-13 inline-block margin-bottom-min-5
                        lighter-10 menu-active-border-lightest
                        submenu-text-orange submenu-hover-bg-lighter submenu-hover-text-orange submenu-active-bg-lightest submenu-active-text-orange">
                        <li class=""><a href="#faq">Tentang Sipinter</a></li> 
                        <li class=""><a href="#faq">Panduan Penggunaan</a></li> 
                        <li class=""><a href="#faq">Kontak Kami</a></li> 
                    </ul> -->
                </div>
            </div>

            <div class="body">
                <!-- <div class="bg-img lighter-50"></div> -->
                <!-- <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg-bintang.png');"></div> -->
                <!-- <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg-blue-v1.png');"></div> -->
                <div class="page-wrapper padding-y-50 container" style="">
                    <h1 class="text-center">
                        <span class="bg-lightest text-azure padding-5 padding-x-15 rounded-md inline-block shadow">Sistem Informasi Penilaian Mandiri Penerapan Sistem Merit (SIPINTER)</span>
                    </h1>
                    <div class="margin-top-50"></div>
                    <div class="">
                        <?= $content ?>
                    </div>
                    <div class="padding-top-30"></div>
                </div>
            </div>

            <!-- <footer class="footer padding-y-10 bg-darker">
                <div class="text-center fs-18">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-kasn.png" height="30px;" class="text-middle margin-right-5">
                    <span class="text-middle"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></span>
                </div>
            </footer> -->
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>