<?php

use app_merit\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");' .
        'setTimeout(function() {$("#modal-alert").modal("hide");}, 3000);',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");' .
        'setTimeout(function() {$("#modal-alert").modal("hide");}, 3000);',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");' .
        'setTimeout(function() {$("#modal-alert").modal("hide");}, 3000);',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");' .
        'setTimeout(function() {$("#modal-alert").modal("hide");}, 3000);',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
        <style type="text/css">
            /*.table tr:not(:first-child) {
              border-top: 1px solid #cce6ff;
            }

            .table thead tr:last-child {
              border-bottom: 1px solid #cce6ff;
            }

            .table tbody + tbody {
              border-top: 1px solid #cce6ff;
            }*/

            /*.table-bordered {
              border: 1px solid #3376b8;
            }

            .table-bordered th:not(:last-child),
            .table-bordered td:not(:last-child) {
              border-right: 1px solid #3376b8;
            }*/
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header bg-azure text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header bg-azure text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper has-bg-img">
            <style type="text/css">
                @keyframes blink {
                  50% {
                    opacity: 0.0;
                  }
                }
                @-webkit-keyframes blink {
                  50% {
                    opacity: 0.0;
                  }
                }
                .blink {
                  animation: blink 2s step-start 0s infinite;
                  -webkit-animation: blink 2s step-start 0s infinite;
                }
            </style>

            <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/pastel.png')"></div>

            <div class="body has-sidebar-left">

                <aside class="sidebar-left shadow-top-right" style="background-image:url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/sidebar.png');background-position:cover">
                    <?php
                        $cache = Yii::$app->cache;
                        $key = 'menu-kasn';
                        /*if ($cache->exists($key)) {
                            $cacheData = $cache->get($key);
                        } else {
                            $cacheData = [
                                'kasn' => technosmart\models\Menu::find()->where(['code' => 'kasn'])->asArray()->all(),
                            ];
                            $cache->set($key, $cacheData);
                        }*/
                        $cacheData['kasn'][] = [
                            'code' => 'kasn',
                            'id' => '1',
                            'parent' => null,
                            'order' => '0',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/dashboard.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Dashboard</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'kasn/dashboard',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['kasn'][] = [
                            'code' => 'kasn',
                            'id' => '2',
                            'parent' => null,
                            'order' => '1',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/verifikasi.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Verifikasi</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'kasn/verifikasi',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['kasn'][] = [
                            'code' => 'kasn',
                            'id' => '3',
                            'parent' => null,
                            'order' => '2',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/instansi.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Instansi</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'kasn/instansi-pemerintah',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['kasn'][] = [
                            'code' => 'kasn',
                            'id' => '4',
                            'parent' => null,
                            'order' => '3',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/profile.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Profil</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'kasn/profile',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['instansi-pemerintah'][] = [
                            'code' => 'kasn',
                            'id' => '11',
                            'parent' => null,
                            'order' => '4',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/dashboard.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Input Penilaian</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'instansi_pemerintah/dashboard',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['instansi-pemerintah'][] = [
                            'code' => 'kasn',
                            'id' => '12',
                            'parent' => null,
                            'order' => '5',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/penilaian-mandiri.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Hasil Penilaian</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'instansi_pemerintah/penilaian-mandiri',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        $cacheData['instansi-pemerintah'][] = [
                            'code' => 'kasn',
                            'id' => '13',
                            'parent' => null,
                            'order' => '6',
                            'enable' => '1',
                            'title' => '<img src="' . Yii::$app->getRequest()->getBaseUrl() . '/img/icon/profile.png" height="26px;" class="margin-right-5 text-middle"> <span class="text-middle">Profil</span>',
                            'icon' => null,
                            'url' => null,
                            'url_controller' => 'instansi_pemerintah/profile',
                            'url_action' => 'index',
                            'param_key_1' => null,
                            'param_value_1' => null,
                            'param_key_2' => null,
                            'param_value_2' => null,
                            'param_key_3' => null,
                            'param_value_3' => null,
                        ];
                        // ddx($cacheData['kasn']);
                    ?>

                    <?php
                        $menuKasn = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['kasn'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-rose menu-hover-darker-10 submenu-active-bg-light-blue lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        $menuInstansiPemerintah = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['instansi-pemerintah'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-rose menu-hover-darker-10 submenu-active-bg-light-blue lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $isKasn = false; ?>
                    <?php $isInstansiPemerintah = false; ?>
                    <?php if ($menuKasn && isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['kasn'])): ?>
                        <?php $isKasn = true; ?>
                    <?php endif; ?>
                    <?php if ($menuInstansiPemerintah && isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['instansi-pemerintah'])): ?>
                        <?php $isInstansiPemerintah = true; ?>
                    <?php endif; ?>

                    <div class="margin-top-15"></div>

                    <div class="padding-x-15">
                        
                    <?php if ($isKasn): ?>
                        <div class="padding-x-20 padding-y-10 fs-16 fw-bold bg-cyan text-lightest text-center has-bg-img">
                            <!-- <div class="bg-img darker-20"></div> -->
                            KASN
                        </div>
                        <?= $menuKasn ?>
                    <?php endif; ?>
                    <?php if ($isKasn && $isInstansiPemerintah): ?>
                        <hr>
                    <?php endif; ?>
                    <?php if ($isInstansiPemerintah): ?>
                        <div class="padding-x-20 padding-y-10 fs-16 fw-bold bg-cyan text-lightest text-center has-bg-img">
                            <!-- <div class="bg-img darker-20"></div> -->
                            Instansi Pemerintah
                        </div>
                        <?= $menuInstansiPemerintah ?>
                    <?php endif; ?>

                    </div>

                    <div class="margin-top-30"></div>

                    <div class="padding-x-15">
                        <!-- <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" height="26px;" class="text-middle margin-right-5"> -->
                        <!-- <span class="text-middle"><?= Yii::$app->params['app.name'] ?> © <?= date('Y') ?></span> -->
                        <div class="padding-x-20 padding-y-10 fs-16 fw-bold bg-azure text-lightest text-center has-bg-img">
                            <div class="bg-img darker-10 blink"></div>
                            <span class="">PENGUMUMAN</span><br>
                            <span class="">TAHAP 1</span>
                        </div>
                        <div class="padding-x-20 padding-y-10 lighter-50">
                            <div class="margin-top-15"></div>
                            <div class="text-center padding-x-5">
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/tahap1.png" style="width:100%" class="">
                            </div>
                        </div>
                        <div class="margin-top-30"></div>
                        <div class="padding-x-20 padding-y-10 fs-16 fw-bold bg-orange text-lightest text-center has-bg-img">
                            <div class="bg-img darker-10 blink"></div>
                            <span class="">PENGUMUMAN</span><br>
                            <span class="">TAHAP 2</span>
                        </div>
                        <div class="padding-x-20 padding-y-10 lighter-50">
                            <div class="margin-top-15"></div>
                            <div class="text-center padding-x-5">
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/tahap2.png" style="width:100%" class="">
                            </div>
                        </div>
                    </div>

                    <div class="margin-top-15"></div>

                    <div class="padding-x-50 text-center">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="100%" class="circle padding-15 bg-lightest">
                    </div>
                    <div class="margin-top-5"></div>
                    <div class="padding-x-15 text-center">
                        <span class="f-italic fs-26 text-uppercase fw-bold text-middle text-dark-cyan"><?= Yii::$app->params['app.name'] ?></span>
                    </div>

                    <div class="margin-top-5"></div>
                </aside>

                <div class="page-wrapper padding-x-30 m-padding-x-10 padding-y-20">
                    <div class="box box-gutter box-break-sm box-space-sm">
                        <div class="box-2 rounded-lg shadow text-center padding-y-15 bg-lightest border-cyan border-thin text-cyan has-bg-img" style="height: 58px;">
                            <div class="bg-img blink" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/pastel-ori.png')"></div>
                            <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" height="26px;" class="text-middle margin-right-5">
                                <span class="f-italic fs-21 text-uppercase fw-bold text-middle"><?= Yii::$app->params['app.name'] ?></span>
                            </a>
                        </div>
                        <div class="box-10 rounded-lg shadow bg-cyan" style="height: 58px;">
                            <div class="pull-left m-pull-none m-text-center padding-y-15 fs-21 fw-bold text-lightest"><?= Yii::$app->params['app.description'] ?></div>
                            
                            <?php if (!Yii::$app->user->isGuest) : ?>
                                <div class="pull-right m-pull-none m-text-center padding-y-15">
                                    <a class="button button-xs border-lightest bg-lightest text-cyan rounded-sm padding-x-15 fs-14 fw-bold" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="margin-top-15"></div>

                    <?= $content ?>

                    <div class="margin-top-30"></div>

                    <footer class="footer padding-y-10 rounded-lg shadow bg-cyan">
                        <div class="text-center fs-18">
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-kasn.png" height="50px;" class="text-middle margin-right-5">
                            <span class="text-middle"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></span>
                        </div>
                    </footer>
                </div>
            </div>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>