<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="clearfix">
    <div class="border-azure border-thin rounded-md padding-30 has-bg-img margin-x-auto" style="width:100%;max-width:600px;">
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>

        <h1 class="text-center text-azure">Tentang Sipinter</h1>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/info.png" width="100px;" class="">
        </div>
        <p class="fs-16 text-dark">
            Penilaian Mandiri Penerapan Sistem Merit merupakan penilaian yang dilakukan oleh Instansi secara mandiri terhadap tingkat penerapan sistem merit dalam manajemen ASN di Instansinya. 
        </p>
        <p class="fs-16 text-dark">
            Penilaian Mandiri Penerapan Sistem Merit dilaksanakan dengan tujuan:
        </p>
        <ul class="fs-15 text-dark">
            <li>Menjamin terwujudnya Sistem Merit dalam kebijakan dan Manajemen ASN di Instansi Pemerintah;</li>
            <li>Mendorong terwujudnya ASN yang profesional, bermartabat, berkinerja tinggi, sejahtera, dan berfungsi sebagai perekat Negara Kesatuan Republik Indonesia;</li>
            <li>Mendukung penyelenggaraan pemerintahan negara yang efektif, efisien dan terbuka, serta bebas dari praktik korupsi, kolusi, nepotisme, dan intervensi politik;</li>
        </ul>

        <p class="fs-16 text-dark">
            SIPINTER adalah Sistem Informasi yang digunakan oleh instansi pemerintah untuk menyampaikan hasil penilaian mandiri beserta buktinya ke KASN. Sistem informasi ini juga digunakan KASN untuk menyampaikan hasil verifikasi dan penilaian sistem merit ke instansi pemerintah.    
        </p>
    </div>
</div>