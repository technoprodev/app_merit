<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="clearfix">
    <div class="border-azure border-thin rounded-md padding-30 has-bg-img margin-x-auto" style="width:100%;max-width:600px;">
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>
        <div class="bg-img lighter-50"></div>

        <h1 class="text-center text-azure">Kontak Sipinter</h1>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/support.png" width="100px;" class="">
        </div>
        <p class="fs-16 text-dark">
            Silahkan hubungi kami untuk informasi lebih lanjut.
        </p>
        <ul class="fs-15 text-dark list-unstyled">
            <li>Komisi Aparatur Sipil Negara</li>
            <li>Jl. Letjend. M.T. Haryono Kav. 52-53 Pancoran, Jakarta Selatan</li>
            <li>Email : sipinter@kasn.go.id</li>
            <li>No. Telp : (021) 7972098</li>
            <li>Website : http://www.kasn.go.id</li>
        </ul>
    </div>
</div>