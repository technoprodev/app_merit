<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md has-bg-img">
    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg.png')"></div>
    <h1 class="text-cyan clearfix">
        <?php if ($model['instansi_pemerintah']->logo) : ?>
            <img src="<?= $model['instansi_pemerintah']->virtual_logo_download ?>" height="90px;" class="pull-left margin-right-5">
        <?php else : ?>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/instansi.png" height="50px;" class="pull-left margin-right-5">
        <?php endif; ?>
        <span class="pull-left margin-right-5 margin-top-15"><?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '' ?></span>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/asn.png" height="100px;" class="pull-right">
        <br>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line.png" style="width:100%" class="">
    </h1>

    <div class="margin-top-60"></div>

    <!-- <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left">
            Tim Verifikasi
        </div>
        <div class="box-10 m-padding-x-0">
            <?php foreach($model['user'] as $user) : ?>
                <div class="margin-bottom-10"><b><?= $user['name'] ?></b> (<?= $user['phone'] ?>) <br><?= $user['jabatan_di_kasn'] ?></div>
            <?php endforeach; ?>
        </div>
    </div> -->

    <table class="table table-no-line">
        <tbody>
            <tr class="text-center fs-16" style="vertical-align: middle;">
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        No
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Aspek
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" colspan="2" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Indikator
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Kondisi Saat Ini
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Bobot Indikator
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Skor
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Nilai Tim Verifikasi
                    </div>
                </th>
                <th class="padding-0 padding-bottom-10 padding-right-10" style="vertical-align: middle;">
                    <div class="bg-azure rounded-md padding-10" style="min-height: 82px;">
                        Nilai Akhir
                    </div>
                </th>
            </tr>
            <?php
                $bobotTotal = 0;
                $nilaiTimInstansiTotal = 0;
                $nilaiTimVerifikasiTotal = 0;
                $nilaiAkhirTotal = 0;
            ?>
            <?php foreach($model['aspek'] as $keyAspek => $aspek) : ?>
                <?php
                    $bobot = 0;
                    $nilaiTimInstansi = 0;
                    $nilaiTimVerifikasi = 0;
                    $nilaiAkhir = 0;
                ?>
                <?php foreach($aspek['indikators'] as $keyIndikator => $indikator) : ?>
                    <?php
                        $bobot += (float)$indikator['bobot'];
                        $nilaiTimInstansi += (int)$indikator['nilai_tim_instansi'];
                        $nilaiTimVerifikasi += (int)$indikator['nilai_tim_verifikasi'];
                        $nilaiAkhir += ((float)$indikator['bobot'] * (int)$indikator['nilai_tim_verifikasi']);
                    ?>
                    <tr>
                        <?php if ($indikator['urutan'] == 1) : ?>
                            <td class="padding-0 padding-bottom-10 padding-right-10">
                                <div class="border-azure border-thin rounded-md padding-10 fs-14 bg-light-azure text-center fs-16" style="min-height: 82px;">
                                    <?= $aspek['urutan'] ?>
                                </div>
                            </td>
                            <td class="padding-0 padding-bottom-10 padding-right-10 f-bold">
                                <div class="border-azure border-thin rounded-md padding-10 fs-14 bg-light-azure fw-bold" style="min-height: 82px;">
                                    <?= $aspek['nama'] ?>
                                </div>
                            </td>
                        <?php else : ?>
                            <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                            <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                        <?php endif; ?>
                        <td class="padding-0 padding-bottom-10 padding-right-10">
                            <div class="border-azure rounded-md padding-10 fs-14" style="min-height: 82px;">
                                <?= $indikator['urutan'] ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10">
                            <div class="border-azure rounded-md padding-10 fs-14" style="min-height: 82px;">
                                <?= $indikator['nama'] ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10 text-center">
                            <div class="border-azure rounded-md padding-10 has-bg-img" style="min-height: 82px;">
                                <?php if ($indikator['kondisi_saat_ini']) : ?>
                                    <?= $indikator['kondisi_saat_ini'] ? Html::a('detail', ['kondisi-saat-ini', 'id' => $indikator['idPenilaianDetail']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Kondisi Saat Ini']) : '' ?>
                                <?php else : ?>
                                    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/arsir.png')"></div>
                                <?php endif ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10 text-center text-grayest fs-15">
                            <div class="border-azure rounded-md padding-10" style="min-height: 82px;">
                                <?= $indikator['bobot'] ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10 text-center text-orange fs-15">
                            <div class="border-azure rounded-md padding-10 has-bg-img" style="min-height: 82px;">
                                <?php if ($indikator['nilai_tim_instansi']) : ?>
                                    <?= $indikator['nilai_tim_instansi'] ?>
                                    <br>
                                    <?= $indikator['nilai_tim_instansi'] ? Html::a('detail', ['nilai-tim-instansi', 'id' => $indikator['id'], 'field' => $indikator['nilai_tim_instansi']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Skor']) : '' ?>
                                <?php else : ?>
                                    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/arsir.png')"></div>
                                <?php endif ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10 text-center text-azure fs-15 fw-bold">
                            <div class="border-azure rounded-md padding-10 has-bg-img" style="min-height: 82px;">
                                <?php if ($indikator['nilai_tim_verifikasi']) : ?>
                                    <?= $indikator['nilai_tim_verifikasi'] ?>
                                    <br>
                                    <?= $indikator['nilai_tim_verifikasi'] ? Html::a('detail', ['nilai-tim-verifikasi', 'id' => $indikator['id'], 'field' => $indikator['nilai_tim_verifikasi']], ['class' => 'fs-11', 'modal-sm' => '', 'modal-title' => 'Nilai Tim Verifikasi']) : '' ?>
                                <?php else : ?>
                                    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/arsir.png')"></div>
                                <?php endif ?>
                            </div>
                        </td>
                        <td class="padding-0 padding-bottom-10 padding-right-10 text-center text-azure fs-18 fw-bold">
                            <div class="border-azure rounded-md padding-10" style="min-height: 82px;">
                                <?= ((float)$indikator['bobot'] * (int)$indikator['nilai_tim_verifikasi']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php
                    $bobotTotal += $bobot;
                    $nilaiTimInstansiTotal += $nilaiTimInstansi;
                    $nilaiTimVerifikasiTotal += $nilaiTimVerifikasi;
                    $nilaiAkhirTotal += $nilaiAkhir;
                ?>
                <tr>
                    <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                    <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                    <td class="padding-0 padding-bottom-10 padding-right-10" colspan="6">
                        <div class="border-orange rounded-md padding-10 bg-light-orange" style="min-height: 82px;">
                            <div>
                                Catatan KASN untuk Aspek <?= $aspek['urutan'] ?> : 
                            </div>
                            <div class="fw-bold fs-14">
                                <?= $aspek['catatanAspek'] ? $aspek['catatanAspek'] : '(kosong)' ?>
                            </div>
                        </div>
                    </td>
                    <td class="padding-0 padding-bottom-10 padding-right-10 text-center">
                        <div class="border-orange rounded-md padding-10 bg-light-orange" style="min-height: 82px;">
                            Total <br>
                            <span class="fs-18 fw-bold"><?= $nilaiAkhir ?></span>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                <td class="padding-0 padding-bottom-10 padding-right-10"></td>
                <td class="padding-0 padding-bottom-10 padding-right-10" colspan="7">
                    <div class="rounded-md padding-20 bg-orange text-right fw-bold fs-16">
                        Total Akhir : <?= $nilaiAkhirTotal ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="margin-top-30"></div>

<?php
    $color = 'red';
    if ($nilaiAkhirTotal < 174) {

    } elseif ($nilaiAkhirTotal < 249) {
        $color = 'yellow';
    } elseif ($nilaiAkhirTotal < 324) {
        $color = 'chartreuse';
    } else {
        $color = 'azure';
    }
?>
<div class="box box-gutter box-break-sm">
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 150px;">
            <h3 class="text-cyan text-center margin-bottom-15">
                Kategori
            </h3>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line1.png" style="width:100%" class="">

            <div class="margin-top-30"></div>

            <div class="text-center ff-taviraj">
                <span class="fs-40 padding-x-20 padding-y-5 rounded-sm bg-<?= $color ?>">
                    <?php if ($nilaiAkhirTotal <= 174) : ?>
                        I
                    <?php elseif ($nilaiAkhirTotal < 249) : ?>
                        II
                    <?php elseif ($nilaiAkhirTotal < 324) : ?>
                        III
                    <?php else : ?>
                        IV
                    <?php endif; ?>
                </span>
            </div>

            <div class="margin-top-15"></div>
        </div>
    </div>
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 150px;">
            <h3 class="text-cyan text-center margin-bottom-15">
                Index
            </h3>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line1.png" style="width:100%" class="">

            <div class="margin-top-30"></div>

            <div class="text-center ff-taviraj">
                <span class="fs-40 padding-x-20 padding-y-5 rounded-sm bg-<?= $color ?>">
                    <?= round($nilaiAkhirTotal/400, 2) ?>
                </span>
            </div>
            
            <div class="margin-top-15"></div>
        </div>
    </div>
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 150px;">
            <h3 class="text-cyan text-center margin-bottom-15">
                Sebutan
            </h3>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line1.png" style="width:100%" class="">

            <div class="margin-top-30"></div>

            <div class="text-center ff-taviraj">
                <span class="fs-40 padding-x-20 padding-y-5 rounded-sm bg-<?= $color ?>">
                    <?php if ($nilaiAkhirTotal <= 174) : ?>
                        Buruk
                    <?php elseif ($nilaiAkhirTotal < 249) : ?>
                        Kurang
                    <?php elseif ($nilaiAkhirTotal < 324) : ?>
                        Baik
                    <?php else : ?>
                        Sangat Baik
                    <?php endif; ?>
                </span>
            </div>
            
            <div class="margin-top-15"></div>
        </div>
    </div>
</div>