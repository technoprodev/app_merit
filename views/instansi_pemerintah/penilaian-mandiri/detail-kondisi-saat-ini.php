<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="text-center border-light-azure text-azure padding-15 rounded-xs">
	<img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/kondisi-saat-ini.png" width="70px;">
	<h6><?= $value ?></h6>
</div>
