<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/instansi_pemerintah/profile/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>

    <table class="datatables table table-striped table-hover table-nowrap">
        <thead>
            <tr class="text-dark">
                <th>Action</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>HP</th>
                <th>Jabatan di instansi</th>
                <th>Jabatan di SK</th>
            </tr>
            <tr class="dt-search">
                <th></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search name..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search email..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search username..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search hp..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search jabatan di instansi..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search jabatan di sk..."/></th>
            </tr>
        </thead>
    </table>

    <?php if ($this->context->can('create')): ?>
        <hr class="margin-y-15">
        <div>
            <?= Html::a('Pendaftaran User Baru', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
    <?php endif; ?>
</div>