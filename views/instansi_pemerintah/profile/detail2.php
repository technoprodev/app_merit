<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md has-bg-img">
    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg.png')"></div>
    <h1 class="text-cyan clearfix">
        <?php if ($model['instansi_pemerintah']->logo) : ?>
            <img src="<?= $model['instansi_pemerintah']->virtual_logo_download ?>" height="90px;" class="pull-left margin-right-5">
        <?php else : ?>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/instansi.png" height="50px;" class="pull-left margin-right-5">
        <?php endif; ?>
        <span class="pull-left margin-right-5 margin-top-15"><?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '' ?></span>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/asn.png" height="100px;" class="pull-right">
        <br>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line.png" style="width:100%" class="">
    </h1>

    <div class="margin-top-60"></div>

            <!-- <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/profile.png" height="70px;" class="margin-right-5 text-middle border-lighter padding-5"> -->
<div class="box box-break-sm box-space-sm box-gutter">
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-users text-magenta fs-50"></i></span>
            <br>
            <?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Username
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-user text-magenta fs-50"></i></span>
            <br>
            <?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Nama
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-envelope text-magenta fs-50"></i></span>
            <br>
            <?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Email
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-phone text-magenta fs-50"></i></span>
            <br>
            <?= $model['user']->phone ? $model['user']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Kontak
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-star text-magenta fs-50"></i></span>
            <br>
            <?= $model['user_instansi_pemerintah']->jabatan_di_instansi ? $model['user_instansi_pemerintah']->jabatan_di_instansi : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Jabatan di Instansi
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-star-o text-magenta fs-50"></i></span>
            <br>
            <?= $model['user_instansi_pemerintah']->jabatan_di_sk ? $model['user_instansi_pemerintah']->jabatan_di_sk : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Jabatan di SK
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-star-o text-magenta fs-50"></i></span>
            <br>
            <?= $model['user_instansi_pemerintah']->jabatan_di_sk ? $model['user_instansi_pemerintah']->jabatan_di_sk : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Lampiran SK
            </div>
        </div>
    </div>
    <div class="box-3">
        <div class="border-azure rounded-md text-azure text-center padding-15 fs-18 lighter-50">
            <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 70px; height: 70px;"><i class="fa fa-star-o text-magenta fs-50"></i></span>
            <br>
            <?= $model['user_instansi_pemerintah']->jabatan_di_sk ? $model['user_instansi_pemerintah']->jabatan_di_sk : '<span class="text-gray f-italic">(kosong)</span>' ?>
            <div class="margin-top-5"></div>
            <div class="bg-azure rounded-md text-center padding-10 fs-14">
                Alamat Instansi
            </div>
        </div>
    </div>
</div>

<!-- <div class="margin-bottom-10 fs-16">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/profile.png" height="40px;" class="margin-right-5 text-middle border-lighter padding-5">
    <span class="text-middle">username:</span> <span class="text-middle fw-bold"><?= $model['user']->username ? $model['user']->username : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div>

<div class="margin-bottom-10 fs-16">
    <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-user text-azure fs-18"></i></span>
    <span class="text-middle">nama:</span> <span class="text-middle fw-bold"><?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div>

<div class="margin-bottom-10 fs-16">
    <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-envelope text-azure fs-18"></i></span>
    <span class="text-middle">email:</span> <span class="text-middle fw-bold"><?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div>

<div class="margin-bottom-10 fs-16">
    <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-phone text-azure fs-18"></i></span>
    <span class="text-middle">kontak:</span> <span class="text-middle fw-bold"><?= $model['user']->phone ? $model['user']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div>

<div class="margin-bottom-10 fs-16">
    <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-star text-azure fs-18"></i></span>
    <span class="text-middle">jabatan di instansi:</span> <span class="text-middle fw-bold"><?= $model['user_instansi_pemerintah']->jabatan_di_instansi ? $model['user_instansi_pemerintah']->jabatan_di_instansi : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div>

<div class="margin-bottom-10 fs-16">
    <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-star-o text-azure fs-18"></i></span>
    <span class="text-middle">jabatan di sk:</span> <span class="text-middle fw-bold"><?= $model['user_instansi_pemerintah']->jabatan_di_sk ? $model['user_instansi_pemerintah']->jabatan_di_sk : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
</div> -->

<?php if (!Yii::$app->request->isAjax) : ?>
    <hr class="margin-y-15">
    <div class="form-group clearfix text-center">
        <?= Html::a('Update', ['update'/*, 'id' => $model['user']->id*/], ['class' => 'button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure', 'style' => 'width:250px;']) ?>
        <?= Html::a('Change Password', ['change-password'], ['class' => 'button button-lg border-azure text-azure hover-bg-azure', 'style' => 'width:250px;']) ?>
    </div>
</div>
<?php endif; ?>