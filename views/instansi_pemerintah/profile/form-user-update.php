<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/instansi_pemerintah/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$instansi_pemerintahChildren = [];
if (isset($model['instansi_pemerintah_child']))
    foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild)
        $instansi_pemerintahChildren[] = $instansi_pemerintahChild->attributes;

$this->registerJs(
    'vm.$data.instansi_pemerintah.virtual_category = ' . json_encode($model['user']->virtual_category) . ';' .
    'vm.$data.instansi_pemerintah.instansi_pemerintahChildren = vm.$data.instansi_pemerintah.instansi_pemerintahChildren.concat(' . json_encode($instansi_pemerintahChildren) . ');',
    // 'vm.$data.instansi_pemerintah.instansi_pemerintahChildren = Object.assign({}, vm.$data.instansi_pemerintah.instansi_pemerintahChildren, ' . json_encode($instansi_pemerintahChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['user']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}

/*if ($model['instansi_pemerintah_extend']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['instansi_pemerintah_extend'], ['class' => '']);
}

if (isset($model['instansi_pemerintah_child'])) foreach ($model['instansi_pemerintah_child'] as $key => $instansi_pemerintahChild) {
    if ($instansi_pemerintahChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($instansi_pemerintahChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">
        <?php if ($model['instansi_pemerintah']->logo) : ?>
            <img src="<?= $model['instansi_pemerintah']->virtual_logo_download ?>" height="50px;" class="margin-right-5 text-middle">
        <?php else : ?>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/instansi.png" height="50px;" class="margin-right-5 text-middle">
        <?php endif; ?>
        <span class="text-middle"><?= $title ?></span>
    </h1>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?= $form->field($model['user'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'username', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'username', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'username')->end(); ?>
  
    <?= $form->field($model['user'], 'name', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'name', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>
  
    <?= $form->field($model['user'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'email', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'email', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'email')->end(); ?>
  
    <?= $form->field($model['user'], 'phone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user'], 'phone', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user'], 'phone', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'phone', ['class' => 'form-info']); ?>
    <?= $form->field($model['user'], 'phone')->end(); ?>
  
    <?= $form->field($model['user_instansi_pemerintah'], 'jabatan_di_instansi', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user_instansi_pemerintah'], 'jabatan_di_instansi', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user_instansi_pemerintah'], 'jabatan_di_instansi', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user_instansi_pemerintah'], 'jabatan_di_instansi', ['class' => 'form-info']); ?>
    <?= $form->field($model['user_instansi_pemerintah'], 'jabatan_di_instansi')->end(); ?>
  
    <?= $form->field($model['user_instansi_pemerintah'], 'jabatan_di_sk', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['user_instansi_pemerintah'], 'jabatan_di_sk', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['user_instansi_pemerintah'], 'jabatan_di_sk', ['class' => 'form-text', 'maxlength' => true]); ?>
        <?= Html::error($model['user_instansi_pemerintah'], 'jabatan_di_sk', ['class' => 'form-info']); ?>
    <?= $form->field($model['user_instansi_pemerintah'], 'jabatan_di_sk')->end(); ?>

    <hr class="margin-y-15">

    <?php if ($errorMessage) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-wrapper clearfix">
        <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button border-azure bg-lightest text-azure']); ?>
        <?= Html::a('Kembali', ['index'], ['class' => 'button border-azure bg-lightest text-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

</div>