<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
        <label class="control-label f-bold">Aspek</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <div class="padding-y-5">1. Perencanaan Kebutuhan Pegawai</div>
        <div class="help-block fs-11 margin-0"></div>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
        <label class="control-label f-bold">Indikator</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <div class="padding-y-5">1.1	Ketersediaan peta jabatan dan  rencana kebutuhan pegawai untuk jangka menengah (5 tahun) yang sudah ditetapkan PPK</div>
        <div class="help-block fs-11 margin-0"></div>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
        <label class="control-label f-bold">Skor</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <div class="padding-y-5">
            <div class="radio">
                <label>
                    <input id="radio-elegant-1" name="radio-elegant" value="1" checked="" type="radio">
                    <i></i>Belum disusun atau sudah disusun namun belum didasarkan pada anjab dan ABK;
                </label>
            </div>
            <div class="radio">
                <label>
                    <input id="radio-elegant-2" name="radio-elegant" value="2" type="radio">
                    <i></i>Sudah disusun  peta jabatan dan formasi kebutuhan pegawai untuk tahun berjalan  berdasarkan anjab dan ABK  dirinci menurut jenis jabatan, pangkat, deskripsi tugas, dan kualifikasi;
                </label>
            </div>
            <div class="radio">
                <label>
                    <input id="radio-elegant-3" name="radio-elegant" value="3" type="radio">
                    <i></i>Sudah disusun peta jabatan dan rencana kebutuhan ASNuntuk  jangka menengahberdasarkan anjab dan ABK, dirinci menurut jenis jabatan, pangkat, deskripsi tugasdan kualifikasi, namun belum ditetapkan dengan keputusan PPK;
                </label>
            </div>
            <div class="radio">
                <label>
                    <input id="radio-elegant-3" name="radio-elegant" value="3" type="radio">
                    <i></i>Sudah disusun peta jabatan dan rencana kebutuhan pegawaiuntuk  jangka menengah, berdasarkan anjab dan ABK, dirinci menurut jabatan, pangkat, deskripsi tugas dan kualifikasi, dan telah ditetapkan dengan keputusan PPK;
                </label>
            </div>
            <div class="help-block fs-11 margin-0"></div>
        </div>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
        <label class="control-label f-bold">Kondisi Saat Ini</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <textarea class="form-control"></textarea>
        <div class="help-block fs-11 margin-0"></div>
    </div>
</div>

<div class="form-group form-group-sm box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
        <label class="control-label f-bold">Bukti</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <button class="btn btn-sm border-azure text-azure bg-lightest">Pilih File</button>
        <a class="btn btn-sm border-azure text-azure bg-lightest"><i class="fa fa-plus"></i></a>
        <div class="help-block fs-11 margin-0"></div>
    </div>
</div>