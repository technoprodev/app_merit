<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/instansi_pemerintah/dashboard/form-penilaian.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\FileInputAsset::register($this);

$buktiWajib = $model['penilaian_detail']->bukti_wajib ? $model['penilaian_detail']->bukti_wajib : '';
$this->registerJs(
    'vm.$data.penilaian_detail.bukti_wajib = "' . $buktiWajib . '";',
    3
);

//
$errorMessage = '';
// $errorVue = false;
if ($model['penilaian_detail']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['penilaian_detail'], ['class' => '']);
}

/*if ($model['dev_extend']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['dev_extend'], ['class' => '']);
}

if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}*/
/*if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30"><?= $title ?></h1>
<?php endif; ?>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Aspek</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']['aspek']['urutan'] ?>. <?= $model['indikator']['aspek']['nama'] ?>
    </div>
</div>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Indikator</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']['aspek']['urutan'] ?>.<?= $model['indikator']['urutan'] ?>. <?= $model['indikator']['nama'] ?>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

<?= $form->field($model['penilaian_detail'], 'kondisi_saat_ini', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
    <div class="box-2 padding-x-0 text-right m-text-left">
        <?= Html::activeLabel($model['penilaian_detail'], 'kondisi_saat_ini', ['class' => 'form-label fw-bold margin-top-5', 'label' => 'Kondisi Saat Ini <span class="text-red">*</span>']); ?>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= Html::activeTextArea($model['penilaian_detail'], 'kondisi_saat_ini', ['class' => 'form-textarea', 'maxlength' => true]); ?>
        <?= Html::error($model['penilaian_detail'], 'kondisi_saat_ini', ['class' => 'form-info']); ?>
    </div>
<?= $form->field($model['penilaian_detail'], 'kondisi_saat_ini')->end(); ?>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Bobot</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= $model['indikator']['bobot'] ?>
    </div>
</div>

<?= $form->field($model['penilaian_detail'], 'nilai_tim_instansi', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
    <div class="box-2 padding-x-0 text-right m-text-left">
        <?= Html::activeLabel($model['penilaian_detail'], 'nilai_tim_instansi', ['class' => 'form-label fw-bold', 'label' => 'Skor <span class="text-red">*</span>']); ?>
    </div>
    <div class="box-10 m-padding-x-0">
        <?= Html::activeRadioList($model['penilaian_detail'], 'nilai_tim_instansi',
            \Yii::$app->db->createCommand(
                "SELECT s.1, s.2, s.3, s.4 FROM indikator s
                WHERE
                    s.id = :id"
            )->bindValue(':id', $model['indikator']['id'])->queryOne(),
            [
                'class' => 'form-radio', 
                'unselect' => null,
                'item' => function($index, $label, $name, $checked, $value) {
                    $checked = $checked ? 'checked' : '';
                    $disabled = in_array($value, []) ? 'disabled' : '';
                    return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                },
            ]
        ); ?>
        <?= Html::error($model['penilaian_detail'], 'nilai_tim_instansi', ['class' => 'form-info']); ?>
    </div>
<?= $form->field($model['penilaian_detail'], 'nilai_tim_instansi')->end(); ?>

<?= $form->field($model['penilaian_detail'], 'virtual_bukti_wajib_upload', ['options' => ['class' => 'form-wrapper box box-break-sm margin-bottom-10'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
    <div class="box-2 padding-x-0 text-right m-text-left">
        <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_wajib_upload', ['class' => 'form-label fw-bold margin-top-5', 'label' => 'Bukti Wajib <span class="text-red">*</span>']); ?>
    </div>
    <div class="box-10 m-padding-x-0">
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            <div class="form-text rounded-xs">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_wajib_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_wajib ?></a></span>
            </div>
            <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_wajib_upload'); ?>
            </span>
        </div>
        <?= Html::error($model['penilaian_detail'], 'virtual_bukti_wajib_upload', ['class' => 'form-info']); ?>
    </div>
<?= $form->field($model['penilaian_detail'], 'virtual_bukti_wajib_upload')->end(); ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <div class="fw-bold">Bukti Tambahan</div>
    </div>
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_1_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_1_upload', ['class' => 'form-label', 'label' => 'Tambahan 1']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_1_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_1 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_1_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_1_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_1_upload')->end(); ?>
    </div>

    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_2_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_2_upload', ['class' => 'form-label', 'label' => 'Tambahan 2']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_2_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_2 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_2_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_2_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_2_upload')->end(); ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_3_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_3_upload', ['class' => 'form-label', 'label' => 'Tambahan 3']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_3_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_3 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_3_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_3_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_3_upload')->end(); ?>
    </div>

    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_4_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_4_upload', ['class' => 'form-label', 'label' => 'Tambahan 4']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_4_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_4 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_4_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_4_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_4_upload')->end(); ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_5_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_5_upload', ['class' => 'form-label', 'label' => 'Tambahan 5']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_5_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_5 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_5_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_5_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_5_upload')->end(); ?>
    </div>
    
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_6_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_6_upload', ['class' => 'form-label', 'label' => 'Tambahan 6']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_6_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_6 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_6_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_6_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_6_upload')->end(); ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_7_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_7_upload', ['class' => 'form-label', 'label' => 'Tambahan 7']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_7_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_7 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_7_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_7_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_7_upload')->end(); ?>
    </div>
    
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_8_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_8_upload', ['class' => 'form-label', 'label' => 'Tambahan 8']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_8_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_8 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_8_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_8_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_8_upload')->end(); ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_9_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_9_upload', ['class' => 'form-label', 'label' => 'Tambahan 9']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_9_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_9 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_9_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_9_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_9_upload')->end(); ?>
    </div>
    
    <div class="box-5 m-padding-x-0">
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_10_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            
            <?= Html::activeLabel($model['penilaian_detail'], 'virtual_bukti_tambahan_10_upload', ['class' => 'form-label', 'label' => 'Tambahan 10']); ?>
        
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['penilaian_detail']->virtual_bukti_tambahan_10_download ?>" target="_blank"><?= $model['penilaian_detail']->bukti_tambahan_10 ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['penilaian_detail'], 'virtual_bukti_tambahan_10_upload'); ?>
                </span>
            </div>
            
            <?= Html::error($model['penilaian_detail'], 'virtual_bukti_tambahan_10_upload', ['class' => 'form-info']); ?>
        
        <?= $form->field($model['penilaian_detail'], 'virtual_bukti_tambahan_10_upload')->end(); ?>
    </div>
</div>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-10 m-padding-x-0">
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
    </div>
    <div class="box-10 m-padding-x-0">
        <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
</div>

<!-- <div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Kondisi Saat Ini</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <textarea class="form-control"></textarea>
        <div class="form-info"></div>
    </div>
</div>

<div class="form-wrapper box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left">
        <label class="form-label fw-bold">Bukti</label>
    </div>
    <div class="box-10 m-padding-x-0">
        <button class="btn btn-sm border-azure text-azure bg-lightest">Pilih File</button>
        <a class="btn btn-sm border-azure text-azure bg-lightest"><i class="fa fa-plus"></i></a>
        <div class="form-info"></div>
    </div>
</div> -->

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
</div>
<?php endif; ?>