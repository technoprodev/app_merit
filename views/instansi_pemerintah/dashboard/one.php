<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md">
    <h1 class="text-cyan margin-bottom-30">Dashboard Penilaian Mandiri</h1>

    <ul class="nav nav-tabs f-bold">
        <li class="active"><a href="#tab-1" data-toggle="tab" class="padding-x-10">Perencanaan Kebutuhan Pegawai <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">1 / 4</span></a></li>
        <li class="activ3"><a href="#tab-2" data-toggle="tab" class="padding-x-10">Pengadaan <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">3 / 7</span></a></li>
        <li class="activ3"><a href="#tab-3" data-toggle="tab" class="padding-x-10">Pembinaan Karir dan Peningkatan Kompetensi <span class="border-azure bg-light-azure fs-10 padding-y-2 padding-x-5 rounded-md">6 / 6</span></a></li>
        <li class="activ3"><a href="#tab-4" data-toggle="tab" class="padding-x-10">Mutasi, Rotasi dan Promosi <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">3 / 7</span></a></li>
        <li class="activ3"><a href="#tab-5" data-toggle="tab" class="padding-x-10">Manajemen Kinerja <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">3 / 7</span></a></li>
        <li class="activ3"><a href="#tab-6" data-toggle="tab" class="padding-x-10">Penggajian, Penghargaan dan Disiplin <span class="border-azure bg-light-azure fs-10 padding-y-2 padding-x-5 rounded-md">8 / 8</span></a></li>
        <li class="activ3"><a href="#tab-7" data-toggle="tab" class="padding-x-10">Perlindungan dan Pelayanan ASN <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">3 / 7</span></a></li>
        <li class="activ3"><a href="#tab-8" data-toggle="tab" class="padding-x-10">Sistem Pendukung <span class="border-red bg-light-red fs-10 padding-y-2 padding-x-5 rounded-md">3 / 7</span></a></li>
    </ul>

    <div class="tab-content bg-lightest">
        <div class="tab-pane padding-30 fade active in" id="tab-1">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1.  Ketersediaan peta jabatan dan  rencana kebutuhan pegawai untuk jangka menengah (5 tahun) yang sudah ditetapkan PPK</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-red border-red" style="width: 18px; height: 18px;"></div></td>
                        <td>2.  Ketersediaan data kepegawaiansecara lengkap dan  real time yang disusun menurut jabatan, pangkat, unit kerja, kualifikasi dan kompetensi;</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-red border-red" style="width: 18px; height: 18px;"></div></td>
                        <td>3.  Ketersediaan data pegawai yang akan memasuki masa pensiun dalam 5 tahun yang disusun menurut jabatan, pangkat, unit kerja;</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Edit Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-red border-red" style="width: 18px; height: 18px;"></div></td>
                        <td>4.  Ketersediaan rencana pengadaan untuk memenuhi kebutuhan ASN untuk jangka menengah (5 tahun) yang berasal dari CALON PNS, PPPK ataupun PNS dari instansi lain;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-2">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Ketersediaan rencana pengadaan ASN untuk tahun berjalan yang dirinci menurut jumlah, jenis jabatan, pangkat, kualifikasi, kompetensi, dan unit kerja, baik dari CALON PNS, PPPK, ataupun PNS dari instansi lainserta TNI/Polri (untuk instansi tertentu);&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Ketersediaan kebijakan internal (Permen/Pergub / Perbup/Perwal) terkait pengadaan ASN  dan TNI/Polri (untuk instansi tertentu) secara terbuka, kompetitif, transparan dan tidak diskriminatif;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Pelaksanaan penerimaan CALON PNS/PPPK/ PNS dari instansi lain dilakukan  secara terbuka;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>4. Pelaksanaan LPJ (Latihan Pra Jabatan ) bagi CALON PNS;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>5. Persentase  pegawai yang baru diangkat menjadi PNS yang  penempatan pertamanya sesuai  jabatan yang dilamar;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-3">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Ketersediaan standar kompetensi manajerial, bidang, dan sosio kultural untuk setiap jabatan;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Ketersediaan profil pegawai yang disusun berdasarkan pemetaan talenta/kompetensi;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Ketersediaan talent pool dan rencana suksesi yang disusun berdasarkan profil ASN (kualifikasi, kompetensi, dan kinerja) dengan mempertimbangkan pola karir instansi;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>4. Ketersediaan informasi tentang kesenjangan kualifikasi dan kompetensi pegawai;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>5. Ketersediaan informasi tentang kesenjangan kinerja;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>6. Ketersediaan strategi dan program untuk mengatasi kesenjangan kompetensi dan kinerja dalam rangka peningkatan kapasitas ASN;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>7. Penyelenggaraan Diklat yang dalam rangka mengatasi kesenjangan;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>8. Pelaksanaan  peningkatan kompetensi melalui praktik kerja dan pertukaran pegawai;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>9. Pelaksanaan  peningkatan kompetensi melalui coaching, counseling dan mentoring;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-4">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Penyusunan dan penetapan kebijakan internal (Permen/Pergub/Perbup/Perwal) tentang pola karier;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Ketersediaan kebijakan internal (Permen/Pergub/Perbup/Perwali)tentang mutasi, rotasi  dan promosi secara obyektif dan transparan dengan mengacu kepada rencana suksesi;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Pelaksanaan kebijakan pengisian JPT, Administrator dan Pengawas secara terbuka dan kompetitif ;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-5">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Penyusunan kontrak kinerja yang terukur;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Penerapan metode penilaian kinerja yang obyektif dan terukur;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Pelaksanaan penilaian kinerja secara berkala untuk memastikan tercapainya kontrak kinerja;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>4. Ketersediaan informasi dan strategi untuk mengatasi  permasalahan kinerja;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>5. Penggunaan hasil penilaian kinerja sebagai dasar bagi penentuan keputusan manajemen terkait pembinaan dan pengembangan karier (promosi, mutasi, demosi, rotasi, diklat);&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-6">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Kebijakan pembayaran tunjangan kinerja dikaitkan dengan hasil penilaian kinerja;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Ketersediaan  kebijakan internal (Permen/Pergub/Perbup/Perwali) untuk memberi  penghargaan yang bersifat finansial dan non-finansial terhadap pegawai berprestasi luar biasa;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Penegakan  kode etik dan kode perilaku ASN di lingkungan instansinya;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>4. Pengelolaan data terkait pelanggaran disiplin, pelanggaran kode etik dan kode perilaku yang dilakukan pegawai;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-7">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Kebijakan perlindungan pegawai diluar dari jaminan kesehatan, jaminan kecelakaan kerja, program pensiun, yang diselenggarakan  secara nasional;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Penyediaan kemudahan bagi pegawai yang membutuhkan pelayanan administrasi;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane padding-30 fade" id="tab-8">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr role="row">
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 50px;">Action</th>
                        <th class="text-dark f-default" style="border-bottom: 1px; width: 20px;"></th>
                        <th class="text-dark f-default" style="border-bottom: 1px">Indikator</th>
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>1. Pembangunan Sistem Informasi Kepegawaian yang berbasis online  yang terintegrasi dengan sistem penilaian kinerja, penegakan disiplin dan pembinaan pegawai;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>2. Penerapan e-performance yang terintegrasi dengan  Sistem Informasi Kepegawaian yang berbasis online;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>3. Penggunaan e-office yang memudahkan pelayanan administrasi kepegawaian;&</td>
                    </tr>
                    <tr>
                        <td><a href="<?= Yii::$app->urlManager->createUrl("instansi_pemerintah/dashboard/form") ?>" modal-lg="" modal-title="Form Penilaian" class="btn btn-sm padding-5 bg-lightest text-azure border-azure hover-bg-light-azure">Isi Penilaian</a></td>
                        <td><div class="pull-left circle-icon margin-x-10 bg-azure border-azure" style="width: 18px; height: 18px;"></div></td>
                        <td>4. Pembangunan dan penggunaan asessment center dalam pemetaan kompetensi dan pengisian jabatan;&</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
