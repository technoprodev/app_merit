<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
?>

<style type="text/css">
.table-striped tbody tr:nth-of-type(2n+1) {
    background-color: rgba(102, 175, 233, .1);
}
.table-bordered {
  border: 1px solid #3376b8;
}

.table-bordered th:not(:last-child),
.table-bordered td:not(:last-child) {
  border-right: 1px solid #3376b8;
}
.nav > li > a:hover, .nav > li > a:focus {
    text-decoration: none;
    background-color: #3376b8;
}
</style>

<?php
    $bobotTotal = 0;
    $nilaiTimInstansiTotal = 0;
    $nilaiTimVerifikasiTotal = 0;
    $nilaiAkhirTotal = 0;
?>
<?php foreach($model['aspek'] as $keyAspek => $aspek) : ?>
    <?php
        $bobot = 0;
        $nilaiTimInstansi = 0;
        $nilaiTimVerifikasi = 0;
        $nilaiAkhir = 0;
    ?>
    <?php foreach($aspek['indikators'] as $keyIndikator => $indikator) : ?>
        <?php
            $bobot += (float)$indikator['bobot'];
            $nilaiTimInstansi += (int)$indikator['nilai_tim_instansi'];
            $nilaiTimVerifikasi += (int)$indikator['nilai_tim_verifikasi'];
            $nilaiAkhir += ((float)$indikator['bobot'] * (int)$indikator['nilai_tim_verifikasi']);
        ?>
    <?php endforeach; ?>
    <?php
        $bobotTotal += $bobot;
        $nilaiTimInstansiTotal += $nilaiTimInstansi;
        $nilaiTimVerifikasiTotal += $nilaiTimVerifikasi;
        $nilaiAkhirTotal += $nilaiAkhir;
    ?>
<?php endforeach; ?>

<?php
    $color = 'red';
    if ($nilaiAkhirTotal < 174) {

    } elseif ($nilaiAkhirTotal < 249) {
        $color = 'yellow';
    } elseif ($nilaiAkhirTotal < 324) {
        $color = 'chartreuse';
    } else {
        $color = 'azure';
    }
?>

<div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md has-bg-img">
    <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg.png')"></div>
    <h1 class="text-cyan clearfix">
        <?php if ($model['instansi_pemerintah']->logo) : ?>
            <img src="<?= $model['instansi_pemerintah']->virtual_logo_download ?>" height="90px;" class="pull-left margin-right-5">
        <?php else : ?>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/instansi.png" height="50px;" class="pull-left margin-right-5">
        <?php endif; ?>
        <div class="pull-left margin-right-5 margin-top-15">
            <div><?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '' ?></div>
            <div class="ff-taviraj">
                <span class="fs-14 rounded-sm border-<?= $color ?> text-<?= $color ?> padding-x-15 margin-right-5">
                    Kategori :
                    <?php if ($nilaiAkhirTotal <= 174) : ?>
                        I (Buruk)
                    <?php elseif ($nilaiAkhirTotal < 249) : ?>
                        II (Kurang)
                    <?php elseif ($nilaiAkhirTotal < 324) : ?>
                        III (Baik)
                    <?php else : ?>
                        IV (Sangat Baik)
                    <?php endif; ?>
                </span>
            </div>        
        </div>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/asn.png" height="100px;" class="pull-right">
        <br>
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line.png" style="width:100%" class="">
    </h1>

    <div class="margin-top-60"></div>

    <!-- <div class="margin-bottom-10 fs-16 fw-bold">
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/instansi.png" height="40px;" class="margin-right-5 text-middle border-lighter padding-5">
        <span class="text-middle"><?= $model['instansi_pemerintah']->nama ? $model['instansi_pemerintah']->nama : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
    </div>

    <div class="margin-bottom-10 fs-16 fw-bold">
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/profile.png" height="40px;" class="margin-right-5 text-middle border-lighter padding-5">
        <span class="text-middle"><?= $model['user']->name ? $model['user']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
    </div>

    <div class="margin-bottom-10 fs-16 fw-bold">
        <span class="margin-right-5 text-middle border-lighter padding-5 inline-block text-center" style="width: 40px; height: 40px;"><i class="fa fa-envelope text-azure fs-18"></i></span>
        <span class="text-middle"><?= $model['user']->email ? $model['user']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></span>
    </div> -->

    <style type="text/css">
        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            color: transparent;
            background-color: transparent;
        }
    </style>

    <table class="nav nav-pills table table-no-line">
        <?php $color = [
            'rose',
            'magenta',
            'violet',
            'azure',
            'cyan',
            'spring',
            'chartreuse',
            'orange',
        ]; ?>
        <?php foreach($model['aspek'] as $key => $aspek) : ?>
            <?php $current = count(array_filter($aspek['indikators'], function($indikator) { return $indikator['kondisi_saat_ini'] != null; })) ?>
            <?php $count = count($aspek['indikators']) ?>
            <td class="padding-0 <?= $aspek['urutan'] == $urutan ? 'active' : 'activ3' ?>" style="width: 12.5%">
                <a href="#tab-<?= $aspek['urutan'] ?>" data-toggle="tab" class="margin-0">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon-aspek/<?= $aspek['urutan'] ?>.png" style="width:100%">
                </a>
            </td>
        <?php endforeach; ?>
    </table>

    <div class="tab-content">
        <?php foreach($model['aspek'] as $key => $aspek) : ?>
            <div class="tab-pane padding-top-30 fade <?= $aspek['urutan'] == $urutan ? 'active in' : '' ?>" id="tab-<?= $aspek['urutan'] ?>">
                <table class="table table-no-line">
                    <tbody>
                        <tr class="text-center fs-16">
                            <td class="padding-0 padding-bottom-10 padding-right-10">
                                <div class="bg-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10">No</div>
                            </td>
                            <td class="padding-0 padding-bottom-10 padding-right-10">
                                <div class="bg-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10">Indikator</div>
                            </td>
                            <td class="padding-0 padding-bottom-10 padding-right-10" style="width: 200px;">
                                <div class="bg-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10">Status</div>
                            </td>
                            <td class="padding-0 padding-bottom-10 padding-right-10" style="width: 70px;">
                                <div class="bg-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10">Action</div>
                            </td>
                        </tr>
                        <?php foreach($aspek['indikators'] as $key => $indikator) : ?>
                            <tr>
                                <td class="padding-0 padding-bottom-10 padding-right-10">
                                    <div class="border-<?= $color[($aspek['urutan']-1)] ?> text-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10 text-center fs-16" style="min-height: 60px;">
                                        <?= $indikator['urutan'] ?>
                                    </div>
                                </td>
                                <td class="padding-0 padding-bottom-10 padding-right-10">
                                    <div class="border-<?= $color[($aspek['urutan']-1)] ?> text-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10" style="min-height: 60px;">
                                        <?= $indikator['nama'] ?>
                                    </div>
                                </td>
                                <td class="padding-0 padding-bottom-10 padding-right-10">
                                    <div class="border-<?= $color[($aspek['urutan']-1)] ?> text-<?= $color[($aspek['urutan']-1)] ?> rounded-md padding-10" style="min-height: 60px;">
                                        <div class="pull-left circle-icon margin-right-10 <?= $indikator['kondisi_saat_ini'] != null ? 'bg-azure border-azure' : 'bg-red border-red' ?>" style="width: 18px; height: 18px;"></div>
                                        <?= $indikator['kondisi_saat_ini'] != null ? '<span class="text-azure">Sudah diisi</span>' : '<span class="text-red">Belum diisi</span>' ?>
                                    </div>
                                </td>
                                <td class="padding-0 padding-bottom-10 padding-right-10">
                                    <a href="<?= Yii::$app->urlManager->createUrl(['instansi_pemerintah/dashboard/penilaian', 'id' => $indikator['id']]) ?>" modal-lg="" modal-title="Form Penilaian" class="button button-xs rounded-md padding-5 bg-lightest <?= $indikator['kondisi_saat_ini'] != null ? 'text-azure border-azure hover-bg-light-azure' : 'text-red border-red hover-bg-light-red' ?>" style="height: 60px"><?= $indikator['kondisi_saat_ini'] != null ? 'Ubah' : 'Isi' ?> Penilaian</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <!-- <table class="table table-striped table-hover table-condensed table-bordered">
                    <thead>
                        <tr class="bg-azure border-azure fs-14">
                            <th>Indikator</th>
                            <th style="width: 200px;">Status</th>
                            <th style="width: 50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($aspek['indikators'] as $key => $indikator) : ?>
                            <tr>
                                <td><?= $indikator['urutan'] ?>. <?= $indikator['nama'] ?></td>
                                <td>
                                    <div class="pull-left circle-icon margin-right-10 <?= $indikator['kondisi_saat_ini'] != null ? 'bg-azure border-azure' : 'bg-red border-red' ?>" style="width: 18px; height: 18px;"></div>
                                    <?= $indikator['kondisi_saat_ini'] != null ? '<span class="text-azure">Sudah diisi</span>' : '<span class="text-red">Belum diisi</span>' ?>
                                </td>
                                <td>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['instansi_pemerintah/dashboard/penilaian', 'id' => $indikator['id']]) ?>" modal-lg="" modal-title="Form Penilaian" class="button button-xs padding-5 bg-lightest <?= $indikator['kondisi_saat_ini'] != null ? 'text-azure border-azure hover-bg-light-azure' : 'text-red border-red hover-bg-light-red' ?>"><?= $indikator['kondisi_saat_ini'] != null ? 'Ubah' : 'Isi' ?> Penilaian</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table> -->
            </div>
        <?php endforeach; ?>
    </div>
</div>

<div class="margin-top-30"></div>

<div class="box box-gutter box-break-sm">
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 370px;">
            <h3 class="text-cyan margin-bottom-30">
                Upload BA Penilaian
                <?php if ($model['penilaian']->berita_acara_penilaian) : ?>
                    <i class="fa fa-upload pull-right"></i>
                <?php else : ?>
                    <i class="fa fa-upload pull-right text-red"></i>
                <?php endif; ?>
            </h3>

            <?php if ($model['penilaian']->berita_acara_penilaian) : ?>
                <div class="text-center border-light-azure text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/check.png" width="70px;">
            <?php else : ?>
                <div class="text-center border-light-red text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/not-found.png" width="70px;">
            <?php endif; ?>
                    <div class="margin-top-15"></div>

                    <?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

                    <?= $form->field($model['penilaian'], 'virtual_berita_acara_penilaian_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="form-text">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a href="<?= $model['penilaian']->virtual_berita_acara_penilaian_download ?>" target="_blank"><?= $model['penilaian']->berita_acara_penilaian ?></a></span>
                        </div>
                        <div class="margin-top-5"></div>
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <a href="#" style="border-radius:0" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            <span style="border-radius:0;border-left: 1px solid #ccc;" class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <?= Html::activeFileInput($model['penilaian'], 'virtual_berita_acara_penilaian_upload'); ?>
                            </span>
                        </div>
                        <?= Html::error($model['penilaian'], 'virtual_berita_acara_penilaian_upload', ['class' => 'form-info']); ?>
                    <?= $form->field($model['penilaian'], 'virtual_berita_acara_penilaian_upload')->end(); ?>
                    <div class="clearfix">
                        <?= Html::submitButton('Submit', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure button-block button-lg']) ?>
                    </div>
                    
                    <?php ActiveForm::end(); ?>

                </div>
        </div>
    </div>
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 370px;">
            <h3 class="text-cyan margin-bottom-30">
                Download BA Verifikasi
                <?php if ($model['penilaian']->berita_acara_verifikasi) : ?>
                    <i class="fa fa-download pull-right"></i>
                <?php else : ?>
                    <i class="fa fa-download pull-right text-red"></i>
                <?php endif; ?>
            </h3>
            <?php if ($model['penilaian']->berita_acara_verifikasi) : ?>
                <div class="text-center border-light-azure text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/check.png" width="70px;">
                    <h6>Berita Acara Verifikasi sudah tersedia. Dokumen ini dapat didownload sekarang.</h6>
                    <a href="<?= $model['penilaian']->virtual_berita_acara_verifikasi_download ?>" target="_blank" class="button border-azure bg-azure hover-bg-lightest hover-text-azure button-block button-lg">Download</a>
                </div>
            <?php else : ?>
                <div class="text-center border-light-red text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/not-found.png" width="70px;">
                    <h6>Berita Acara Verifikasi belum tersedia. Dokumen ini dapat didownload setelah Tim Verifikasi Menguploadnnya.</h6>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <div class="box-4">
        <div class="padding-x-30 padding-top-10 padding-bottom-30 bg-lightest shadow border-cyan rounded-md border-cyan rounded-md" style="min-height: 370px;">
            <h3 class="text-cyan margin-bottom-30">
                Download Rekomendasi
                <?php if ($model['penilaian']->rekomendasi) : ?>
                    <i class="fa fa-download pull-right"></i>
                <?php else : ?>
                    <i class="fa fa-download pull-right text-red"></i>
                <?php endif; ?>
            </h3>
            <?php if ($model['penilaian']->rekomendasi) : ?>
                <div class="text-center border-light-azure text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/check.png" width="70px;">
                    <h6>Rekomendasi sudah tersedia. Dokumen ini dapat didownload sekarang.</h6>
                    <a href="<?= $model['penilaian']->virtual_rekomendasi_download ?>" target="_blank" class="button border-azure bg-azure hover-bg-lightest hover-text-azure button-block button-lg">Download</a>
                </div>
            <?php else : ?>
                <div class="text-center border-light-azure text-azure padding-15 rounded-xs">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/icon/not-found.png" width="70px;">
                    <h6>Rekomendasi belum tersedia. Dokumen ini dapat didownload setelah Tim Verifikasi Menguploadnnya.</h6>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
