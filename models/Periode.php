<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "periode".
 *
 * @property integer $id
 * @property string $status
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 *
 * @property Aspek[] $aspeks
 * @property Penilaian[] $penilaians
 * @property InstansiPemerintah[] $instansiPemerintahs
 * @property PenilaianDetail[] $penilaianDetails
 * @property PenilaianAspek[] $penilaianAspeks
 */
class Periode extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode';
    }

    public function rules()
    {
        return [
            //id

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //tanggal_mulai
            [['tanggal_mulai'], 'required'],
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'required'],
            [['tanggal_selesai'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspeks()
    {
        return $this->hasMany(Aspek::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaians()
    {
        return $this->hasMany(Penilaian::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiPemerintahs()
    {
        return $this->hasMany(InstansiPemerintah::className(), ['id' => 'id_instansi_pemerintah'])->viaTable('penilaian', ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianDetails()
    {
        return $this->hasMany(PenilaianDetail::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianAspeks()
    {
        return $this->hasMany(PenilaianAspek::className(), ['id_periode' => 'id']);
    }

    public static function getPeriodeAktif()
    {
        return Self::find()->where(['status' => 'Aktif'])->one()->id;
    }
}
