<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "penilaian_detail".
 *
 * @property integer $id
 * @property integer $id_instansi_pemerintah
 * @property integer $id_periode
 * @property integer $id_user_instansi_pemerintah
 * @property integer $id_indikator
 * @property string $kondisi_saat_ini
 * @property string $bukti_wajib
 * @property string $bukti_tambahan_1
 * @property string $bukti_tambahan_2
 * @property string $bukti_tambahan_3
 * @property string $bukti_tambahan_4
 * @property string $bukti_tambahan_5
 * @property string $bukti_tambahan_6
 * @property string $bukti_tambahan_7
 * @property string $bukti_tambahan_8
 * @property string $bukti_tambahan_9
 * @property string $bukti_tambahan_10
 * @property integer $nilai_tim_instansi
 * @property integer $nilai_tim_verifikasi
 * @property string $catatan
 *
 * @property InstansiPemerintah $instansiPemerintah
 * @property UserInstansiPemerintah $userInstansiPemerintah
 * @property Indikator $indikator
 * @property Periode $periode
 */
class PenilaianDetail extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_bukti_wajib_upload;
    public $virtual_bukti_wajib_download;
    
    public $virtual_bukti_tambahan_1_upload;
    public $virtual_bukti_tambahan_1_download;
    
    public $virtual_bukti_tambahan_2_upload;
    public $virtual_bukti_tambahan_2_download;
    
    public $virtual_bukti_tambahan_3_upload;
    public $virtual_bukti_tambahan_3_download;
    
    public $virtual_bukti_tambahan_4_upload;
    public $virtual_bukti_tambahan_4_download;

    public $virtual_bukti_tambahan_5_upload;
    public $virtual_bukti_tambahan_5_download;
    
    public $virtual_bukti_tambahan_6_upload;
    public $virtual_bukti_tambahan_6_download;
    
    public $virtual_bukti_tambahan_7_upload;
    public $virtual_bukti_tambahan_7_download;
    
    public $virtual_bukti_tambahan_8_upload;
    public $virtual_bukti_tambahan_8_download;
    
    public $virtual_bukti_tambahan_9_upload;
    public $virtual_bukti_tambahan_9_download;

    public $virtual_bukti_tambahan_10_upload;
    public $virtual_bukti_tambahan_10_download;

    public static function tableName()
    {
        return 'penilaian_detail';
    }

    public function rules()
    {
        return [
            //id

            //id_instansi_pemerintah
            [['id_instansi_pemerintah'], 'required'],
            [['id_instansi_pemerintah'], 'integer'],
            [['id_instansi_pemerintah'], 'unique', 'targetAttribute' => ['id_instansi_pemerintah', 'id_indikator']],
            [['id_instansi_pemerintah'], 'exist', 'skipOnError' => true, 'targetClass' => InstansiPemerintah::className(), 'targetAttribute' => ['id_instansi_pemerintah' => 'id']],

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //id_user_instansi_pemerintah
            [['id_user_instansi_pemerintah'], 'required'],
            [['id_user_instansi_pemerintah'], 'integer'],
            [['id_user_instansi_pemerintah'], 'exist', 'skipOnError' => true, 'targetClass' => UserInstansiPemerintah::className(), 'targetAttribute' => ['id_user_instansi_pemerintah' => 'id_user']],

            //id_indikator
            [['id_indikator'], 'required'],
            [['id_indikator'], 'integer'],
            [['id_indikator'], 'unique', 'targetAttribute' => ['id_instansi_pemerintah', 'id_indikator']],
            [['id_indikator'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::className(), 'targetAttribute' => ['id_indikator' => 'id']],

            //kondisi_saat_ini
            [['kondisi_saat_ini'], 'required'],
            [['kondisi_saat_ini'], 'string'],

            //bukti_wajib
            [['bukti_wajib'], 'required'],
            [['bukti_wajib'], 'string', 'max' => 256],

            //bukti_tambahan_1
            [['bukti_tambahan_1'], 'string', 'max' => 256],

            //bukti_tambahan_2
            [['bukti_tambahan_2'], 'string', 'max' => 256],

            //bukti_tambahan_3
            [['bukti_tambahan_3'], 'string', 'max' => 256],

            //bukti_tambahan_4
            [['bukti_tambahan_4'], 'string', 'max' => 256],

            //bukti_tambahan_5
            [['bukti_tambahan_5'], 'string', 'max' => 256],

            //bukti_tambahan_6
            [['bukti_tambahan_6'], 'string', 'max' => 256],

            //bukti_tambahan_7
            [['bukti_tambahan_7'], 'string', 'max' => 256],

            //bukti_tambahan_8
            [['bukti_tambahan_8'], 'string', 'max' => 256],

            //bukti_tambahan_9
            [['bukti_tambahan_9'], 'string', 'max' => 256],

            //bukti_tambahan_10
            [['bukti_tambahan_10'], 'string', 'max' => 256],

            //nilai_tim_instansi
            [['nilai_tim_instansi'], 'required'],
            [['nilai_tim_instansi'], 'integer'],

            //nilai_tim_verifikasi
            [['nilai_tim_verifikasi'], 'integer'],

            //catatan
            [['catatan'], 'string'],
            
            //virtual_bukti_*_download
            [['virtual_bukti_wajib_download', 'virtual_bukti_tambahan_1_download', 'virtual_bukti_tambahan_2_download', 'virtual_bukti_tambahan_3_download', 'virtual_bukti_tambahan_4_download', 'virtual_bukti_tambahan_5_download', 'virtual_bukti_tambahan_6_download', 'virtual_bukti_tambahan_7_download', 'virtual_bukti_tambahan_8_download', 'virtual_bukti_tambahan_9_download', 'virtual_bukti_tambahan_10_download'], 'safe'],
            
            //virtual_bukti_*_upload
            [['virtual_bukti_wajib_upload', 'virtual_bukti_tambahan_1_upload', 'virtual_bukti_tambahan_2_upload', 'virtual_bukti_tambahan_3_upload', 'virtual_bukti_tambahan_4_upload', 'virtual_bukti_tambahan_5_upload', 'virtual_bukti_tambahan_6_upload', 'virtual_bukti_tambahan_7_upload', 'virtual_bukti_tambahan_8_upload', 'virtual_bukti_tambahan_9_upload', 'virtual_bukti_tambahan_10_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, xls, xlsx, ppt, pptx, jpg, jpeg, png, pdf, zip, rar', 'maxSize' => 100000 * 1024, 'tooBig' => 'Maximum allowed file size is 10MB'],

            //virtual_bukti_wajib_upload
            [['virtual_bukti_wajib_upload'], 'required',
                'when' => function ($model) { return $model->bukti_wajib == '' || $model->bukti_wajib == null; },
                'whenClient' => 'function (attribute, value) { return vm.$data.penilaian_detail.bukti_wajib == "" || vm.$data.penilaian_detail.bukti_wajib == null; }',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_bukti_wajib_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_wajib_upload');
        if ($this->virtual_bukti_wajib_upload) {
            $this->bukti_wajib = $this->virtual_bukti_wajib_upload->name;
        }

        $this->virtual_bukti_tambahan_1_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_1_upload');
        if ($this->virtual_bukti_tambahan_1_upload) {
            $this->bukti_tambahan_1 = $this->virtual_bukti_tambahan_1_upload->name;
        }

        $this->virtual_bukti_tambahan_2_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_2_upload');
        if ($this->virtual_bukti_tambahan_2_upload) {
            $this->bukti_tambahan_2 = $this->virtual_bukti_tambahan_2_upload->name;
        }

        $this->virtual_bukti_tambahan_3_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_3_upload');
        if ($this->virtual_bukti_tambahan_3_upload) {
            $this->bukti_tambahan_3 = $this->virtual_bukti_tambahan_3_upload->name;
        }

        $this->virtual_bukti_tambahan_4_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_4_upload');
        if ($this->virtual_bukti_tambahan_4_upload) {
            $this->bukti_tambahan_4 = $this->virtual_bukti_tambahan_4_upload->name;
        }

        $this->virtual_bukti_tambahan_5_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_5_upload');
        if ($this->virtual_bukti_tambahan_5_upload) {
            $this->bukti_tambahan_5 = $this->virtual_bukti_tambahan_5_upload->name;
        }

        $this->virtual_bukti_tambahan_6_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_6_upload');
        if ($this->virtual_bukti_tambahan_6_upload) {
            $this->bukti_tambahan_6 = $this->virtual_bukti_tambahan_6_upload->name;
        }

        $this->virtual_bukti_tambahan_7_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_7_upload');
        if ($this->virtual_bukti_tambahan_7_upload) {
            $this->bukti_tambahan_7 = $this->virtual_bukti_tambahan_7_upload->name;
        }

        $this->virtual_bukti_tambahan_8_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_8_upload');
        if ($this->virtual_bukti_tambahan_8_upload) {
            $this->bukti_tambahan_8 = $this->virtual_bukti_tambahan_8_upload->name;
        }

        $this->virtual_bukti_tambahan_9_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_9_upload');
        if ($this->virtual_bukti_tambahan_9_upload) {
            $this->bukti_tambahan_9 = $this->virtual_bukti_tambahan_9_upload->name;
        }

        $this->virtual_bukti_tambahan_10_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_10_upload');
        if ($this->virtual_bukti_tambahan_10_upload) {
            $this->bukti_tambahan_10 = $this->virtual_bukti_tambahan_10_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_bukti_wajib_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_wajib']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_wajib;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_1_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_1']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_1;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_2_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_2']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_2;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_3_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_3']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_3;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_4_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_4']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_4;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_5_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_5']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_5;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_6_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_6']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_6;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_7_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_7']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_7;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_8_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_8']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_8;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_9_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_9']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_9;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_bukti_tambahan_10_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_10']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_10;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_bukti_wajib_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_wajib']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_wajib_upload->saveAs($path . '/' . $this->bukti_wajib);
        }
        
        if ($this->virtual_bukti_tambahan_1_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_1']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_1_upload->saveAs($path . '/' . $this->bukti_tambahan_1);
        }
        
        if ($this->virtual_bukti_tambahan_2_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_2']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_2_upload->saveAs($path . '/' . $this->bukti_tambahan_2);
        }
        
        if ($this->virtual_bukti_tambahan_3_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_3']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_3_upload->saveAs($path . '/' . $this->bukti_tambahan_3);
        }
        
        if ($this->virtual_bukti_tambahan_4_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_4']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_4_upload->saveAs($path . '/' . $this->bukti_tambahan_4);
        }
        
        if ($this->virtual_bukti_tambahan_5_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_5']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_5_upload->saveAs($path . '/' . $this->bukti_tambahan_5);
        }
        
        if ($this->virtual_bukti_tambahan_6_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_6']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_6_upload->saveAs($path . '/' . $this->bukti_tambahan_6);
        }
        
        if ($this->virtual_bukti_tambahan_7_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_7']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_7_upload->saveAs($path . '/' . $this->bukti_tambahan_7);
        }
        
        if ($this->virtual_bukti_tambahan_8_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_8']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_8_upload->saveAs($path . '/' . $this->bukti_tambahan_8);
        }
        
        if ($this->virtual_bukti_tambahan_9_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_9']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_9_upload->saveAs($path . '/' . $this->bukti_tambahan_9);
        }
        
        if ($this->virtual_bukti_tambahan_10_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_10']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_10_upload->saveAs($path . '/' . $this->bukti_tambahan_10);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_wajib']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_wajib;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_1']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_1;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_2']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_2;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_3']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_3;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_4']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_4;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_5']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_5;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_6']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_6;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_7']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_7;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_8']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_8;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_9']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_9;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_10']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_tambahan_10;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->bukti_wajib) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_wajib']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_wajib_download = $path . '/' . $this->bukti_wajib;
        }
        
        if($this->bukti_tambahan_1) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_1']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_1_download = $path . '/' . $this->bukti_tambahan_1;
        }
        
        if($this->bukti_tambahan_2) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_2']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_2_download = $path . '/' . $this->bukti_tambahan_2;
        }
        
        if($this->bukti_tambahan_3) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_3']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_3_download = $path . '/' . $this->bukti_tambahan_3;
        }
        
        if($this->bukti_tambahan_4) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_4']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_4_download = $path . '/' . $this->bukti_tambahan_4;
        }
        
        if($this->bukti_tambahan_5) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_5']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_5_download = $path . '/' . $this->bukti_tambahan_5;
        }
        
        if($this->bukti_tambahan_6) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_6']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_6_download = $path . '/' . $this->bukti_tambahan_6;
        }
        
        if($this->bukti_tambahan_7) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_7']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_7_download = $path . '/' . $this->bukti_tambahan_7;
        }
        
        if($this->bukti_tambahan_8) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_8']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_8_download = $path . '/' . $this->bukti_tambahan_8;
        }
        
        if($this->bukti_tambahan_9) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_9']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_9_download = $path . '/' . $this->bukti_tambahan_9;
        }
        
        if($this->bukti_tambahan_10) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian_detail-bukti_tambahan_10']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_10_download = $path . '/' . $this->bukti_tambahan_10;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_instansi_pemerintah' => 'Id Instansi Pemerintah',
            'id_periode' => 'Id Periode',
            'id_user_instansi_pemerintah' => 'Id User Instansi Pemerintah',
            'id_indikator' => 'Id Indikator',
            'kondisi_saat_ini' => 'Kondisi Saat Ini',
            'bukti_wajib' => 'Bukti Wajib',
            'bukti_tambahan_1' => 'Bukti Tambahan 1',
            'bukti_tambahan_2' => 'Bukti Tambahan 2',
            'bukti_tambahan_3' => 'Bukti Tambahan 3',
            'bukti_tambahan_4' => 'Bukti Tambahan 4',
            'bukti_tambahan_5' => 'Bukti Tambahan 5',
            'bukti_tambahan_6' => 'Bukti Tambahan 6',
            'bukti_tambahan_7' => 'Bukti Tambahan 7',
            'bukti_tambahan_8' => 'Bukti Tambahan 8',
            'bukti_tambahan_9' => 'Bukti Tambahan 9',
            'bukti_tambahan_10' => 'Bukti Tambahan 10',
            'nilai_tim_instansi' => 'Nilai Tim Instansi',
            'nilai_tim_verifikasi' => 'Nilai Tim Verifikasi',
            'catatan' => 'Catatan',
            'virtual_bukti_wajib_download' => 'Bukti Wajib',
            'virtual_bukti_wajib_upload' => 'Bukti Wajib',
            'virtual_bukti_tambahan_1_download' => 'Bukti Tambahan 1',
            'virtual_bukti_tambahan_1_upload' => 'Bukti Tambahan 1',
            'virtual_bukti_tambahan_2_download' => 'Bukti Tambahan 2',
            'virtual_bukti_tambahan_2_upload' => 'Bukti Tambahan 2',
            'virtual_bukti_tambahan_3_download' => 'Bukti Tambahan 3',
            'virtual_bukti_tambahan_3_upload' => 'Bukti Tambahan 3',
            'virtual_bukti_tambahan_4_download' => 'Bukti Tambahan 4',
            'virtual_bukti_tambahan_4_upload' => 'Bukti Tambahan 4',
            'virtual_bukti_tambahan_5_download' => 'Bukti Tambahan 5',
            'virtual_bukti_tambahan_5_upload' => 'Bukti Tambahan 5',
            'virtual_bukti_tambahan_6_download' => 'Bukti Tambahan 6',
            'virtual_bukti_tambahan_6_upload' => 'Bukti Tambahan 6',
            'virtual_bukti_tambahan_7_download' => 'Bukti Tambahan 7',
            'virtual_bukti_tambahan_7_upload' => 'Bukti Tambahan 7',
            'virtual_bukti_tambahan_8_download' => 'Bukti Tambahan 8',
            'virtual_bukti_tambahan_8_upload' => 'Bukti Tambahan 8',
            'virtual_bukti_tambahan_9_download' => 'Bukti Tambahan 9',
            'virtual_bukti_tambahan_9_upload' => 'Bukti Tambahan 9',
            'virtual_bukti_tambahan_10_download' => 'Bukti Tambahan 10',
            'virtual_bukti_tambahan_10_upload' => 'Bukti Tambahan 10',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiPemerintah()
    {
        return $this->hasOne(InstansiPemerintah::className(), ['id' => 'id_instansi_pemerintah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInstansiPemerintah()
    {
        return $this->hasOne(UserInstansiPemerintah::className(), ['id_user' => 'id_user_instansi_pemerintah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::className(), ['id' => 'id_indikator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }
}
