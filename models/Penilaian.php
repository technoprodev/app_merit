<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "penilaian".
 *
 * @property integer $id
 * @property integer $id_instansi_pemerintah
 * @property integer $id_periode
 * @property string $status
 * @property string $berita_acara_penilaian
 * @property string $berita_acara_verifikasi
 * @property string $rekomendasi
 *
 * @property InstansiPemerintah $instansiPemerintah
 * @property Periode $periode
 */
class Penilaian extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_berita_acara_penilaian_upload;
    public $virtual_berita_acara_penilaian_download;
    public $virtual_berita_acara_verifikasi_upload;
    public $virtual_berita_acara_verifikasi_download;
    public $virtual_rekomendasi_upload;
    public $virtual_rekomendasi_download;

    public static function tableName()
    {
        return 'penilaian';
    }

    public function rules()
    {
        return [
            //id

            //id_instansi_pemerintah
            [['id_instansi_pemerintah'], 'required'],
            [['id_instansi_pemerintah'], 'integer'],
            [['id_instansi_pemerintah'], 'unique', 'targetAttribute' => ['id_instansi_pemerintah', 'id_periode']],
            [['id_instansi_pemerintah'], 'exist', 'skipOnError' => true, 'targetClass' => InstansiPemerintah::className(), 'targetAttribute' => ['id_instansi_pemerintah' => 'id']],

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'unique', 'targetAttribute' => ['id_instansi_pemerintah', 'id_periode']],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //berita_acara_penilaian
            [['berita_acara_penilaian'], 'string', 'max' => 256],

            //berita_acara_verifikasi
            [['berita_acara_verifikasi'], 'string', 'max' => 256],

            //rekomendasi
            [['rekomendasi'], 'string', 'max' => 256],
            
            //virtual_berita_acara_penilaian_download
            [['virtual_berita_acara_penilaian_download'], 'safe'],
            
            //virtual_berita_acara_penilaian_upload
            [['virtual_berita_acara_penilaian_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, xls, xlsx, ppt, pptx, jpg, jpeg, png, pdf', 'maxSize' => 100000 * 1024, 'tooBig' => 'Maximum allowed file size is 10MB'],
            
            //virtual_berita_acara_verifikasi_download
            [['virtual_berita_acara_verifikasi_download'], 'safe'],
            
            //virtual_berita_acara_verifikasi_upload
            [['virtual_berita_acara_verifikasi_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, xls, xlsx, ppt, pptx, jpg, jpeg, png, pdf', 'maxSize' => 100000 * 1024, 'tooBig' => 'Maximum allowed file size is 10MB'],
            
            //virtual_rekomendasi_download
            [['virtual_rekomendasi_download'], 'safe'],
            
            //virtual_rekomendasi_upload
            [['virtual_rekomendasi_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, xls, xlsx, ppt, pptx, jpg, jpeg, png, pdf', 'maxSize' => 100000 * 1024, 'tooBig' => 'Maximum allowed file size is 10MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_berita_acara_penilaian_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_berita_acara_penilaian_upload');
        if ($this->virtual_berita_acara_penilaian_upload) {
            $this->berita_acara_penilaian = $this->virtual_berita_acara_penilaian_upload->name;
        }

        $this->virtual_berita_acara_verifikasi_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_berita_acara_verifikasi_upload');
        if ($this->virtual_berita_acara_verifikasi_upload) {
            $this->berita_acara_verifikasi = $this->virtual_berita_acara_verifikasi_upload->name;
        }

        $this->virtual_rekomendasi_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_rekomendasi_upload');
        if ($this->virtual_rekomendasi_upload) {
            $this->rekomendasi = $this->virtual_rekomendasi_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_berita_acara_penilaian_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_penilaian']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->berita_acara_penilaian;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_berita_acara_verifikasi_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_verifikasi']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->berita_acara_verifikasi;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        if ($this->virtual_rekomendasi_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['penilaian-rekomendasi']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->rekomendasi;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_berita_acara_penilaian_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_penilaian']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_berita_acara_penilaian_upload->saveAs($path . '/' . $this->berita_acara_penilaian);
        }
        
        if ($this->virtual_berita_acara_verifikasi_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_verifikasi']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_berita_acara_verifikasi_upload->saveAs($path . '/' . $this->berita_acara_verifikasi);
        }
        
        if ($this->virtual_rekomendasi_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['penilaian-rekomendasi']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_rekomendasi_upload->saveAs($path . '/' . $this->rekomendasi);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_penilaian']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->berita_acara_penilaian;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian-berita_acara_verifikasi']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->berita_acara_verifikasi;
        if (is_file($filePath)) unlink($filePath);
        
        $fileRoot = Yii::$app->params['configurations_file']['penilaian-rekomendasi']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->rekomendasi;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->berita_acara_penilaian) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian-berita_acara_penilaian']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_berita_acara_penilaian_download = $path . '/' . $this->berita_acara_penilaian;
        }
        
        if($this->berita_acara_verifikasi) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian-berita_acara_verifikasi']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_berita_acara_verifikasi_download = $path . '/' . $this->berita_acara_verifikasi;
        }
        
        if($this->rekomendasi) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['penilaian-rekomendasi']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_rekomendasi_download = $path . '/' . $this->rekomendasi;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_instansi_pemerintah' => 'Id Instansi Pemerintah',
            'id_periode' => 'Id Periode',
            'status' => 'Status',
            'berita_acara_penilaian' => 'Berita Acara Penilaian',
            'berita_acara_verifikasi' => 'Berita Acara Verifikasi',
            'rekomendasi' => 'Rekomendasi',
            'virtual_berita_acara_penilaian_download' => 'Berita Acara Penilaian',
            'virtual_berita_acara_penilaian_upload' => 'Berita Acara Penilaian',
            'virtual_berita_acara_verifikasi_download' => 'Berita Acara Verifikasi',
            'virtual_berita_acara_verifikasi_upload' => 'Berita Acara Verifikasi',
            'virtual_rekomendasi_download' => 'Rekomendasi',
            'virtual_rekomendasi_upload' => 'Rekomendasi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiPemerintah()
    {
        return $this->hasOne(InstansiPemerintah::className(), ['id' => 'id_instansi_pemerintah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    public static function getRealStatus($idInstansiPemerintah = null)
    {
        if (!$idInstansiPemerintah) $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;

        $countIndikator = count(Indikator::find()
            ->join('LEFT JOIN', 'aspek k', 'k.id = indikator.id_aspek')
            ->where([
                'id_periode' => \app_merit\models\Periode::getPeriodeAktif(),
            ])
            ->all());

        $countPenilaianDetail = count(PenilaianDetail::find()
            ->where([
                'id_periode' => \app_merit\models\Periode::getPeriodeAktif(),
                'id_instansi_pemerintah' => $idInstansiPemerintah,
            ])
            ->all());
        
        if ($countPenilaianDetail == 0) {
            return 'Belum Input';
        } else if ($countPenilaianDetail == $countIndikator) {
            return 'Selesai Input';
        } else if ($countPenilaianDetail < $countIndikator) {
            return 'Sedang Input';
        }
    }
}
