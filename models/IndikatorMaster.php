<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "indikator_master".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $id_aspek_master
 * @property string $1
 * @property string $2
 * @property string $3
 * @property string $4
 * @property string $5
 * @property string $6
 * @property string $bobot
 * @property integer $urutan
 *
 * @property AspekMaster $aspekMaster
 */
class IndikatorMaster extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'indikator_master';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string'],

            //id_aspek_master
            [['id_aspek_master'], 'required'],
            [['id_aspek_master'], 'integer'],
            [['id_aspek_master'], 'exist', 'skipOnError' => true, 'targetClass' => AspekMaster::className(), 'targetAttribute' => ['id_aspek_master' => 'id']],

            //1
            [['1'], 'required'],
            [['1'], 'string'],

            //2
            [['2'], 'required'],
            [['2'], 'string'],

            //3
            [['3'], 'string'],

            //4
            [['4'], 'string'],

            //5
            [['5'], 'string'],

            //6
            [['6'], 'string'],

            //bobot
            [['bobot'], 'required'],
            [['bobot'], 'number'],

            //urutan
            [['urutan'], 'required'],
            [['urutan'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'id_aspek_master' => 'Id Aspek Master',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            'bobot' => 'Bobot',
            'urutan' => 'Urutan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspekMaster()
    {
        return $this->hasOne(AspekMaster::className(), ['id' => 'id_aspek_master']);
    }
}
