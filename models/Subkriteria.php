<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "indikator".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $id_aspek
 * @property string $1
 * @property string $2
 * @property string $3
 * @property string $4
 * @property string $5
 * @property string $6
 * @property double $bobot
 * @property integer $urutan
 *
 * @property PenilaianDetail[] $penilaianDetails
 * @property Aspek $aspek
 */
class Subaspek extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'indikator';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string'],

            //id_aspek
            [['id_aspek'], 'required'],
            [['id_aspek'], 'integer'],
            [['id_aspek'], 'exist', 'skipOnError' => true, 'targetClass' => Aspek::className(), 'targetAttribute' => ['id_aspek' => 'id']],

            //1
            [['1'], 'required'],
            [['1'], 'string'],

            //2
            [['2'], 'required'],
            [['2'], 'string'],

            //3
            [['3'], 'string'],

            //4
            [['4'], 'string'],

            //5
            [['5'], 'string'],

            //6
            [['6'], 'string'],

            //bobot
            [['bobot'], 'required'],
            [['bobot'], 'number'],

            //urutan
            [['urutan'], 'required'],
            [['urutan'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'id_aspek' => 'Id Aspek',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            'bobot' => 'Bobot',
            'urutan' => 'Urutan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianDetails()
    {
        return $this->hasMany(PenilaianDetail::className(), ['id_indikator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspek()
    {
        return $this->hasOne(Aspek::className(), ['id' => 'id_aspek']);
    }
}
