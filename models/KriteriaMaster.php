<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "aspek_master".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $urutan
 *
 * @property SubaspekMaster[] $indikatorMasters
 */
class AspekMaster extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'aspek_master';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],

            //urutan
            [['urutan'], 'required'],
            [['urutan'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'urutan' => 'Urutan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubaspekMasters()
    {
        return $this->hasMany(SubaspekMaster::className(), ['id_aspek_master' => 'id']);
    }
}
