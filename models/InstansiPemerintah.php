<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "instansi_pemerintah".
 *
 * @property integer $id
 * @property string $nama
 * @property string $jenis
 * @property string $id_provinces
 * @property string $id_regencies
 * @property string $logo
 * @property string $updated_at
 *
 * @property Provinces $provinces
 * @property Regencies $regencies
 * @property Penilaian[] $penilaians
 * @property Periode[] $periodes
 * @property PenilaianDetail[] $penilaianDetails
 * @property Indikator[] $indikators
 * @property UserInstansiPemerintah[] $userInstansiPemerintahs
 */
class InstansiPemerintah extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_logo_upload;
    public $virtual_logo_download;

    public static function tableName()
    {
        return 'instansi_pemerintah';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'updated_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //jenis
            [['jenis'], 'required'],
            [['jenis'], 'string'],

            //id_provinces
            [['id_provinces'], 'string', 'max' => 2],
            [['id_provinces'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Provinces::className(), 'targetAttribute' => ['id_provinces' => 'id']],

            //id_regencies
            [['id_regencies'], 'string', 'max' => 4],
            [['id_regencies'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Regencies::className(), 'targetAttribute' => ['id_regencies' => 'id']],

            //logo
            [['logo'], 'string', 'max' => 256],

            //updated_at
            [['updated_at'], 'safe'],
            
            //virtual_logo_download
            [['virtual_logo_download'], 'safe'],
            
            //virtual_logo_upload
            [['virtual_logo_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png', 'maxSize' => 2000 * 1024, 'tooBig' => 'Maximum allowed file size is 2MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_logo_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_logo_upload');
        if ($this->virtual_logo_upload) {
            $this->logo = $this->virtual_logo_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_logo_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['instansi_pemerintah-logo']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->logo;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_logo_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['instansi_pemerintah-logo']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_logo_upload->saveAs($path . '/' . $this->logo);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = Yii::$app->params['configurations_file']['instansi_pemerintah-logo']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->logo;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->logo) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['instansi_pemerintah-logo']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_logo_download = $path . '/' . $this->logo;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'id_provinces' => 'Provinsi',
            'id_regencies' => 'Kabupaten/Kota',
            'logo' => 'Logo',
            'virtual_logo_download' => 'Logo',
            'virtual_logo_upload' => 'Logo',
            'updated_at' => 'Terakhir Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinces()
    {
        return $this->hasOne(\technosmart\modules\location\models\Provinces::className(), ['id' => 'id_provinces']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasOne(\technosmart\modules\location\models\Regencies::className(), ['id' => 'id_regencies']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaians()
    {
        return $this->hasMany(Penilaian::className(), ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodes()
    {
        return $this->hasMany(Periode::className(), ['id' => 'id_periode'])->viaTable('penilaian', ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianDetails()
    {
        return $this->hasMany(PenilaianDetail::className(), ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndikators()
    {
        return $this->hasMany(Indikator::className(), ['id' => 'id_indikator'])->viaTable('penilaian_detail', ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianAspeks()
    {
        return $this->hasMany(PenilaianAspek::className(), ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInstansiPemerintahs()
    {
        return $this->hasMany(UserInstansiPemerintah::className(), ['id_instansi_pemerintah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianAktif()
    {
        return $this->hasOne(Penilaian::className(), ['id_instansi_pemerintah' => 'id'])->where(['penilaian.id_periode' => \app_merit\models\Periode::getPeriodeAktif()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalAkhir()
    {
        return $this->hasOne(Penilaian::className(), ['id_instansi_pemerintah' => 'id'])->where(['penilaian.id_periode' => \app_merit\models\Periode::getPeriodeAktif()]);
    }
}
