<?php
namespace app_merit\models;

use Yii;
use app_merit\models\User;

class Login extends \technosmart\models\Login
{
    public $captcha;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['captcha', 'required'];
        $rules[] = ['captcha', 'captcha', 'captchaAction' => 'captcha/captcha'];
        return $rules;
    }

    protected function getUser()
    {
        if ($this->user === null) {
            switch ($this->scenario) {
                case 'using-login':
                    $this->user = User::findByLogin($this->login);
                    break;
                case 'using-username':
                    $this->user = User::findByUsername($this->username);
                    break;
                case 'using-email':
                    $this->user = User::findByEmail($this->email);
                    break;
                case 'using-phone':
                    $this->user = User::findByPhone($this->phone);
                    break;
                default:
                    $this->user = User::findByLogin($this->login);
            }
        }

        return $this->user;
    }
}