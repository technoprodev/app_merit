<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "penilaian_aspek".
 *
 * @property integer $id
 * @property integer $id_instansi_pemerintah
 * @property integer $id_periode
 * @property integer $id_aspek
 * @property string $catatan
 *
 * @property InstansiPemerintah $instansiPemerintah
 * @property Periode $periode
 * @property Aspek $aspek
 */
class PenilaianAspek extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'penilaian_aspek';
    }

    public function rules()
    {
        return [
            //id

            //id_instansi_pemerintah
            [['id_instansi_pemerintah'], 'required'],
            [['id_instansi_pemerintah'], 'integer'],
            [['id_instansi_pemerintah'], 'exist', 'skipOnError' => true, 'targetClass' => InstansiPemerintah::className(), 'targetAttribute' => ['id_instansi_pemerintah' => 'id']],

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //id_aspek
            [['id_aspek'], 'required'],
            [['id_aspek'], 'integer'],
            [['id_aspek'], 'exist', 'skipOnError' => true, 'targetClass' => Aspek::className(), 'targetAttribute' => ['id_aspek' => 'id']],

            //catatan
            [['catatan'], 'required'],
            [['catatan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_instansi_pemerintah' => 'Id Instansi Pemerintah',
            'id_periode' => 'Id Periode',
            'id_aspek' => 'Id Aspek',
            'catatan' => 'Catatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiPemerintah()
    {
        return $this->hasOne(InstansiPemerintah::className(), ['id' => 'id_instansi_pemerintah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAspek()
    {
        return $this->hasOne(Aspek::className(), ['id' => 'id_aspek']);
    }
}
