<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "aspek".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $nama
 * @property integer $urutan
 *
 * @property Periode $periode
 * @property PenilaianAspek[] $penilaianAspeks
 * @property Subaspek[] $indikators
 */
class Aspek extends \technosmart\yii\db\ActiveRecord
{
    public $idPenilaianAspek;
    public $catatanAspek;

    public static function tableName()
    {
        return 'aspek';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],

            //urutan
            [['urutan'], 'integer'],
            [['urutan'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Id Periode',
            'nama' => 'Nama',
            'urutan' => 'Urutan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianAspeks()
    {
        return $this->hasMany(PenilaianAspek::className(), ['id_aspek' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubaspeks()
    {
        return $this->hasMany(Subaspek::className(), ['id_aspek' => 'id']);
    }
}
