<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "user_kasn".
 *
 * @property integer $id_user
 * @property string $jabatan_di_kasn
 * @property string $jabatan_di_sk
 *
 * @property User $user
 */
class UserKasn extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_kasn';
    }

    public function rules()
    {
        return [
            //id_user
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //jabatan_di_kasn
            [['jabatan_di_kasn'], 'string', 'max' => 256],

            //jabatan_di_sk
            [['jabatan_di_sk'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'jabatan_di_kasn' => 'Jabatan di KASN',
            'jabatan_di_sk' => 'Jabatan di SK',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
