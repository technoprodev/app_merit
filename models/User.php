<?php
namespace app_merit\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            //username
            [['username'], 'required'],
            
            //email
            [['email'], 'required'],
        ]);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Nama',
            'email' => 'Email',
            'phone' => 'Handhpone',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'password_repeat' => 'Ulangi Password',
            'password_old' => 'Password Lama',
        ];
    }

    /*public static function findByLogin($login)
    {
        return static::find()
            ->join('INNER JOIN', 'user_extend ue', 'ue.id = user.id')
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }*/

    public function getUserInstansiPemerintah()
    {
        return $this->hasOne(UserInstansiPemerintah::className(), ['id_user' => 'id']);
    }

    public function getUserKasn()
    {
        return $this->hasOne(UserKasn::className(), ['id_user' => 'id']);
    }
}