<?php
namespace app_merit\models;

use Yii;

/**
 * This is the model class for table "user_instansi_pemerintah".
 *
 * @property integer $id_user
 * @property integer $id_instansi_pemerintah
 * @property string $jabatan_di_instansi
 * @property string $jabatan_di_sk
 *
 * @property PenilaianDetail[] $penilaianDetails
 * @property User $user
 * @property InstansiPemerintah $instansiPemerintah
 */
class UserInstansiPemerintah extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_instansi_pemerintah';
    }

    public function rules()
    {
        return [
            //id_user
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //id_instansi_pemerintah
            [['id_instansi_pemerintah'], 'required'],
            [['id_instansi_pemerintah'], 'integer'],
            [['id_instansi_pemerintah'], 'exist', 'skipOnError' => true, 'targetClass' => InstansiPemerintah::className(), 'targetAttribute' => ['id_instansi_pemerintah' => 'id']],

            //jabatan_di_instansi
            [['jabatan_di_instansi'], 'string', 'max' => 256],

            //jabatan_di_sk
            [['jabatan_di_sk'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'id_instansi_pemerintah' => 'Id Instansi Pemerintah',
            'jabatan_di_instansi' => 'Jabatan di Instansi',
            'jabatan_di_sk' => 'Jabatan di SK',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenilaianDetails()
    {
        return $this->hasMany(PenilaianDetail::className(), ['id_user_instansi_pemerintah' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiPemerintah()
    {
        return $this->hasOne(InstansiPemerintah::className(), ['id' => 'id_instansi_pemerintah']);
    }
}
