server dev
	install forticlient https://forticlient.com/downloads
		Connection Name : KASN
		Remote Gateway : 103.215.25.138
		Customize Port : 1443
	putty
		119.252.168.236
		11.11.1.8 (work now)
		Sipint3r-#@!

server prod
	103.215.25.139:2083
	sipinter
	Sipint3r-#@!

Sipinter: Sistem Informasi Penilaian Mandiri Sistem Merit
Siska: Sistem informasi penilaian kualitas seleksi terbuka

Instansi Pemerintah
	Dashboard
		BA muncul jika sudah selesai input seluruh aspek
			Tombol Print Berita Acara by new page (muncul jika semua penilaian sudah lengkap)
				judul: Berita Acara Pelaksanaan Penilaian Mandiri oleh Instansi
				info: instansi, tanggal pertama kali input penilaian, daftar tim penilai (no, nama, jabatan, kontak, ttd yg isinya kosong)
				pernyataan: Dengan ini menyatakan bahwa telah ...
				paragraph list
					Aspek 1
					semua Kondisi Saat Ini masing2 indikator
					nilai total aspek tsb
					input catatan *
				pernyataan: Demikian berita acara ...
				info: Kota, Tanggal server, kasih jarak, Nama ketua
				submit: Print BA
				bisa dilihat berkali2 dan di print lagi
			Ada tombol Upload Berita Acara by new page (muncul jika sudah Print)
				jika sudah milih file, tombol submit nya muncul
			tombol print BA dan upload BA hilang kalau sudah pernah upload BA
			selama sudah pernah upload dan menunggu rekomendasi, tidak ada info apapun diatas TAB
		Rekomendasi KASN muncul ketika KASN sudah upload
		Tab
			per aspek
			cth: Aspek 1 (2/5)
			2/5 = informasi jumlah sub yang sudah diinput / total yang harus diinput
			2/5 bewarna merah jika belum lengkap & biru jika sudah lengkap
			pindah tab refresh, tapi judul halaman masih sama
			list
				action by popup
					Isi Penilaian (jika masih kosong)
					Edit Penilaian (jika sudah pernah diisi)
					Lihat Detail (jika periode sudah ditutup)
				status
					buletan merah kedap kedip jika kosong
					buletan biru jika udh diisi
				action penilaian
					info aspek
					info sub aspek
					milih skor *
					kondisi saat ini *
					bukti 1 *
					bukti tambahan dinamis tidak wajib maks 5
					ditambahkan beberapa bukti wajib, dimana masing-masing indikator bisa memiliki persyaratan bukti yang berbeda-beda
					instansi pemerintah bisa download template bukti yang wajib
					submit & refresh ke halaman dashboard#tab
		input Catatan Hasil Penilaian Mandiri muncul jika sudah selesai input seluruh aspek
	Penilaian Mandiri
		judul “Hasil Penilaian Mandiri Sistem Merit oleh tim penilai instansi”
		list
			nomor
			aspek (nama aspek (persen %))
			indikator
			kondisi saat ini isinya tombol popup untuk melihat detail
			bobot yang ditentukan admin (cth: 5) semua bobot 5 kan ya ?
			skor yang diinput tim penilai (cth: 3)
			nilai (cth: bobot * skor = 15)
		permasing2 aspek ada nilai sub total
		total nilai (380/400), catatan
	Profile
		username (tidak bisa diedit), password bisa diganti, instansi bisa direname, kota bisa milih
		tim penilai bisa nambah secara dinamis (nama, jabatan di instansi, kontak (sudah ada +628 isi lanjutannya saja) & jabatan dalam SK tim (pilih siapa yg jadi ketua) ), SK yang wajib diupload ulang jika nama berubah, jabatan di instansi berubah, posisi ketua berubah
KASN
	Dashboard
		IP Daerah by Maps: peta yang ada titik provinsi, simbol merah kategori1, simbol orange kategori2, simbol hijau kategori3, simbol biru kategori 4, yang merah kedip2 lebih cepat dari kedip2nya orange
		IP Daerah by Maps: ketika simbol di hover : muncul info total verifikasi & kategori berapa
		IP pusat by table: kolom buletan yang warnanya sama kaya maps, nama instansi, total verifikasi, kategori
	Verifikasi
		Reminder otomatis by sms, 2 minggu sebelum, seminggu sebelum dan 3 hari sebelum, menunggu pengurusan SMS gateway di Zenziva.net
		list instansi sort by yang sudah upload BA
			no
			nama instansi
			status
				belum input
				sedang input
				selesai input
				selesai upload BA
				sudah diverifikasi
				sudah print BA
				sudah upload BA Verifikasi
			action
				lihat penilaian by popup (sedang input & selesai input) (sama seperti verifikasi tp ngga ada input & kolomnya tetap ada)
				verifikasi by new page (upload BA)
					judul: Formulir penilaian oleh tim verifikasi
					info
						nama instansi
						tim verifikasi beserta kontak
						jabatan
					list
						no
						aspek
						sub aspek
						nilai tim instansi
						nilai tim verifikasi (awalnya kosong)
						catatan (awalnya kosong)
						action
							input verifikasi by pop up
								info: Aspek, Sub Aspek, Kondisi saat ini, Bukti
								input: nilai verifikasi, catatan
				download BA (upload BA)
				edit verifikasi (sudah diverifikasi, sudah print & sudah upload BA)
				Print BA by new page (sudah diverifikasi, sudah print, Sudah upload BA)
					judul: Berita Acara Penilaian oleh Tim Verifikasi
					info: instansi, daftar tim verifikasi (no, nama, jabatan, ttd yg isinya kosong)
					pernyataan: Dengan ini menyatakan bahwa telah ...
					Aspek 1, Nilai (total per aspek), input catatan *
					pernyataan: Demikian berita acara ..., Kota, Tanggal server, kasih jarak, Nama ketua, tombol Print
					bisa dilihat berkali2 dan di print lagi
				Upload BA (sudah print, sudah pernah upload BA)
				upload rekomendasi (sudah print, sudah pernah upload BA)
	Instansi
		Daftar semua instansi
		klik detail instansi dan lihat profile seperti yang dilihan instansi (password reset saja, tim penilai & seluruh riwayat SK hanya bisa dilihat)
		tambahan klik detail: kategori (Instansi pemerintah pusah & instansi pemerintah daerah), milih provinsi (untuk keperluan dashboard KASN)
	Profile
		username (tidak bisa diedit), password bisa diganti
		tim verifikasi bisa nambah secara dinamis (nama, jabatan di instansi, kontak (sudah ada +628 isi lanjutannya saja) & jabatan dalam SK tim (pilih siapa yg jadi ketua) ), SK yang wajib diupload ulang jika nama berubah, jabatan di instansi berubah, posisi ketua berubah
General
	Klik diluar popup tidak close
	pengurusan fitur sms otomatis ke vendor yang menyediakan layanan SMS gateway (seperti Zenziva, bisa dilihat di google mengenai jenis paket layanan dan kontak yang bisa dihubungi)
	sub aspek diganti indikator
	tambahkan nomor yang bisa dihubungi
	faq
	contact us yang memungkinkan instansi pemerintah bertanya dan KASN mendapat notifikasi pertayaan masuk melalui email
	kasn maps & benerin format ip pusat di tabel dashboard kasn
	ip kasn : datatables di semua table, tambahin tombol search di list verifikasi
	tambahin icon ditombol verifikasi dan kembali
	sebelum nama instansi ditambahin logo instansi
	tombol export dikasih logo
	halaman verifikasi dibagi perkementrian, lembaga, provinsi & kabupaten/kota

28/06/2018
	diuji coba pada semua browser
	tampilan instansi pemerintah pusat diganti menjadi bar chart
	tampilan instansi pemerintah daerah diganti dengan geo spatial
		badan informasi geo spatial menyediakan open source peta spatial

16/06/2018
	captcha
	1 instansi pemerintah hanya 1 user saja, tapi bisa digunakan untuk login secara paralel.
	1 akun tersebut bisa menambahkan stafnya siapa saja, tapi bukan untuk login, melainkan hanya sebagai catatan di berita acara
	informasi profile di dashboard baik di ip dan kasn
	yang dihalaman kasn, penilaian mandiri diganti verifikasi
	yang dihalaman ip, menu dashboard diganti input penilaian
	yang dihalaman ip, menu penilaian mandiri diganti hasil penilaian
	yang dihalaman ip, nama kondisi saat ini diganti dengan nama skor, inputan kondisi saat ini dimunculin sendiri dan tipenya bercerita
	urutannya disesuaikan dan bobot nya tetap dimunculkan, pokoknya sesuai dengan lampiran

1 ip 1 user
ip nambahin data staf
info profile di dashboard ip dan kasn

25/07/18
	dependen dropdown di regency
	Pada saat membuat akun IP, apabila kategori K/L maka pilihan Provinsi secara otomatis tidak bisa (hilang).
	Seluruh menu menggunakan bahasa Indonesia
	Ketika sudah sukses membuat akun IP, maka setelah “save” pop  up hilang otomatis (muncul 2 detik)
	Tombol Instansi Pemerintah ditempatkan pada lokasi yang mudah dilihat oleh User.
	PR untuk LIAN: Memberikan database K/L/Prov/Kab/Kota ke FANDI PRADANA.
	By default : short by update (Pada saat create IP)
	Typo dalam menu: “Jabatan Di Sk” huruf sesuaikan dengan EYD
	Sediakan tombol “BACK” halaman sebelumnya
	Pada Dashboard Role IP : Data Instansi belum mengambil dari Database
	Upload bukti tambahan sampai dengan 10 Bukti
	Belum bisa upload bukti tambahan
	Upload BA Penilaian error
	Download BA Penilaian error
	Nilai Tim Instansi pada Role KASN warna diganti dan center
	Nilai Tim Verifikasi Hanya sampai level 4
	Nilai Tim Verifikasi perlu dimunculkan keterangannya dalam pop up 
	Warna dan design setiap pop up perlu diubah agar menarik
	Apabila download maka muncul dalam newtab
	Tampilan pada upload BA Penilaian perlu ditambahkan logo/symbol yang berbeda sesuai dengan kebutuha
	Pada saat KASN melakukan verifikasi penilaian setelah disubmit jangan kembali ke halaman awal list instansi pemerintah selain itu diharapkan dapat ali pada aspek/indicator yang baru diverifikasi
	detail catatan kasn pada saat verifikasi masih belum muncul
	Tambahkan petunjuk atau keterangan yang memudahkan tim verifikasi untuk melakukan tahap selanjutnya apabila sudah selesai melakukan verifikasi
	Urutan kolom dalam halaman verifikasi adalah download BA Penilaian dahulu baru Verifikasi dan selanjutnya BA Verifikasi dan terakhir Rekomendasi
	Dalam Role KASN Profile Instansi Pemerintah ditampilkan logo masing-masing instansi
	Dalam Reset password ditulis memakai Bahasa Indonesia dan ada keterangan Password Lama dan Password Baru
	Pada Dashboard langsung ditulis Nama Instansi masing-masing (Penilaian Mandiri Provinsi Jawa Barat) dan terdapat LOGO Instansi
	Nama dan email dalam Dashboard dihilangkan saja
	Seluruh dashboard Profile di Role Instansi Pemerintah design header seragam
	Pada Role KASN di halaman verifikasi perlu ditambahkan catatan untuk masing-masing aspek yaitu dengan menambahkan row baru dan terdapat tombol t catatan aspek dikolom action. Kemudian catatan tersebut dapat dilihat pada Role Instansi Pemerintah halaman hasil verifikasi dibagian paling h dan didalam kotak besar tersendiri. Selanjutnya perlu ditambahkan sub total nilai yang dapat dijumlahkan (mengikuti table excel)
	Tambahin tanda bintang required di bukti wajib
	Merah belum upload, biru Upload ulang dihalaman verifikasi KASN
	Aspek dan indicator bisa ditambah, edit dan hapus
