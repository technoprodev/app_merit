<?php
namespace app_merit\controllers;

use Yii;
use technosmart\yii\web\Controller;

class HomeController extends Controller
{
    public function actionTentang()
    {
        $this->layout = 'auth';
        return $this->render('tentang', [
            'title' => 'Tentang Sipinter',
        ]);
    }

    public function actionKontak()
    {
        $this->layout = 'auth';
        return $this->render('kontak', [
            'title' => 'Kontak Sipinter',
        ]);
    }
}