<?php
namespace app_merit\controllers;

use Yii;
use app_merit\models\User;
use app_merit\models\UserExtend;
use technosmart\yii\web\Controller;

class UserController extends Controller
{
    public static $permissions = [
        'view', 'create', 'update', 'reset-password'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view'], 'view'],
                [['index', 'view', 'create'], 'create'],
                [['index', 'view', 'update'], 'update'],
                [['index', 'view', 'reset-password'], 'reset-password'],
                [['index', 'view', 'delete'], 'delete'],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.name',
                'u.username',
                'u.email',
                'u.phone',
                'u.status',
                'ue.sex AS sex',
            ])
            ->from('user u')
            ->join('INNER JOIN', 'user_extend ue', 'ue.id = u.id')
        ;

        return $this->datatables($query, $post = Yii::$app->request->post(), User::getDb());
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Users',
            ]);
        }

        $model['user'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of ' . $model['user']->name,
        ]);
    }

    public function actionCreate()
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user_extend'] = isset($model['user']->user) ? $model['user']->user : new UserExtend();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_extend']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['user_extend']->id = $model['user']->id;
                if (!$model['user_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-create', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Create New User',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionUpdate($id)
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user_extend'] = isset($model['user']->user) ? $model['user']->user : new UserExtend();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_extend']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['user_extend']->id = $model['user']->id;
                if (!$model['user_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Update ' . $model['user']->name . ' (username: ' . $model['user']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionResetPassword($id)
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user']->scenario = 'password';
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-reset-password', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Reset Password ' . $model['user']->name . ' (username: ' . $model['user']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        $model->password = 'dummy password';
        $model->password_repeat = 'dummy password';

        if ($model->save()) {
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
}