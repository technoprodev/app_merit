<?php
namespace app_merit\controllers\kasn;

use Yii;
use app_merit\models\User;
use app_merit\models\UserKasn;
use technosmart\yii\web\Controller;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kasn']]
            ]),
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = User::find()
            ->leftJoin('user_kasn uk', '`uk`.`id_user` = `user`.`id`')
            ->where(['id' => $id])
            ->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.name',
                'u.email',
                'u.username',
                'u.phone',
                'uk.jabatan_di_kasn',
                'uk.jabatan_di_sk',
            ])
            ->from('user u')
            ->innerJoin('user_kasn uk', '`uk`.`id_user` = `u`.`id`')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), User::getDb());
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar User KASN',
            ]);
        }

        $model['user'] = $this->findModel($id);
        $model['user_kasn'] = $this->findModel($id)->userKasn;
        return $this->render('detail', [
            'model' => $model,
            'title' => 'Detail ' . $model['user']->name,
        ]);
    }

    public function actionCreate()
    {
        $error = true;

        $model['user'] = new User(['scenario' => 'password']);
        $model['user_kasn'] = isset($model['user']->userKasn) ? $model['user']->userKasn : new UserKasn();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_kasn']->load($post);
            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['kasn'];

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['user_kasn']->id_user = $model['user']->id;
                if (!$model['user_kasn']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-create', [
                'model' => $model,
                'title' => 'Pendaftaran User Baru',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $error = true;

        $model['user'] = $this->findModel($id);
        $model['user_kasn'] = isset($model['user']->userKasn) ? $model['user']->userKasn : new UserKasn();

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_kasn']->load($post);
            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['kasn'];

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['user_kasn']->id_user = $model['user']->id;
                if (!$model['user_kasn']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-update', [
                'model' => $model,
                'title' => 'Update User',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionResetPassword($id)
    {
        $error = true;

        $model['user'] = $this->findModel($id);
        $model['user']->scenario = 'password';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-reset-password', [
                'model' => $model,
                'title' => 'Reset Password User',
            ]);
        else
            return $this->redirect(['index']);
    }
}