<?php
namespace app_merit\controllers\kasn;

use Yii;
use app_merit\models\InstansiPemerintah;
use app_merit\models\User;
use app_merit\models\UserExtend;
use technosmart\yii\web\Controller;

class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kasn']]
            ]),
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select('`instansi_pemerintah`.nama, count(`penilaian_detail`.nilai_tim_verifikasi * `indikator`.bobot) AS nilai_akhir ')
            ->from('`instansi_pemerintah`')
            ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_instansi_pemerintah` = `instansi_pemerintah`.`id` AND `penilaian_detail`.`id_periode` = 1')
            ->leftJoin('indikator', '`indikator`.`id` = `penilaian_detail`.`id_indikator`')
            ->groupBy('`instansi_pemerintah`.id')
            ->where([
                'instansi_pemerintah.jenis' => ['Kementerian', 'Lembaga'],
            ])
            ;

        // $model['instansi_pemerintah_pusat'] = $query->all(InstansiPemerintah::getDb());

        return $this->datatables($query, $post = Yii::$app->request->post(), User::getDb());
    }

    public function actionIndex()
    {
        /*$query = new \yii\db\Query();
        $query
            ->select('`instansi_pemerintah`.*, count(`penilaian_detail`.nilai_tim_verifikasi * `indikator`.bobot) AS nilai_akhir ')
            ->from('`instansi_pemerintah`')
            ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_instansi_pemerintah` = `instansi_pemerintah`.`id` AND `penilaian_detail`.`id_periode` = 1')
            ->leftJoin('indikator', '`indikator`.`id` = `penilaian_detail`.`id_indikator`')
            ->groupBy('`instansi_pemerintah`.id')
            ->where([
                'instansi_pemerintah.jenis' => ['Kementerian', 'Lembaga'],
            ])
            ;

        $model['instansi_pemerintah_pusat'] = $query->all(InstansiPemerintah::getDb());*/

        $query = new \yii\db\Query();
        $query
            ->select('`instansi_pemerintah`.*, `provinces`.`name`, count(`penilaian_detail`.nilai_tim_verifikasi * `indikator`.bobot) AS nilai_akhir ')
            ->from('`instansi_pemerintah`')
            ->leftJoin('provinces', '`provinces`.`id` = `instansi_pemerintah`.`id_provinces`')
            ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_instansi_pemerintah` = `instansi_pemerintah`.`id` AND `penilaian_detail`.`id_periode` = 1')
            ->leftJoin('indikator', '`indikator`.`id` = `penilaian_detail`.`id_indikator`')
            ->groupBy('`instansi_pemerintah`.id')
            ->where([
                'instansi_pemerintah.jenis' => ['Provinsi'],
            ])
            ;

        $model['instansi_pemerintah_daerah'] = $query->all(InstansiPemerintah::getDb());

        foreach ($model['instansi_pemerintah_daerah'] as $key => $instansiPemerintahDaerah) {
            if ($instansiPemerintahDaerah['nilai_akhir'] <= 174)  {
                $color = 'red';
                $kategori = 1;
            } elseif ($instansiPemerintahDaerah['nilai_akhir'] < 249)  {
                $color = 'yellow';
                $kategori = 2;
            } elseif ($instansiPemerintahDaerah['nilai_akhir'] < 324)  {
                $color = 'green';
                $kategori = 3;
            } else {
                $color = 'azure';
                $kategori = 4;
            }
            switch ($instansiPemerintahDaerah['name']):
                case strtoupper('Aceh'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.AC';
                    $model['code']['ID.AC'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.AC'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Kalimantan Timur'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.KI';
                    $model['code']['ID.KI'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.KI'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Jawa Barat'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.JR';
                    $model['code']['ID.JR'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.JR'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Jawa Tengah'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.JT';
                    $model['code']['ID.JT'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.JT'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Bengkulu'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.BE';
                    $model['code']['ID.BE'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.BE'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Banten'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.BT';
                    $model['code']['ID.BT'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.BT'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('DKI JAKARTA'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.JK';
                    $model['code']['ID.JK'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.JK'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Kalimantan Barat'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.KB';
                    $model['code']['ID.KB'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.KB'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Lampung'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.LA';
                    $model['code']['ID.LA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.LA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sumatera Selatan'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SL';
                    $model['code']['ID.SL'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SL'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('KEPULAUAN BANGKA BELITUNG'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.BB';
                    $model['code']['ID.BB'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.BB'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Bali'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.BA';
                    $model['code']['ID.BA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.BA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Jawa Timur'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.JI';
                    $model['code']['ID.JI'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.JI'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Kalimantan Selatan'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.KS';
                    $model['code']['ID.KS'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.KS'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Nusa Tenggara Timur'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.NT';
                    $model['code']['ID.NT'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.NT'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sulawesi Selatan'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SE';
                    $model['code']['ID.SE'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SE'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sulawesi Barat'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SR';
                    $model['code']['ID.SR'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SR'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Kepulauan Riau'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.KR';
                    $model['code']['ID.KR'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.KR'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Gorontalo'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.GO';
                    $model['code']['ID.GO'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.GO'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Jambi'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.JA';
                    $model['code']['ID.JA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.JA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Kalimantan Tengah'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.KT';
                    $model['code']['ID.KT'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.KT'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('PAPUA BARAT'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.IB';
                    $model['code']['ID.IB'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.IB'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sumatera Utara'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SU';
                    $model['code']['ID.SU'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SU'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Riau'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.RI';
                    $model['code']['ID.RI'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.RI'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sulawesi Utara'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SW';
                    $model['code']['ID.SW'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SW'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Maluku Utara'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.LA';
                    $model['code']['ID.LA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.LA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sumatera Barat'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SB';
                    $model['code']['ID.SB'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SB'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('DI YOGYAKARTA'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.YO';
                    $model['code']['ID.YO'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.YO'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Maluku'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.MA';
                    $model['code']['ID.MA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.MA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Nusa Tenggara Barat'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.NB';
                    $model['code']['ID.NB'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.NB'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sulawesi Tenggara'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.SG';
                    $model['code']['ID.SG'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.SG'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('Sulawesi Tengah'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.ST';
                    $model['code']['ID.ST'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.ST'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                case strtoupper('PAPUA'):
                    $model['instansi_pemerintah_daerah'][$key]['code'] = 'ID.PA';
                    $model['code']['ID.PA'] = [
                        'fillKey' => 'kategori' . $kategori,
                    ];
                    $model['label']['ID.PA'] = $instansiPemerintahDaerah['name'] . ': ' . $instansiPemerintahDaerah['nilai_akhir'];
                    break;
                default:
                    $model['instansi_pemerintah_daerah'][$key]['code'] = null;
                    break;
            endswitch;
        }

        // ddx($model['instansi_pemerintah_daerah']);

        return $this->render('index', [
            'model' => $model,
            // 'title' => 'Detail of ' . $model['user']->name,
        ]);
    }

    public function actionForm()
    {
        // $model['user'] = $this->findModel($id);
        return $this->render('form', [
            // 'model' => $model,
            // 'title' => 'Detail of ' . $model['user']->name,
        ]);
    }

    public function actionCreate()
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user_extend'] = isset($model['user']->user) ? $model['user']->user : new UserExtend();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_extend']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['user_extend']->id = $model['user']->id;
                if (!$model['user_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-create', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Create New User',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionUpdate($id)
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user_extend'] = isset($model['user']->user) ? $model['user']->user : new UserExtend();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_extend']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['user_extend']->id = $model['user']->id;
                if (!$model['user_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Update ' . $model['user']->name . ' (username: ' . $model['user']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionResetPassword($id)
    {
        $render = false;

        $model['user'] = isset($id) ? $this->findModel($id) : new User(['scenario' => 'password']);
        $model['user']->scenario = 'password';
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            $transaction = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }

                $transaction->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-reset-password', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Reset Password ' . $model['user']->name . ' (username: ' . $model['user']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['user']->id]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_DELETED;
        $model->password = 'dummy password';
        $model->password_repeat = 'dummy password';

        if ($model->save()) {
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
}