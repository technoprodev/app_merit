<?php
namespace app_merit\controllers\kasn;

use Yii;
use app_merit\models\InstansiPemerintah;
use app_merit\models\User;
use app_merit\models\UserInstansiPemerintah;
use technosmart\yii\web\Controller;

class InstansiPemerintahController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kasn']]
            ]),
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = InstansiPemerintah::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'ip.id',
                'ip.nama',
                'ip.jenis',
                'p.name AS province',
                'r.name AS regency',
                'uip.id_user',
                // 'MIN(uip.id_user) AS id_user',
                'ip.updated_at'
            ])
            ->from('instansi_pemerintah ip')
            ->join('LEFT JOIN', 'provinces p', 'p.id = ip.id_provinces')
            ->join('LEFT JOIN', 'regencies r', 'r.id = ip.id_regencies')
            ->join('LEFT JOIN', 'user_instansi_pemerintah uip', 'uip.id_instansi_pemerintah = ip.id')
            // ->groupBy('ip.id')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), InstansiPemerintah::getDb());
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Instansi Pemerintah',
                'model' => ['instansi_pemerintah' => new InstansiPemerintah()],
            ]);
        }

        $model['instansi_pemerintah'] = $this->findModel($id);
        return $this->render('detail', [
            'model' => $model,
            'title' => 'Detail ' . $model['instansi_pemerintah']->nama,
        ]);
    }

    public function actionCreate()
    {
        $error = true;

        $model['instansi_pemerintah'] = isset($id) ? $this->findModel($id) : new InstansiPemerintah();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['instansi_pemerintah']->load($post);

            $transaction['instansi_pemerintah'] = InstansiPemerintah::getDb()->beginTransaction();

            try {
                if ($model['instansi_pemerintah']->jenis == 'Kementerian' || $model['instansi_pemerintah']->jenis == 'Lembaga') {
                    $model['instansi_pemerintah']->id_provinces = null;
                }
                if ($model['instansi_pemerintah']->jenis == 'Kementerian' || $model['instansi_pemerintah']->jenis == 'Lembaga' || $model['instansi_pemerintah']->jenis == 'Provinsi') {
                    $model['instansi_pemerintah']->id_regencies = null;
                }
                if (!$model['instansi_pemerintah']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['instansi_pemerintah']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['instansi_pemerintah']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['instansi_pemerintah']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Pendaftaran Instansi Pemerintah',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $error = true;

        $model['instansi_pemerintah'] = isset($id) ? $this->findModel($id) : new InstansiPemerintah();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['instansi_pemerintah']->load($post);

            $transaction['instansi_pemerintah'] = InstansiPemerintah::getDb()->beginTransaction();

            try {
                $model['instansi_pemerintah']->updated_at = new \yii\db\Expression("now()");
                if ($model['instansi_pemerintah']->jenis == 'Kementerian' || $model['instansi_pemerintah']->jenis == 'Lembaga') {
                    $model['instansi_pemerintah']->id_provinces = null;
                }
                if ($model['instansi_pemerintah']->jenis == 'Kementerian' || $model['instansi_pemerintah']->jenis == 'Lembaga' || $model['instansi_pemerintah']->jenis == 'Provinsi') {
                    $model['instansi_pemerintah']->id_regencies = null;
                }
                if (!$model['instansi_pemerintah']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['instansi_pemerintah']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['instansi_pemerintah']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['instansi_pemerintah']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Data Instansi Pemerintah',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionUpdateUser($id)
    {
        $error = true;

        $model['instansi_pemerintah'] = $this->findModel($id);
        if ($model['instansi_pemerintah']->userInstansiPemerintahs) {
            $model['user'] = $model['instansi_pemerintah']->userInstansiPemerintahs[0]->user;
        } else {
            $model['user'] = new User(['scenario' => 'password']);
        }
        $model['user_instansi_pemerintah'] = isset($model['user']->userInstansiPemerintah) ? $model['user']->userInstansiPemerintah : new UserInstansiPemerintah();


        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_instansi_pemerintah']->load($post);
            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['instansi-pemerintah'];

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['user_instansi_pemerintah']->id_user = $model['user']->id;
                $model['user_instansi_pemerintah']->id_instansi_pemerintah = $model['instansi_pemerintah']->id;
                $model['instansi_pemerintah']->updated_at = new \yii\db\Expression("now()");
                if (!$model['user_instansi_pemerintah']->save() || !$model['instansi_pemerintah']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error) {
            if ($model['user']->isNewRecord) {
                return $this->render('form-user-create', [
                    'model' => $model,
                    'title' => 'Pendaftaran User Instansi Pemerintah Baru',
                ]);
            } else {
                return $this->render('form-user-update', [
                    'model' => $model,
                    'title' => 'Update User Instansi Pemerintah',
                ]);
            }
        }
        else
            return $this->redirect(['index']);
    }

    public function actionResetPassword($id)
    {
        $error = true;

        $model['instansi_pemerintah'] = $this->findModel($id);
        if ($model['instansi_pemerintah']->userInstansiPemerintahs) {
            $model['user'] = $model['instansi_pemerintah']->userInstansiPemerintahs[0]->user;
        } else {
            $model['user'] = new User(['scenario' => 'password']);
        }
        $model['user']->scenario = 'password';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-user-reset-password', [
                'model' => $model,
                'title' => 'Reset Password User Instansi Pemerintah',
            ]);
        else
            return $this->redirect(['index']);
    }
}