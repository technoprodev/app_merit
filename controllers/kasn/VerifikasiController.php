<?php
namespace app_merit\controllers\kasn;

use Yii;
use app_merit\models\InstansiPemerintah;
use app_merit\models\Aspek;
use app_merit\models\PenilaianAspek;
use app_merit\models\Indikator;
use app_merit\models\PenilaianDetail;
use app_merit\models\Periode;
use app_merit\models\Penilaian;
use technosmart\yii\web\Controller;

class VerifikasiController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kasn']]
            ]),
        ];
    }
    
    protected function findModelPeriode()
    {
        if (($model = Periode::find()->where(['id' => \app_merit\models\Periode::getPeriodeAktif()])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Periode penilaian sedang tidak aktif. Harap hubungi KASN untuk mengetahui kapan periode penilaian akan aktif.');
        }
    }

    protected function findModelInstansiPemerintah($id)
    {
        if (($model = InstansiPemerintah::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPenilaian($idInstansiPemerintah, $idPeriode)
    {
        if (($model = Penilaian::find()->where(['id_instansi_pemerintah' => $idInstansiPemerintah, 'id_periode' => $idPeriode])->one()) !== null) {
            return $model;
        } else {
            return new Penilaian();
        }
    }

    protected function findModelAspek($id)
    {
        if (($model = Aspek::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPenilaianAspek($idAspek, $idInstansiPemerintah)
    {
        if (($model = PenilaianAspek::find()->where(['id_aspek' => $idAspek, 'id_instansi_pemerintah' => $idInstansiPemerintah])->one()) !== null) {
            return $model;
        } else {
            return new PenilaianAspek();
        }
    }

    protected function findModelIndikator($id)
    {
        if (($model = Indikator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPenilaianDetail($idIndikator, $idInstansiPemerintah)
    {
        if (($model = PenilaianDetail::find()->where(['id_indikator' => $idIndikator, 'id_periode' => \app_merit\models\Periode::getPeriodeAktif(), 'id_instansi_pemerintah' => $idInstansiPemerintah])->one()) !== null) {
            return $model;
        } else {
            return new PenilaianDetail();
        }
    }

    public function actionDatatables()
    {
        $belumInput = new \yii\db\Expression('IFNULL(`p`.`status`, "Belum Input") AS statuss');
        $query = new \yii\db\Query();
        $query
            ->select([
                'ip.id',
                'ip.nama',
                'ip.jenis',
                'p.id as id_penilaian',
                'p.berita_acara_penilaian',
                'p.berita_acara_verifikasi',
                'p.rekomendasi',
            ])
            ->addSelect($belumInput)
            ->from('instansi_pemerintah ip')
            ->leftJoin('penilaian p', '`p`.`id_instansi_pemerintah` = `ip`.`id` AND `p`.`id_periode` = 1')
            ->orderBy(['p.status' => SORT_ASC])
            // ->groupBy('ip.id')
        ;

        // ddx($query->createCommand()->getRawSql());
        
        return $this->datatables($query, Yii::$app->request->post(), InstansiPemerintah::getDb());
    }

    public function actionIndex()
    {
        return $this->render('list', [
            // 'model' => $model,
            'title' => 'Verfikiasi',
        ]);
    }

    public function actionListVerifikasi($idInstansiPemerintah)
    {
        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);

        $model['aspek'] = Aspek::find()
            ->select('aspek.*, penilaian_aspek.id AS idPenilaianAspek, penilaian_aspek.catatan AS catatanAspek')
            ->join('LEFT JOIN', 'penilaian_aspek', '`penilaian_aspek`.`id_aspek` = `aspek`.`id` AND `penilaian_aspek`.`id_instansi_pemerintah` = ' . $idInstansiPemerintah)
            ->where(['aspek.id_periode' => \app_merit\models\Periode::getPeriodeAktif()])
            ->orderBy(['urutan' => SORT_ASC])
            ->with([
                'indikators' => function ($query) use ($idInstansiPemerintah) {
                    $query
                    ->select('indikator.*, penilaian_detail.id AS idPenilaianDetail, penilaian_detail.kondisi_saat_ini, penilaian_detail.nilai_tim_instansi, penilaian_detail.nilai_tim_verifikasi, penilaian_detail.catatan')
                    ->from(['indikator' => Indikator::tableName()])
                    ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_indikator` = `indikator`.`id` AND `penilaian_detail`.`id_instansi_pemerintah` = ' . $idInstansiPemerintah)
                    ->orderBy(['id_aspek' => SORT_ASC, 'urutan' => SORT_ASC]);
                },
            ])
            ->asArray()
            ->all();

        return $this->render('list-verifikasi', [
            'model' => $model,
            'title' => 'Verifikasi',
            'idInstansiPemerintah' => $idInstansiPemerintah,
        ]);
    }

    public function actionKondisiSaatIni($id)
    {
        $model['penilaian_detail'] = PenilaianDetail::find()
            ->where(['penilaian_detail.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-kondisi-saat-ini', [
            'value' => $model['penilaian_detail']['kondisi_saat_ini'],
            'title' => 'Kondisi Saat Ini',
        ]);
    }

    public function actionNilaiTimInstansi($id, $field)
    {
        $model['indikator'] = Indikator::find()
            ->join('INNER JOIN', 'aspek k', 'k.id = indikator.id_aspek')
            ->where(['k.id_periode' => 1])
            ->AndWhere(['indikator.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-nilai-tim-instansi', [
            'value' => $model['indikator'][$field],
            'title' => 'Nilai Tim Instansi',
        ]);
    }

    public function actionNilaiTimVerifikasi($id, $field)
    {
        $model['indikator'] = Indikator::find()
            ->join('INNER JOIN', 'aspek k', 'k.id = indikator.id_aspek')
            ->where(['k.id_periode' => 1])
            ->AndWhere(['indikator.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-nilai-tim-verifikasi', [
            'value' => $model['indikator'][$field],
            'title' => 'Nilai Tim Veifikasi',
        ]);
    }

    public function actionCatatan($id)
    {
        $model['penilaian_detail'] = PenilaianDetail::find()
            ->where(['penilaian_detail.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-kondisi-saat-ini', [
            'value' => $model['penilaian_detail']['catatan'],
            'title' => 'Kondisi Saat Ini',
        ]);
    }

    public function actionCatatanAspek($id)
    {
        $model['penilaian_aspek'] = PenilaianAspek::find()
            ->where(['penilaian_aspek.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-kondisi-saat-ini', [
            'value' => $model['penilaian_aspek']['catatan'],
            'title' => 'Kondisi Saat Ini',
        ]);
    }

    public function actionFormVerifikasi($idIndikator, $idInstansiPemerintah)
    {
        $error = true;

        $model['indikator'] = $this->findModelIndikator($idIndikator);
        $model['penilaian_detail'] = $this->findModelPenilaianDetail($idIndikator, $idInstansiPemerintah);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian_detail']->load($post);

            $transaction['penilaian_detail'] = PenilaianDetail::getDb()->beginTransaction();

            try {
                if ($model['penilaian_detail']->isNewRecord) {
                    $model['penilaian_detail']->id_periode = \app_merit\models\Periode::getPeriodeAktif();
                    $model['penilaian_detail']->id_instansi_pemerintah = $idInstansiPemerintah;
                    $model['penilaian_detail']->id_user_instansi_pemerintah = Yii::$app->user->identity->id;
                    $model['penilaian_detail']->id_indikator = $idIndikator;
                }
                if (!$model['penilaian_detail']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['penilaian_detail']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian_detail']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian_detail']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-verifikasi', [
                'model' => $model,
                'title' => 'Form Verifikasi',
            ]);
        else
            return $this->redirect(['list-verifikasi', 'idInstansiPemerintah' => $idInstansiPemerintah]);
    }

    public function actionFormCatatanAspek($idAspek, $idInstansiPemerintah)
    {
        $error = true;

        $model['aspek'] = $this->findModelAspek($idAspek);
        $model['penilaian_aspek'] = $this->findModelPenilaianAspek($idAspek, $idInstansiPemerintah);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian_aspek']->load($post);

            $transaction['penilaian_aspek'] = PenilaianAspek::getDb()->beginTransaction();

            try {
                if ($model['penilaian_aspek']->isNewRecord) {
                    $model['penilaian_aspek']->id_periode = \app_merit\models\Periode::getPeriodeAktif();
                    $model['penilaian_aspek']->id_aspek = $idAspek;
                    $model['penilaian_aspek']->id_instansi_pemerintah = $idInstansiPemerintah;
                }
                if (!$model['penilaian_aspek']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['penilaian_aspek']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian_aspek']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian_aspek']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-catatan-aspek', [
                'model' => $model,
                'title' => 'Form Catatan Aspek',
            ]);
        else
            return $this->redirect(['list-verifikasi', 'idInstansiPemerintah' => $idInstansiPemerintah]);
    }

    public function actionFormUploadBaVerifikasi($idInstansiPemerintah)
    {
        $error = true;

        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        $model['periode'] = $this->findModelPeriode();
        $model['penilaian'] = $this->findModelPenilaian($idInstansiPemerintah, $model['periode']->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian']->load($post);

            $transaction['penilaian'] = PenilaianDetail::getDb()->beginTransaction();

            try {
                $model['penilaian']->id_periode = $model['periode']->id;
                $model['penilaian']->id_instansi_pemerintah = $idInstansiPemerintah;
                $model['penilaian']->status = Penilaian::getRealStatus($model['instansi_pemerintah']->id);
                if (!$model['penilaian']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['penilaian']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-upload-ba-verifikasi', [
                'model' => $model,
                'title' => 'Upload BA Verifikasi',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionFormUploadRekomendasi($idInstansiPemerintah)
    {
        $error = true;

        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        $model['periode'] = $this->findModelPeriode();
        $model['penilaian'] = $this->findModelPenilaian($idInstansiPemerintah, $model['periode']->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian']->load($post);

            $transaction['penilaian'] = PenilaianDetail::getDb()->beginTransaction();

            try {
                $model['penilaian']->id_periode = $model['periode']->id;
                $model['penilaian']->id_instansi_pemerintah = $idInstansiPemerintah;
                $model['penilaian']->status = Penilaian::getRealStatus($model['instansi_pemerintah']->id);
                if (!$model['penilaian']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['penilaian']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-upload-rekomendasi', [
                'model' => $model,
                'title' => 'Upload Rekomendasi',
            ]);
        else
            return $this->redirect(['index']);
    }
}