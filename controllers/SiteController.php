<?php
namespace app_merit\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_merit\models\User;
use app_merit\models\UserExtend;
use app_merit\models\Login;

class SiteController extends SiteControl
{
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        if (isset(Yii::$app->user->identity->userInstansiPemerintah)) {
            return $this->redirect('instansi_pemerintah/dashboard');
        }
        if (isset(Yii::$app->user->identity->userKasn)) {
            return $this->redirect('kasn/dashboard');
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $border = ['#663333', '#664d33', '#666633', '#4d6633', '#336633', '#33664d', '#336666', '#334d66', '#333366', '#4d3366', '#663366', '#66334d'];

        $background = ['#e6b8b8', '#e6cfb8', '#e6e6b8', '#cfe6b8', '#b8e6b8', '#b8e6cf', '#b8e6e6', '#b8cfe6', '#b8b8e6', '#cfb8e6', '#e6b8e6', '#e6b8cf'];

        $query = new \yii\db\Query();
        $query
            ->select('`instansi_pemerintah`.*, count(`penilaian_detail`.nilai_tim_verifikasi * `indikator`.bobot) AS nilai_akhir ')
            ->from('`instansi_pemerintah`')
            ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_instansi_pemerintah` = `instansi_pemerintah`.`id` AND `penilaian_detail`.`id_periode` = 1')
            ->leftJoin('indikator', '`indikator`.`id` = `penilaian_detail`.`id_indikator`')
            ->groupBy('`instansi_pemerintah`.id')
            ->where([
                'instansi_pemerintah.jenis' => ['Pusat', 'Lembaga', 'Provinsi', 'Kota', 'Kabupaten'],
            ])
            ->orderBy([
                'nilai_akhir' => SORT_DESC,
            ])
            ->limit(10)
            ;

        $model['pie'] = $query->all();
        $model['label'] = $model['data'] = [];
        foreach ($model['pie'] as $key => $value) {
            $model['label'][] = $value['nama'];
            $model['data'][] = $value['nilai_akhir'];
            $model['background'][] = $background[$key % 12];
            $model['border'][] = $border[$key % 12];
        }
        // ddx($model['pie']);

        $model['login'] = new Login(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'auth';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }
}