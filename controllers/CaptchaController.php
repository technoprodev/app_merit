<?php
namespace app_merit\controllers;

use Yii;
use technosmart\yii\web\Controller;

class CaptchaController extends Controller
{
    public function actions()
    {
        $actions = parent::actions();
        $actions['captcha'] = [
            'class' => 'yii\captcha\CaptchaAction',
        ];
        return $actions;
    }
}