<?php
namespace app_merit\controllers\instansi_pemerintah;

use Yii;
use app_merit\models\User;
use app_merit\models\InstansiPemerintah;
use technosmart\yii\web\Controller;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['instansi-pemerintah']]
            ]),
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = User::find()
            ->leftJoin('user_instansi_pemerintah uip', '`uip`.`id_user` = `user`.`id` AND `uip`.`id_instansi_pemerintah` = :id_instansi_pemerintah')
            ->addParams([':id_instansi_pemerintah' => Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah])
            ->where(['id' => $id])
            ->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelAll()
    {
        if (($model = User::find()
            ->leftJoin('user_instansi_pemerintah uip', '`uip`.`id_user` = `user`.`id` AND `uip`.`id_instansi_pemerintah` = :id_instansi_pemerintah')
            ->addParams([':id_instansi_pemerintah' => Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah])
            ->all()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModel2()
    {
        if (($model = User::find()
            ->leftJoin('user_instansi_pemerintah uip', '`uip`.`id_user` = `user`.`id`')
            ->where(['id' => Yii::$app->user->identity->id])
            ->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelInstansiPemerintah($id)
    {
        if (($model = InstansiPemerintah::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'u.id',
                'u.name',
                'u.email',
                'u.username',
                'u.phone',
                'uip.jabatan_di_instansi',
                'uip.jabatan_di_sk',
            ])
            ->from('user u')
            ->innerJoin('user_instansi_pemerintah uip', '`uip`.`id_user` = `u`.`id` AND `uip`.`id_instansi_pemerintah` = :id_instansi_pemerintah')
            ->addParams([':id_instansi_pemerintah' => Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), User::getDb());
    }

    public function actionIndex()
    {
        $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;

        $model['user'] = $this->findModel2();
        $model['user_instansi_pemerintah'] = $model['user']->userInstansiPemerintah;
        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);

        return $this->render('detail2', [
            'model' => $model,
            'title' => '' . $model['instansi_pemerintah']->nama,
        ]);
    }

    public function actionUpdate()
    {
        $error = true;

        $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;

        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        $model['user'] = $this->findModel2();
        $model['user_instansi_pemerintah'] = isset($model['user']->userInstansiPemerintah) ? $model['user']->userInstansiPemerintah : new UserInstansiPemerintah();


        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);
            $model['user_instansi_pemerintah']->load($post);
            // $model['assignments'] = Yii::$app->request->post('assignments', []);
            $model['assignments'] = ['instansi-pemerintah'];

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if ($model['user']->isNewRecord) {
                    $model['user']->status = 1;
                    $model['user']->setPassword($model['user']->password);
                    $model['user']->generateAuthKey();
                }
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['user_instansi_pemerintah']->id_user = $model['user']->id;
                $model['user_instansi_pemerintah']->id_instansi_pemerintah = $model['instansi_pemerintah']->id;
                if (!$model['user_instansi_pemerintah']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                Yii::$app->authManager->revokeAll($model['user']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['user']->id);
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error) {
            return $this->render('form-user-update', [
                'model' => $model,
                'title' => 'Update Profil User ' . $model['instansi_pemerintah']->nama,
            ]);
        }
        else
            return $this->redirect(['index']);
    }

    public function actionChangePassword()
    {
        $error = true;

        $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;

        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        $model['user'] = $this->findModel2();
        $model['user']->scenario = 'password-change';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if (!$model['user']->validate()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $model['user']->scenario = 'password';
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-user-reset-password', [
                'model' => $model,
                'title' => 'Ganti Password User ' . $model['instansi_pemerintah']->nama,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function aactionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar User ' . Yii::$app->user->identity->userInstansiPemerintah->instansiPemerintah->nama,
            ]);
        }

        $model['user'] = $this->findModel($id);
        return $this->render('detail', [
            'model' => $model,
            'title' => 'Detail ' . $model['user']->name,
        ]);
    }

    public function aactionCreate()
    {
        $error = true;

        $model['user'] = isset($id) ? $this->findModel($id) : new User();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Pendaftaran User Baru',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function aactionUpdate($id)
    {
        $error = true;

        $model['user'] = isset($id) ? $this->findModel($id) : new User();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update User',
            ]);
        else
            return $this->redirect(['index']);
    }
}