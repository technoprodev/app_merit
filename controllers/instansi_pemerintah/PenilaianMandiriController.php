<?php
namespace app_merit\controllers\instansi_pemerintah;

use Yii;
use app_merit\models\Aspek;
use app_merit\models\Indikator;
use app_merit\models\PenilaianDetail;
use app_merit\models\InstansiPemerintah;
use app_merit\models\User;
use technosmart\yii\web\Controller;

class PenilaianMandiriController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['instansi-pemerintah']]
            ]),
        ];
    }
    
    protected function findModelInstansiPemerintah($id)
    {
        if (($model = InstansiPemerintah::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;
        
        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        
        $model['aspek'] = Aspek::find()
            ->select('aspek.*, penilaian_aspek.id AS idPenilaianAspek, penilaian_aspek.catatan AS catatanAspek')
            ->join('LEFT JOIN', 'penilaian_aspek', '`penilaian_aspek`.`id_aspek` = `aspek`.`id` AND `penilaian_aspek`.`id_instansi_pemerintah` = ' . $idInstansiPemerintah)
            ->where(['aspek.id_periode' => \app_merit\models\Periode::getPeriodeAktif()])
            ->orderBy(['urutan' => SORT_ASC])
            ->with([
                'indikators' => function ($query) use ($idInstansiPemerintah) {
                    $query->select('indikator.*, penilaian_detail.id AS idPenilaianDetail, penilaian_detail.kondisi_saat_ini, penilaian_detail.nilai_tim_instansi, penilaian_detail.nilai_tim_verifikasi, penilaian_detail.catatan')
                        ->from(['indikator' => Indikator::tableName()])
                        ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_indikator` = `indikator`.`id` AND `penilaian_detail`.`id_instansi_pemerintah` = ' . $idInstansiPemerintah)
                        ->orderBy(['id_aspek' => SORT_ASC, 'urutan' => SORT_ASC]);
                },
            ])
            ->asArray()
            ->all();

        $model['user'] = User::find()
            ->select('user.*, uk.jabatan_di_kasn')
            ->innerJoin('user_kasn uk', 'uk.id_user = user.id')
            ->asArray()
            ->all();

        return $this->render('list-verifikasi', [
            'model' => $model,
            'title' => 'Verifikasi',
        ]);
    }

    public function actionKondisiSaatIni($id)
    {
        $model['penilaian_detail'] = PenilaianDetail::find()
            ->where(['penilaian_detail.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-kondisi-saat-ini', [
            'value' => $model['penilaian_detail']['kondisi_saat_ini'],
            'title' => 'Kondisi Saat Ini',
        ]);
    }

    public function actionNilaiTimInstansi($id, $field)
    {
        $model['indikator'] = Indikator::find()
            ->join('INNER JOIN', 'aspek k', 'k.id = indikator.id_aspek')
            ->where(['k.id_periode' => 1])
            ->AndWhere(['indikator.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-nilai-tim-instansi', [
            'value' => $model['indikator'][$field],
            'title' => 'Nilai Tim Instansi',
        ]);
    }

    public function actionNilaiTimVerifikasi($id, $field)
    {
        $model['indikator'] = Indikator::find()
            ->join('INNER JOIN', 'aspek k', 'k.id = indikator.id_aspek')
            ->where(['k.id_periode' => 1])
            ->AndWhere(['indikator.id' => $id])
            ->asArray()
            ->one();

        return $this->render('detail-nilai-tim-verifikasi', [
            'value' => $model['indikator'][$field],
            'title' => 'Nilai Tim Veifikasi',
        ]);
    }
}