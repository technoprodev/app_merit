<?php
namespace app_merit\controllers\instansi_pemerintah;

use Yii;
use app_merit\models\User;
use app_merit\models\InstansiPemerintah;
use app_merit\models\Penilaian;
use app_merit\models\Aspek;
use app_merit\models\PenilaianAspek;
use app_merit\models\Indikator;
use app_merit\models\PenilaianDetail;
use technosmart\yii\web\Controller;

class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['instansi-pemerintah']]
            ]),
        ];
    }
    
    protected function findModelInstansiPemerintah($id)
    {
        //hapus
        if (($model = InstansiPemerintah::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPenilaian()
    {
        if (($model = Penilaian::find()->where(['id_periode' => \app_merit\models\Periode::getPeriodeAktif(), 'id_instansi_pemerintah' => Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah])->one()) !== null) {
            return $model;
        } else {
            return new Penilaian();
        }
    }

    protected function findModelIndikator($id)
    {
        if (($model = Indikator::find()->where(['id' => $id])->with(['aspek'])->asArray()->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelPenilaianDetail($id)
    {
        if (($model = PenilaianDetail::find()->where(['id_indikator' => $id, 'id_periode' => \app_merit\models\Periode::getPeriodeAktif(), 'id_instansi_pemerintah' => Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah])->one()) !== null) {
            return $model;
        } else {
            return new PenilaianDetail();
        }
    }

    public function actionIndex($urutan = 1)
    {
        $idInstansiPemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;

        $model['instansi_pemerintah'] = $this->findModelInstansiPemerintah($idInstansiPemerintah);
        $model['penilaian'] = $this->findModelPenilaian();
        $model['user'] = Yii::$app->user->identity;
        $model['aspek'] = Aspek::find()
            ->where(['id_periode' => \app_merit\models\Periode::getPeriodeAktif()])
            ->orderBy(['urutan' => SORT_ASC])
            ->with([
                'indikators' => function ($query) use ($idInstansiPemerintah) {
                    $query
                    ->select('indikator.*, penilaian_detail.kondisi_saat_ini, penilaian_detail.nilai_tim_instansi, penilaian_detail.nilai_tim_verifikasi, penilaian_detail.catatan')
                    ->from(['indikator' => Indikator::tableName()])
                    ->leftJoin('penilaian_detail', '`penilaian_detail`.`id_indikator` = `indikator`.`id` AND `penilaian_detail`.`id_instansi_pemerintah` = ' . $idInstansiPemerintah)
                    ->orderBy(['id_aspek' => SORT_ASC, 'urutan' => SORT_ASC]);
                },
            ])
            ->asArray()
            ->all();

        $error = true;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian']->load($post);

            $transaction['penilaian'] = Penilaian::getDb()->beginTransaction();

            try {
                $model['penilaian']->id_periode = \app_merit\models\Periode::getPeriodeAktif();
                $model['penilaian']->id_instansi_pemerintah = $idInstansiPemerintah;
                $model['penilaian']->status = Penilaian::getRealStatus();
                if (!$model['penilaian']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $error = false;

                $transaction['penilaian']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('list', [
                'model' => $model,
                'urutan' => $urutan,
                'title' => 'Dashboard Instansi Pemerintah',
            ]);
        else
            return $this->redirect(['index']);

    }

    public function actionPenilaian($id)
    {
        $error = true;

        $model['indikator'] = $this->findModelIndikator($id);
        $model['penilaian'] = $this->findModelPenilaian();
        $model['penilaian_detail'] = $this->findModelPenilaianDetail($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['penilaian_detail']->load($post);

            $transaction['penilaian_detail'] = PenilaianDetail::getDb()->beginTransaction();

            try {
                $model['penilaian_detail']->id_periode = \app_merit\models\Periode::getPeriodeAktif();
                $model['penilaian_detail']->id_instansi_pemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;
                $model['penilaian_detail']->id_user_instansi_pemerintah = Yii::$app->user->identity->id;
                $model['penilaian_detail']->id_indikator = $id;
                if (!$model['penilaian_detail']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['penilaian']->status = Penilaian::getRealStatus();
                $model['penilaian']->id_instansi_pemerintah = Yii::$app->user->identity->userInstansiPemerintah->id_instansi_pemerintah;
                $model['penilaian']->id_periode = \app_merit\models\Periode::getPeriodeAktif();
                
                if (!$model['penilaian']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $error = false;

                $transaction['penilaian_detail']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['penilaian_detail']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['penilaian_detail']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
            $error = true;
        }

        if ($error)
            return $this->render('form-penilaian', [
                'model' => $model,
                'title' => 'Form Penilaian',
            ]);
        else
            return $this->redirect(['index', 'urutan' => $model['indikator']['aspek']['urutan']]);
    }
}