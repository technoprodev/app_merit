$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kasn/verifikasi/datatables'),
                    type: 'POST'
                },
                order: [[5, 'desc']],
                columns: [
                    /*{
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            resetPassword = '';
                            if (row.id_user) {
                                resetPassword = '<a href="' + fn.urlTo('kasn/verifikasi/reset-password', {id: data}) + '" class="text-orange"><i class="fa fa-lock"></i></a>&nbsp;&nbsp;';
                            }
                            return '' +
                                '<a href="' + fn.urlTo('kasn/verifikasi', {id: data}) + '" class="text-azure" modal-md="" modal-title="Detail ' + row.nama + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/verifikasi/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/verifikasi/update-user', {id: data}) + '" class="text-cyan"><i class="fa fa-user"></i></a>&nbsp;&nbsp;' +
                                resetPassword +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    {data: 'nama'},
                    {data: 'jenis'},
                    {
                        data: {
                            _: 'p.status',
                            filter: 'p.status',
                            sort: 'p.status',
                            display: 'statuss',
                        },
                        render: function ( data, type, row ) {
                            if (data == 'Belum Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-belum.png" width="250" height="auto">';
                            } else if (data == 'Sedang Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-sedang.png" width="250" height="auto">';
                            } else if (data == 'Selesai Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-selesai.png" width="250" height="auto">';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.berita_acara_penilaian',
                            filter: 'p.berita_acara_penilaian',
                            sort: 'p.berita_acara_penilaian',
                            display: 'berita_acara_penilaian',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('') + 'upload/penilaian-berita_acara_penilaian/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                            } else {
                                return '<span class="text-red">Belum tersedia.</span>';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ip.id',
                            filter: 'ip.id',
                            sort: 'ip.id',
                            display: 'id',
                        },
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('kasn/verifikasi/list-verifikasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Verifikasi</a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.berita_acara_verifikasi',
                            filter: 'p.berita_acara_verifikasi',
                            sort: 'p.berita_acara_verifikasi',
                            display: 'berita_acara_verifikasi',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('kasn/verifikasi/form-upload-ba-verifikasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" modal-md="" modal-title="Upload Berita Acara Verifikasi">Upload Ulang</a>';
                            } else {
                                return '<a href="' + fn.urlTo('kasn/verifikasi/form-upload-ba-verifikasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-red border-red hover-bg-lightest hover-text-red" modal-md="" modal-title="Upload Berita Acara Verifikasi">Upload</a>';
                            }
                            return '<a href="' + fn.urlTo('') + 'upload/penilaian-berita_acara_verifikasi/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.rekomendasi',
                            filter: 'p.rekomendasi',
                            sort: 'p.rekomendasi',
                            display: 'rekomendasi',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('kasn/verifikasi/form-upload-rekomendasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" modal-md="" modal-title="Upload Berita Acara Verifikasi">Upload Ulang</a>';
                            } else {
                                return '<a href="' + fn.urlTo('kasn/verifikasi/form-upload-rekomendasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-red border-red hover-bg-lightest hover-text-red" modal-md="" modal-title="Upload Berita Acara Verifikasi">Upload</a>';
                            }
                            return '<a href="' + fn.urlTo('') + 'upload/penilaian-rekomendasi/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 1 || index == 2) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});