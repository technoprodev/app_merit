<!--
            <tbody>
                <?php foreach ($model['instansi_pemerintah'] as $key => $instansiPemerintah) : ?>
                    <tr>
                        <td><?= $key+1 ?></td>
                        <td><?= $instansiPemerintah->nama ?></td>
                        <td>
                            <?php if ($instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Selesai Input'): ?>
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/status-selesai.png" width="250" height="auto">
                            <?php elseif ($instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Sedang Input'): ?>
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/status-sedang.png" width="250" height="auto">
                            <?php else: ?>
                                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/status-belum.png" width="250" height="auto">
                            <?php endif ?>
                        </td>
                        <td>
                            <?php if ($instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->berita_acara_penilaian) : ?>
                                <a href="<?= $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->berita_acara_penilaian ? $instansiPemerintah->penilaianAktif->virtual_berita_acara_penilaian_download : '' ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>
                            <?php else : ?>
                                <span class="text-red">Belum tersedia.</span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Verifikasi</a>
                        </td>
                        <td>
                            <?php $tombol = $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->berita_acara_verifikasi ? 'Upload Ulang' : 'Upload'; ?>
                            <?php $color = $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->berita_acara_verifikasi ? 'azure' : 'red'; ?>
                            <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/form-upload-ba-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-<?= $color ?> border-<?= $color ?> hover-bg-lightest hover-text-<?= $color ?>" modal-md="" modal-title="Upload Berita Acara Verifikasi">
                                <?= $tombol ?>
                            </a>
                        </td>
                        <td>
                            <?php $tombol = $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->rekomendasi ? 'Upload Ulang' : 'Upload'; ?>
                            <?php $color = $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->rekomendasi ? 'azure' : 'red'; ?>
                            <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/form-upload-rekomendasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-<?= $color ?> border-<?= $color ?> hover-bg-lightest hover-text-<?= $color ?>" modal-md="" modal-title="Upload Rekomendasi">
                                <?= $tombol ?>
                            </a>

                            <?php if (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Sedang Input') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                            <?php elseif (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Selesai Input') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                            <?php elseif (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Selesai Upload BA Penilaian') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Download BA Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Verifikasi</a>
                            <?php elseif (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Sudah Diverifikasi') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Download BA Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Edit Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Print BA Verifikasi</a>
                            <?php elseif (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Sudah Print BA Verifikasi') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Download BA Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Edit Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Print BA Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Upload BA Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Upload Rekomendasi</a>
                            <?php elseif (false && $instansiPemerintah->penilaianAktif && $instansiPemerintah->penilaianAktif->status == 'Sudah Upload BA Verifikasi') : ?>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Lihat Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Download BA Penilaian</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Edit Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Print BA Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Upload BA Verifikasi</a>
                                <a href="<?= Yii::$app->urlManager->createUrl(['kasn/verifikasi/list-verifikasi', 'idInstansiPemerintah' => $instansiPemerintah->id]) ?>" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Upload Rekomendasi</a>
                            <?php endif; ?>
                        </td>
                    </tr>    
                <?php endforeach; ?>
            </tbody>-->