$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kasn/instansi-pemerintah/datatables'),
                    type: 'POST'
                },
                order: [[5, 'desc']],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            resetPassword = '';
                            if (row.id_user) {
                                resetPassword = '<a href="' + fn.urlTo('kasn/instansi-pemerintah/reset-password', {id: data}) + '" class="text-orange"><i class="fa fa-lock"></i></a>&nbsp;&nbsp;';
                            }
                            return '' +
                                '<a href="' + fn.urlTo('kasn/instansi-pemerintah', {id: data}) + '" class="text-azure" modal-md="" modal-title="Detail ' + row.nama + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/instansi-pemerintah/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/instansi-pemerintah/update-user', {id: data}) + '" class="text-cyan"><i class="fa fa-user"></i></a>&nbsp;&nbsp;' +
                                resetPassword +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'nama'},
                    {data: 'jenis'},
                    {
                        data: {
                            _: 'p.name',
                            filter: 'p.name',
                            sort: 'p.name',
                            display: 'province',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'r.name',
                            filter: 'r.name',
                            sort: 'r.name',
                            display: 'regency',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'updated_at'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 2 || index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});