$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kasn/profile/datatables'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<a href="' + fn.urlTo('kasn/profile', {id: data}) + '" class="text-azure" modal-md="" modal-title="Detail ' + row.name + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/profile/update', {id: data}) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('kasn/profile/reset-password', {id: data}) + '" class="text-orange"><i class="fa fa-lock"></i></a>&nbsp;&nbsp;' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'username'},
                    {data: 'phone'},
                    {data: 'jabatan_di_kasn'},
                    {data: 'jabatan_di_sk'},
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});