$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kasn/dashboard/datatables'),
                    type: 'POST'
                },
                order: [[0, 'desc']],
                columns: [
                    {
                        data: {
                            _: 'instansi_pemerintah.nama',
                            filter: 'instansi_pemerintah.nama',
                            sort: 'instansi_pemerintah.nama',
                            display: 'nama',
                        },
                        render: function ( data, type, row ) {
                            if (row.nilai_akhir <= 174)  {
                                color = 'red';
                                kategori = 1;
                            } else if (row.nilai_akhir < 249)  {
                                color = 'yellow';
                                kategori = 2;
                            } else if (row.nilai_akhir < 324)  {
                                color = 'green';
                                kategori = 3;
                            } else {
                                color = 'azure';
                                kategori = 4;
                            }
                            return '' +
                                '<div class="text-' + color + '">' + data + '</div>' +
                            '' ;
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'instansi_pemerintah.nama',
                            filter: 'instansi_pemerintah.nama',
                            sort: 'instansi_pemerintah.nama',
                            display: 'nilai_akhir',
                        },
                        render: function ( data, type, row ) {
                            if (data <= 174)  {
                                color = 'red';
                                kategori = 1;
                            } else if (data < 249)  {
                                color = 'yellow';
                                kategori = 2;
                            } else if (data < 324)  {
                                color = 'green';
                                kategori = 3;
                            } else {
                                color = 'azure';
                                kategori = 4;
                            }
                            return '' +
                                '<div class="text-' + color + '">' + data + '</div>' +
                            '' ;
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'instansi_pemerintah.nama',
                            filter: 'instansi_pemerintah.nama',
                            sort: 'instansi_pemerintah.nama',
                            display: 'nilai_akhir',
                        },
                        render: function ( data, type, row ) {
                            if (data <= 174)  {
                                color = 'red';
                                kategori = 1;
                            } else if (data < 249)  {
                                color = 'yellow';
                                kategori = 2;
                            } else if (data < 324)  {
                                color = 'green';
                                kategori = 3;
                            } else {
                                color = 'azure';
                                kategori = 4;
                            }
                            return '' +
                                '<div class="circle-icon bg-' + color + ' border-' + color + ' margin-right-5" style="width: 18px; height: 18px;"></div>' +
                                '<span class="text-' + color + '">Kategori ' + kategori + '</span>' +
                            '' ;
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{data: 'jenis'},
                    {
                        data: {
                            _: 'p.status',
                            filter: 'p.status',
                            sort: 'p.status',
                            display: 'statuss',
                        },
                        render: function ( data, type, row ) {
                            if (data == 'Belum Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-belum.png" width="250" height="auto">';
                            } else if (data == 'Sedang Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-sedang.png" width="250" height="auto">';
                            } else if (data == 'Selesai Input') {
                                return '<img src="' + fn.urlTo('') + '/img/status-selesai.png" width="250" height="auto">';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.berita_acara_penilaian',
                            filter: 'p.berita_acara_penilaian',
                            sort: 'p.berita_acara_penilaian',
                            display: 'berita_acara_penilaian',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('') + 'upload/penilaian-berita_acara_penilaian/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                            } else {
                                return '<span class="text-red">Belum tersedia.</span>';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ip.id',
                            filter: 'ip.id',
                            sort: 'ip.id',
                            display: 'id',
                        },
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('kasn/dashboard/list-verifikasi', {idInstansiPemerintah: row.id}) + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure">Verifikasi</a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.berita_acara_verifikasi',
                            filter: 'p.berita_acara_verifikasi',
                            sort: 'p.berita_acara_verifikasi',
                            display: 'berita_acara_verifikasi',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('') + 'upload/penilaian-berita_acara_verifikasi/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                            } else {
                                return '<span class="text-red">Belum tersedia.</span>';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.rekomendasi',
                            filter: 'p.rekomendasi',
                            sort: 'p.rekomendasi',
                            display: 'rekomendasi',
                        },
                        render: function ( data, type, row ) {
                            if (data != null) {
                                return '<a href="' + fn.urlTo('') + 'upload/penilaian-rekomendasi/' + row.id_penilaian + '/' + data + '" class="button button-xs margin-bottom-5 bg-lightest text-azure border-azure hover-bg-lightest hover-text-azure" target="_blank">Download</a>';
                            } else {
                                return '<span class="text-red">Belum tersedia.</span>';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },*/
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (false) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});