<?php
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app_merit',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_merit\controllers',
    'bootstrap' => [
        'log',
        function() {
            Yii::setAlias('@download-penilaian_detail-bukti_wajib', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_wajib');
            Yii::setAlias('@upload-penilaian_detail-bukti_wajib', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_wajib');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_1', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_1');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_1', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_1');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_2', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_2');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_2', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_2');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_3', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_3');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_3', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_3');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_4', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_4');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_4', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_4');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_5', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_5');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_5', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_5');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_6', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_6');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_6', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_6');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_7', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_7');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_7', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_7');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_8', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_8');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_8', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_8');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_9', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_9');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_9', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_9');
            Yii::setAlias('@download-penilaian_detail-bukti_tambahan_10', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian_detail-bukti_tambahan_10');
            Yii::setAlias('@upload-penilaian_detail-bukti_tambahan_10', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian_detail-bukti_tambahan_10');
            Yii::setAlias('@download-penilaian-berita_acara_penilaian', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian-berita_acara_penilaian');
            Yii::setAlias('@upload-penilaian-berita_acara_penilaian', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian-berita_acara_penilaian');
            Yii::setAlias('@download-penilaian-berita_acara_verifikasi', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian-berita_acara_verifikasi');
            Yii::setAlias('@upload-penilaian-berita_acara_verifikasi', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian-berita_acara_verifikasi');
            Yii::setAlias('@download-penilaian-rekomendasi', Yii::$app->getRequest()->getBaseUrl() . '/upload/penilaian-rekomendasi');
            Yii::setAlias('@upload-penilaian-rekomendasi', dirname(dirname(__DIR__)) . '/app_merit/web/upload/penilaian-rekomendasi');
            Yii::setAlias('@download-instansi_pemerintah-logo', Yii::$app->getRequest()->getBaseUrl() . '/upload/instansi_pemerintah-logo');
            Yii::setAlias('@upload-instansi_pemerintah-logo', dirname(dirname(__DIR__)) . '/app_merit/web/upload/instansi_pemerintah-logo');
        },
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_merit',
        ],
        'user' => [
            'identityClass' => 'app_merit\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_merit', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_merit',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;