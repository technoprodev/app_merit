if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            dev: {
                text: null,
                date: null,
                virtual_category: [],
                devChildren: [],
            },
        },
        methods: {
            addChild: function() {
                this.dev.devChildren.push({
                    id: null,
                    id_dev: null,
                    child: null
                });
            },
            removeChild: function(i, isNew) {
                if (this.dev.devChildren[i].id == null)
                    this.dev.devChildren.splice(i, 1);
                else
                    this.dev.devChildren[i].id*=-1;
            },
        },
        /*mounted() {
            $(".input-datepicker").datepicker().on(
                "changeDate" : function() {
                    this.dev.date = $('.input-datepicker').val();
                }
            );
        },*/
    });
}