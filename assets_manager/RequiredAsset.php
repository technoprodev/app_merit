<?php
namespace app_merit\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_merit/assets';

    public $css = [];

    public $js = [];

    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}